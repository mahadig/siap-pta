var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');
var del = require('del');

gulp.task('clean-js', function () {
  return del([
    'dist/js/*.js'
  ]);
});

gulp.task('clean-css', function () {
  return del([
    'dist/css/*.css'
  ]);
});

gulp.task('pack-js', gulp.series('clean-js', function () {  
  return gulp.src(
    [
    'assets/plugins/jquery/jquery.min.js',
    'assets/plugins/bootstrap/js/popper.min.js', 
    'assets/plugins/bootstrap/js/bootstrap.min.js',
    'assets/js/perfect-scrollbar.jquery.min.js',
    'assets/js/sidebarmenu.js',
    'assets/js/custom.js',
    'assets/plugins/select2/dist/js/select2.full.min.js',
    'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
    'assets/js/bootstrap-datepicker.min.js',
    'assets/js/bootstrap-datepicker.id.min.js',
    'assets/plugins/datatables.net/js/jquery.dataTables.min.js',
    'assets/plugins/Chart.js/Chart.min.js'
    ]
    ).pipe(concat('bundle.js'))
    // .pipe(minify({
    //     ext:{
    //         min:'.js'
    //     },
    //     noSource: true
    // }))
    .pipe(rev())
    .pipe(gulp.dest('dist/js'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
        merge: true,
        base: 'dist'
    }))
    .pipe(gulp.dest('dist'));
}));

gulp.task('pack-css', gulp.series('clean-css', function () {  
  return gulp.src(
    [
      'assets/plugins/bootstrap/css/bootstrap.min.css', 
      'assets/plugins/perfect-scrollbar/dist/css/perfect-scrollbar.min.css',
      'assets/css/icons/font-awesome/css/fontawesome-all.css',
      'assets/css/icons/material-design-iconic-font/css/materialdesignicons.min.css',
      'assets/css/spinners.css',
      'assets/css/animate.css',
      'assets/css/style.css',
      'assets/css/colors/green-dark.css',
      'assets/plugins/select2/dist/css/select2.min.css',
      'assets/css/datatable.css',
      'assets/css/custom.css',
      'assets/css/bootstrap-datepicker.min.css'
    ])
    .pipe(concat('style.css'))
    .pipe(cleanCss())
    .pipe(rev())
    .pipe(gulp.dest('dist/css'))
    .pipe(rev.manifest('dist/rev-manifest.json', {
      merge: true,
      base: 'dist'
    }))
    .pipe(gulp.dest('dist'));
}));

gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js', gulp.series('pack-js'));
  gulp.watch('assets/css/**/*.css', gulp.series('pack-css'));
});

gulp.task('default', gulp.series('watch'));