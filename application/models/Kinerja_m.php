<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kinerja_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function kinerjaHakim($start, $perpage = 0, $columns, $order, $filter){
		if(count($columns) > 0){
			$orderIndex = (int) $order[0]['column'];
			$orderDir = $order[0]['dir'];
			$orderColumnName = $columns[$orderIndex]['data'];

			$orderBy = " ORDER BY $orderColumnName $orderDir";
		}

		$tgl_start = $filter['tgl_start'];
		$tgl_end = $filter['tgl_end'];

    	if(is_array($filter) && validateDate($tgl_start) && validateDate($tgl_end)){
			$tgl_start = toSQLDate($tgl_start);
			$tgl_end = toSQLDate($tgl_end);

			$where = "AND (tgl_penetapan_majelis >= '".$tgl_start."' AND tgl_penetapan_majelis <= '".$tgl_end."')";
		}else{
			$where = "AND YEAR(tgl_penetapan_majelis) = YEAR(NOW()) AND tgl_penetapan_majelis is not null";
		}

		return $this->db->query("SELECT *, (km+konsep+anggota+resume) as total FROM
			(
				select id, nama,

				(select count(perkara_id) * 2 from perkara where km_id = hakim_tinggi.id $where) as km,
				(select count(perkara_id) * 3 from perkara where konseptor_id = hakim_tinggi.id $where) as konsep,
				(select count(perkara_id) * 1 from perkara_hakim_anggota where hakim_id = hakim_tinggi.id $where) as anggota,
				(select count(perkara_id) * 2 from perkara_hakim_anggota where hakim_id = hakim_tinggi.id and resume = 'Y' $where) as resume

				from hakim_tinggi
			) as t $orderBy")->result_array();
	}

	function beban($id){
		return $this->db->query('
			SELECT count(perkara_id) as jml FROM perkara WHERE km_id = ? 
			AND (YEAR(tgl_minutasi) = YEAR(NOW()) OR (YEAR(tgl_register) = YEAR(NOW()) OR tgl_minutasi IS NULL))
			', [ $id ])->row()->jml;
	}

	function k_all($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) <=60 THEN 1 ELSE 0 END) as hijau,
				SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >60 AND DATEDIFF(tgl_minutasi,tgl_register) <= 90 THEN 1 ELSE 0 END) 
				as kuning,
				SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) > 90 THEN 1 ELSE 0 END) as merah,
				SUM(
				IF(DATEDIFF(tgl_minutasi,tgl_register) >= 0 AND DATEDIFF(tgl_minutasi,tgl_register) <=60, 3, 
				IF(DATEDIFF(tgl_minutasi,tgl_register) <= 90, 2, 
				IF(DATEDIFF(tgl_minutasi,tgl_register) > 90,1,0)))
				) as bobot
			FROM perkara as a
			WHERE km_id = ? 
			AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id])->row_array();
	}

	function k_kirimBerkas($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding) >= 0 AND DATEDIFF(tgl_minutasi,tgl_mohon_banding) <= IF(tabayun = 0, 30, 45) , 3, 
				IF(DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding) <= IF(tabayun = 0, 45, 60), 2, 
				IF(DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding) > IF(tabayun = 0, 45, 60),1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_phs($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_phs,tgl_penetapan_majelis) >= 0 AND DATEDIFF(tgl_phs,tgl_penetapan_majelis) <=3, 3, 
				IF(DATEDIFF(tgl_phs,tgl_penetapan_majelis) <= 7, 2, 
				IF(DATEDIFF(tgl_phs,tgl_penetapan_majelis) > 7,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_phs,tgl_penetapan_majelis)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_sidang($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_putusan,tgl_sidang_pertama) >= 0 AND 
				DATEDIFF(tgl_putusan,tgl_sidang_pertama) <=60, 3, 
				IF(DATEDIFF(tgl_putusan,tgl_sidang_pertama) <= 90, 2, 
				IF(DATEDIFF(tgl_putusan,tgl_sidang_pertama) > 90,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_putusan,tgl_sidang_pertama)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_minut($id){ 
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_minutasi,tgl_putusan) >= 0 AND 
				DATEDIFF(tgl_minutasi,tgl_putusan) <=1, 3, 
				IF(DATEDIFF(tgl_minutasi,tgl_putusan) <= 5, 2, 
				IF(DATEDIFF(tgl_minutasi,tgl_putusan) > 5,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_minutasi,tgl_putusan)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_serah($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_serah_panmud,tgl_minutasi) >= 0 AND 
				DATEDIFF(tgl_serah_panmud,tgl_minutasi) <=1, 3, 
				IF(DATEDIFF(tgl_serah_panmud,tgl_minutasi) <= 5, 2, 
				IF(DATEDIFF(tgl_serah_panmud,tgl_minutasi) > 5,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_serah_panmud,tgl_minutasi)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_kirim($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) >= 0 AND 
				DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) <=1, 3, 
				IF(DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) <= 7, 2, 
				IF(DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) > 7,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_kirim_pa,tgl_serah_panmud)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_meja3($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) >= 0 AND 
				DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) <=1, 3, 
				IF(DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) <= 7, 2, 
				IF(DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) > 7,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_serah_meja3,tgl_kirim_pa)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_anonim($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_anonimasi,tgl_serah_meja3) >= 0 AND 
				DATEDIFF(tgl_anonimasi,tgl_serah_meja3) <=1, 3, 
				IF(DATEDIFF(tgl_anonimasi,tgl_serah_meja3) <= 7, 2, 
				IF(DATEDIFF(tgl_anonimasi,tgl_serah_meja3) > 7,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_anonimasi,tgl_serah_meja3)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function k_upload($id){
		return $this->db->query('SELECT *, IFNULL(ROUND(bobot / (total*3)*100,2),0) as kinerja FROM (
			SELECT 
				(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as total,
				SUM(
				IF(DATEDIFF(tgl_upload,tgl_anonimasi) >= 0 AND 
				DATEDIFF(tgl_upload,tgl_anonimasi) <=1, 3, 
				IF(DATEDIFF(tgl_upload,tgl_anonimasi) <= 7, 2, 
				IF(DATEDIFF(tgl_upload,tgl_anonimasi) > 7,1,0)))
				) as bobot,
				ROUND(AVG(DATEDIFF(tgl_upload,tgl_anonimasi)),1) as avg
			FROM perkara as a
			WHERE km_id = ? AND (YEAR(tgl_minutasi) = YEAR(NOW()))
		) as t', [ $id ])->row_array();
	}

	function kinerjaKonseptor($columns, $order, $filter){
		if(count($columns) > 0){
			$orderIndex = (int) $order[0]['column'];
			$orderDir = $order[0]['dir'];
			$orderColumnName = $columns[$orderIndex]['data'];

			$this->db->order_by($orderColumnName, $orderDir);
		}
		
		$this->db->select("count(perkara_id) as jumlah, nama_konseptor, konseptor_id");

		$tgl_start = $filter['tgl_start'];
		$tgl_end = $filter['tgl_end'];

    	if(is_array($filter) && validateDate($tgl_start) && validateDate($tgl_end)){
			$tgl_start = toSQLDate($tgl_start);
			$tgl_end = toSQLDate($tgl_end);

			$where = "(tgl_kirim_konsep >= '".$tgl_start."' AND tgl_kirim_konsep <= '".$tgl_end."')";
		}else{
			$where = "YEAR(tgl_kirim_konsep) = YEAR(NOW()) AND tgl_kirim_konsep is not null";
		}

		$this->db->where($where, null);
		$this->db->where('konseptor_id is not null', null);
		$this->db->group_by("konseptor_id");

		return $this->db->get('perkara')->result_array();
	}

	function overall($start, $perpage = 0, $columns, $order, $filter){
		if(count($columns) > 0){
			$orderIndex = (int) $order[0]['column'];
			$orderDir = $order[0]['dir'];
			$orderColumnName = $columns[$orderIndex]['data'];

			if($orderColumnName == 'nomor_perkara_banding' || $orderColumnName == 'nomor_perkara_pa'){
				$this->db->order_by("CAST(SUBSTRING_INDEX($orderColumnName, '/', 1) AS UNSIGNED) $orderDir");
				$this->db->order_by("CAST(SUBSTRING_INDEX($orderColumnName, '/', 2) AS UNSIGNED) $orderDir");
			}else{
				$this->db->order_by($orderColumnName, $orderDir);
			}
		}

    	$this->db->select('SQL_CALC_FOUND_ROWS 
			a.km_id, c.jumlah, nama_km, 
			(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as perkara_selesai,
			(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 0 ELSE 1 END)) as sisa,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) <=60 THEN 1 ELSE 0 END) as hijau,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >60 AND DATEDIFF(tgl_minutasi,tgl_register) <= 90 THEN 1 ELSE 0 END) 
			as kuning,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) > 90 THEN 1 ELSE 0 END) as merah,

			IFNULL(ROUND(SUM(
			IF(DATEDIFF(tgl_minutasi,tgl_register) <=60, 3, IF(DATEDIFF(tgl_minutasi,tgl_register) <= 90, 2, IF(DATEDIFF(tgl_minutasi,tgl_register) > 90,1,0)))
			) / (c.jumlah*3)*100,2),0)
			as kinerja', false);
    	
    	$where = "";

		$tgl_start = $filter['tgl_start'];
		$tgl_end = $filter['tgl_end'];

    	if(is_array($filter) && validateDate($tgl_start) && validateDate($tgl_end)){
			$tgl_start = toSQLDate($tgl_start);
			$tgl_end = toSQLDate($tgl_end);

			$where .= "(tgl_penetapan_majelis >= '".$tgl_start."' AND tgl_penetapan_majelis <= '".$tgl_end."')";
		}else{
			$where .= "YEAR(tgl_penetapan_majelis) = YEAR(NOW()) AND tgl_penetapan_majelis is not null";
		}
		

		if($where){
			$this->db->where($where, null);
			$where = "AND ".$where;
		}

		$this->db->where('a.km_id is not null', null);
		$this->db->where('a.km_id != 0', null);
    	$this->db->from('perkara as a');
    	// $this->db->order_by('kinerja', 'desc');
    	$this->db->order_by('jumlah', 'desc');
    	$this->db->join('(SELECT km_id, Count(*) AS jumlah 
                       FROM perkara as b 
                       WHERE b.km_id is not null '.$where.'
                       group by km_id) as c', 'c.km_id = a.km_id','left');
    	$this->db->group_by('a.km_id');

    	return $this->db->get()->result_array();
    }

    // public function detailKinerja($hakimId){

    // }
}