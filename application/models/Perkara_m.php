<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perkara_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function saveHakimAnggota($perkaraId, $data){
		$this->db->trans_start();

		$this->db->delete('perkara_hakim_anggota', 
			array('perkara_id' => $perkaraId)); 
		
		if(is_array($data) && count($data) > 0){
			$this->db->insert_batch('perkara_hakim_anggota', $data);
		}

		$this->db->trans_complete();
	}

	function overview($filter_period, $tgl_start, $tgl_end){
		$dates = ["tgl_minutasi", "tgl_register"];

		$where = " and year(tgl_register) = YEAR(NOW()) OR YEAR(tgl_minutasi) = YEAR(NOW()) OR YEAR(tgl_putusan) = YEAR(NOW())";

		if(array_search($filter_period, $dates) !== false && validateDate($tgl_start) && validateDate($tgl_end)){

			$tgl_start = toSQLDate($tgl_start);
			$tgl_end = toSQLDate($tgl_end);

			$where = "AND (($filter_period >= '$tgl_start' AND $filter_period <= '$tgl_end') OR $filter_period is null)";
		}

		$row1 = $this->db->query("select *, ROUND((selesai/beban) * 100,2) as rasio FROM (
			select 
			count(perkara_id) as beban,
			sum(CASE WHEN tgl_minutasi is not null THEN 1 ELSE 0 END) as selesai,
			sum(CASE WHEN tgl_minutasi is null THEN 1 ELSE 0 END) as sisa,
			sum(CASE WHEN tgl_minutasi is null && tgl_putusan is not null THEN 1 ELSE 0 END) as belum_minut,
			(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >=0 THEN 1 ELSE 0 END)) as perkara_selesai,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) <=60 THEN 1 ELSE 0 END) as hijau,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) >60 AND DATEDIFF(tgl_minutasi,tgl_register) <= 90 THEN 1 ELSE 0 END) 
			as kuning,
			SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_register) > 90 THEN 1 ELSE 0 END) as merah

			from perkara
			where (tgl_register is not null)
			".$where."
		) as t")->row();

		$row2 =  $this->db->query("select 
			LEFT(tgl_putusan,7) as periode, year(tgl_putusan) as tahun, month(tgl_putusan) as bulan, count(perkara_id) as jml, jenis_putus_id, jp.jenis_putusan from perkara 
		left join jenis_putusan as jp on jp.id = perkara.jenis_putus_id
		where jenis_putus_id is not null and tgl_putusan is not null 
		".$where."
		group by LEFT(tgl_putusan,7), jenis_putusan 
		order by tgl_putusan asc")->result_array();

		return ['overview' => $row1, 'stats' => $row2];
	}

	function jenis_putusan_nama($id){
		$this->db->select('jenis_putusan');
		$this->db->where('id', $id);

		$res = $this->db->get('jenis_putusan')->row_array();

		if($res) return $res['jenis_putusan'];

		return '';
	}

	function jenis_putusan(){
		return $this->db->get('jenis_putusan')->result_array();
	}

	function getHakimAnggota($perkaraId){
		return $this->db->query('SELECT * FROM perkara_hakim_anggota WHERE perkara_id = ?', 
			[ $perkaraId ])->result_array();
	}

	function find($perkaraId){
		$this->db->where('perkara_id', $perkaraId);

		return $this->db->get('perkara')->row_array();
	}

	function kinerja($perkaraId){
		$this->db->select('*, 
			DATEDIFF(tgl_minutasi,tgl_register) as proses_all,
			DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding) as proses_kirim_berkas,
			DATEDIFF(tgl_phs,tgl_penetapan_majelis) as proses_phs,
			DATEDIFF(tgl_putusan,tgl_sidang_pertama) as proses_sidang,
			DATEDIFF(tgl_minutasi,tgl_putusan) as proses_minut,
			DATEDIFF(tgl_serah_panmud,tgl_minutasi) as proses_serah,
			DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) as proses_kirim,
			DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) as proses_meja3,
			DATEDIFF(tgl_anonimasi,tgl_serah_meja3) as proses_anonim,
			DATEDIFF(tgl_upload,tgl_anonimasi) as proses_upload', false);

		$this->db->from('perkara');
		$this->db->order_by('perkara_id', 'asc');

		$this->db->where('perkara_id', $perkaraId);

		return $this->db->get()->row_array();
	}

	function valid_noperkarapa($str, $id = false){
		$this->db->select('count(perkara_id) as cnt');
		$this->db->where('nomor_perkara_pa', $str);

		if($id){
			$this->db->where('perkara_id !=', $id);
		}

		return $this->db->get('perkara')->row()->cnt == 0;
	}

	function valid_noperkarapta($str, $id = false){
		$this->db->select('count(perkara_id) as cnt');
		$this->db->where('nomor_perkara_banding', $str);

		if($id){
			$this->db->where('perkara_id !=', $id);
		}

		return $this->db->get('perkara')->row()->cnt == 0;
	}

	function table($start, $perpage = 0, $columns, $order, $filter = []){
		
		if(count($columns) > 0){
			$orderIndex = (int) $order[0]['column'];
			$orderDir = $order[0]['dir'];
			$orderColumnName = $columns[$orderIndex]['data'];

			if($orderColumnName == 'nomor_perkara_banding' || $orderColumnName == 'nomor_perkara_pa'){
				$this->db->order_by("CAST(SUBSTRING_INDEX($orderColumnName, '/', 1) AS UNSIGNED) $orderDir");
				$this->db->order_by("CAST(SUBSTRING_INDEX($orderColumnName, '/', 2) AS UNSIGNED) $orderDir");
			}else{
				$this->db->order_by($orderColumnName, $orderDir);
			}
		}

		$dates = ["tgl_minutasi", "tgl_register"];

		if(is_array($filter)){
			foreach ($filter as $key => $value) {
				if(!$value) continue;

				$valInt = (int) $value;

				if($key == "nomor_perkara_pa" || $key == "nomor_perkara_banding"){
					if($valInt > 0){
						$value2 = str_pad($valInt, 4, '0', STR_PAD_LEFT);
						$this->db->group_start();
						$this->db->where("LEFT($key,LOCATE('/',$key) - 1) =", $valInt);
						$this->db->or_where("LEFT($key,LOCATE('/',$key) - 1) =", $value2);
						$this->db->group_end();
					}else{
						$this->db->like('nomor_perkara_pa', $value);
						$this->db->or_like('nomor_perkara_banding', $value);
					}
				}else if($key == "km_id"){
					$this->db->where('km_id', $valInt);
				}else if($key == "pp_id"){
					$this->db->where('pp_id', $valInt);
				}else if($key == "konseptor_id"){
					$this->db->where('konseptor_id', $valInt);
				}
			}

			if(isset($filter['list'])){
				
				if($filter['list'] == "kinerja_majelis"){
					$this->db->where('(YEAR(tgl_minutasi) = YEAR(NOW()) OR (YEAR(tgl_register) = YEAR(NOW()) OR tgl_minutasi IS NULL))', null);

					if($filter['type'] == "selesai"){
						$this->db->where('tgl_minutasi is not null', null);

						if(isset($filter['perf'])){
							if($filter['perf'] == "green"){
								$this->db->where('DATEDIFF(tgl_minutasi,tgl_register) <=60', null);
							}else if($filter['perf'] == "yellow"){
								$this->db->where('DATEDIFF(tgl_minutasi,tgl_register) >60 AND DATEDIFF(tgl_minutasi,tgl_register) <= 90', null);
							}else if($filter['perf'] == "red"){
								$this->db->where('DATEDIFF(tgl_minutasi,tgl_register) > 90', null);
							}

						}

					}else if($filter['type'] == "sisa"){
						$this->db->where('tgl_minutasi is null', null);
					}else if($filter['type'] == "belum_minut"){
						$this->db->where('tgl_minutasi is null AND tgl_putusan is not null', null);
					}

					$tgl_start = $filter['tgl_start'];
					$tgl_end = $filter['tgl_end'];

					if(validateDate($tgl_start) && validateDate($tgl_end)){
						$tgl_start = toSQLDate($tgl_start);
						$tgl_end = toSQLDate($tgl_end);

						$this->db->where('tgl_penetapan_majelis >= ', $tgl_start);
						$this->db->where('tgl_penetapan_majelis <= ', $tgl_end);
					}
				}else if($filter['list'] == "konseptor"){
					$this->db->where('tgl_kirim_konsep is not null', null);

					$tgl_start = $filter['tgl_start'];
					$tgl_end = $filter['tgl_end'];

					if(validateDate($tgl_start) && validateDate($tgl_end)){
						$tgl_start = toSQLDate($tgl_start);
						$tgl_end = toSQLDate($tgl_end);

						$this->db->where('tgl_kirim_konsep >= ', $tgl_start);
						$this->db->where('tgl_kirim_konsep <= ', $tgl_end);
					}
				}else if($filter['list'] == "kinerja_pp"){
					$this->db->where('tgl_penunjukan_pp is not null', null);

					$tgl_start = $filter['tgl_start'];
					$tgl_end = $filter['tgl_end'];

					if($filter['type'] == "selesai"){
						$this->db->where('tgl_minutasi is not null', null);						
					}else if($filter['type'] == "sisa"){
						$this->db->where('tgl_minutasi is null', null);						
					}else if($filter['type'] == "konsep_h"){

						$this->db->where('DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) >= 1 AND DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) <=3', null);						
					}else if($filter['type'] == "konsep_k"){
						$this->db->where('DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) >=4 AND DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) <=7', null);						
					}else if($filter['type'] == "konsep_m"){
						$this->db->where('DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) > 7', null);						
					}else if($filter['type'] == "minut_h"){
						$this->db->where('DATEDIFF(tgl_minutasi,tgl_putusan) >= 1 AND 
							DATEDIFF(tgl_minutasi,tgl_putusan) <=3', null);						
					}else if($filter['type'] == "minut_k"){
						$this->db->where('DATEDIFF(tgl_minutasi,tgl_putusan) >=4 AND 
							DATEDIFF(tgl_minutasi,tgl_putusan) <=7', null);						
					}else if($filter['type'] == "minut_m"){
						$this->db->where('DATEDIFF(tgl_minutasi,tgl_putusan) > 7', null);						
					}

					if(validateDate($tgl_start) && validateDate($tgl_end)){
						$tgl_start = toSQLDate($tgl_start);
						$tgl_end = toSQLDate($tgl_end);

						$this->db->where('tgl_penunjukan_pp >= ', $tgl_start);
						$this->db->where('tgl_penunjukan_pp <= ', $tgl_end);
					}else{
						$this->db->where('YEAR(tgl_penunjukan_pp) =  YEAR(NOW())', null);
					}
				}

				
			}else{
				if(isset($filter['tgl_start'])){
					$tgl_start = $filter['tgl_start'];
					$tgl_end = $filter['tgl_end'];

					if(validateDate($tgl_start) && validateDate($tgl_end)){
						$tgl_start = toSQLDate($tgl_start);
						$tgl_end = toSQLDate($tgl_end);

						$filter_period = $filter['filter_period'];

						if(array_search($filter_period, $dates) !== false){
							$this->db->where($filter_period.' >= ', $tgl_start);
							$this->db->where($filter_period.' <= ', $tgl_end);
						}
					}
				}
			}

			
		}


		$this->db->select('SQL_CALC_FOUND_ROWS *', false);
		

		if(isset($perpage) && $perpage > 0){
			$this->db->order_by('perkara_id','DESC');
			$this->db->limit($perpage, $start);
		}else{
			$this->db->order_by('perkara_id','ASC');
		}

		$query = $this->db->get('perkara as p');

		return $query->result_array();
	}

	function total(){
		$query = $this->db->query("SELECT FOUND_ROWS() as cnt;");

		$res = $query->row();

		if($res){
			return (int) $res->cnt;
		}else{
			return 0;
		}
	}

	function add($data){
		$this->db->insert('perkara', $data);

		return $this->db->insert_id();
	}

	function update($data, $id){
		return $this->db->update('perkara', $data, array('perkara_id' => $id));
	}

	function delete($id){
		return $this->db->delete('perkara', array('perkara_id' => $id));
	}

    //

   //  function sisa(){

   //  	return $this->db->query("select km_id, nama_km, 
			// sum(1) as beban,
			// sum(CASE WHEN tgl_minutasi is not null THEN 1 ELSE 0 END) as putus,
			// sum(CASE WHEN tgl_minutasi is null THEN 1 ELSE 0 END) as sisa
			// from perkara  
			// where km_id is not null 
			// AND (YEAR(tgl_minutasi) = YEAR(NOW()) OR (YEAR(tgl_register) = YEAR(NOW()) OR tgl_minutasi IS NULL))
			// group by km_id 
			// order by sisa desc, beban desc")->result_array();
   //  }


	function monitoring($start, $perpage = 0, $columns, $order, $filter){
		$dates = ["tgl_minutasi", "tgl_register"];

		if(is_array($filter)){
			foreach ($filter as $key => $value) {
				if(!$value) continue;

				$valInt = (int) $value;

				if($key == "nomor_perkara_pa" || $key == "nomor_perkara_banding"){
					if($valInt > 0){
						$value2 = str_pad($valInt, 4, '0', STR_PAD_LEFT);
						$this->db->group_start();
						$this->db->where("LEFT($key,LOCATE('/',$key) - 1) =", $valInt);
						$this->db->or_where("LEFT($key,LOCATE('/',$key) - 1) =", $value2);
						$this->db->group_end();
					}else{
						$this->db->like('nomor_perkara_pa', $value);
						$this->db->or_like('nomor_perkara_banding', $value);
					}
				}else if($key == "km_id"){
					$this->db->where('km_id', $valInt);
				}
			}

			$tgl_start = $filter['tgl_start'];
			$tgl_end = $filter['tgl_end'];

			if(validateDate($tgl_start) && validateDate($tgl_end)){
				$tgl_start = toSQLDate($tgl_start);
				$tgl_end = toSQLDate($tgl_end);

				$filter_period = $filter['filter_period'];

				if(array_search($filter_period, $dates) !== false){
					$this->db->where($filter_period.' >= ', $tgl_start);
					$this->db->where($filter_period.' <= ', $tgl_end);
				}
			}
		}

		$this->db->select('SQL_CALC_FOUND_ROWS 
			perkara_id, km_id, nomor_perkara_banding, nomor_perkara_pa, nama_km, 
			DATEDIFF(tgl_minutasi,tgl_register) as proses_all,
			DATEDIFF(tgl_kirim_berkas,tgl_mohon_banding) as proses_kirim_berkas,
			DATEDIFF(tgl_penetapan_majelis,tgl_register) as proses_pmh,
			DATEDIFF(tgl_penunjukan_pp,tgl_register) as proses_pp,
			DATEDIFF(tgl_phs,tgl_penetapan_majelis) as proses_phs,
			DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) as proses_rangka_putusan,
			DATEDIFF(tgl_putusan,tgl_sidang_pertama) as proses_sidang,
			DATEDIFF(tgl_minutasi,tgl_putusan) as proses_minut,
			DATEDIFF(tgl_serah_panmud,tgl_minutasi) as proses_serah,
			DATEDIFF(tgl_kirim_pa,tgl_serah_panmud) as proses_kirim,
			DATEDIFF(tgl_serah_meja3,tgl_kirim_pa) as proses_meja3,
			DATEDIFF(tgl_anonimasi,tgl_serah_meja3) as proses_anonim,
			DATEDIFF(tgl_upload,tgl_anonimasi) as proses_upload,
			DATEDIFF(tgl_arsip,tgl_anonimasi) as proses_upload', false);

		if(isset($perpage) && $perpage > 0){
			$this->db->limit($perpage, $start);
		}

		$this->db->from('perkara');
		$this->db->order_by('perkara_id', 'asc');

		return $this->db->get()->result_array();

   //  	return $this->db->query("select 
			// from perkara ORDER BY perkara_id DESC")
   //  	->result_array();
	}

    // function sisa(){
    // 	return $this->db->query('select km_id, nama_km, count(nama_km) as jml from perkara  where km_id is not null and tgl_putusan is null group by km_id order by jml desc')->result_array();
    // }

    // function beban(){
    // 	return $this->db->query('select km_id, nama_km, count(nama_km) as jml from perkara  where km_id is not null group by km_id order by jml desc')->result_array();
    // }

    // function putus(){
    // 	return $this->db->query('select km_id, nama_km, count(nama_km) as jml from perkara  where km_id is not null and tgl_putusan is not null group by km_id order by jml desc')->result_array();
    // }
}