<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panitera_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function singkron(){
		$data = array(
			'title' => 'My title',
			'name' => 'My Name',
			'date' => 'My date'
		);

		$this->db->insert('singkron', $data);
	}

	function any($q){
		$this->db->select('id as id, nama, nip');
		$this->db->like('nip', $q);
		$this->db->or_like('nama', $q);

		return $this->db->get('panitera')->result_array();
	}

	function find($hakimId){
		$this->db->where('id', $hakimId);

		return $this->db->get('panitera')->row_array();
	}

	function kinerja($column, $orders, $filter){
		$bulan = intval($filter['bulan']);
		$tahun = intval($filter['tahun']);

		if($bulan != 0 && $tahun != 0){
			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;
		}else if($tahun != 0){
			$period = $tahun.'-'.date('m');
		}else{
			$period = date('Y-m');
		}

		$order = "";
		if(count($column) > 0){
			$orderIndex = (int) $orders[0]['column'];
			$orderDir = $orders[0]['dir'];
			$orderColumnName = $column[$orderIndex]['data'];

			if($orderDir != 'disabled'){
				$order = " ORDER BY $orderColumnName $orderDir";
			}
		}

		return $this->db->query("
			SELECT *, IFNULL(ROUND(((k_minut * (20/100))+(k_konsep* (20/100))+(nilai_telaah* (50/100))+(nilai_singkron* (10/100))), 2), 0) as nilai_akhir FROM (SELECT pp.pp_id, pp.nama_pp, IFNULL(minut, 0) as minut, IFNULL(ROUND((minut / (total*5)) *100, 2), 0) as k_minut, IFNULL(ROUND((konsep / (total_konsep*5)) *100, 2), 0) as k_konsep, IFNULL(total_konsep,0) as total_konsep, IFNULL(nilai_telaah, 0) as nilai_telaah, IFNULL(ROUND(hit_singkron / t3.hari_kerja,2), 0) as nilai_singkron FROM (
			SELECT pp_id, nama_pp FROM perkara WHERE pp_id IS NOT NULL GROUP BY pp_id
		) as pp

		LEFT JOIN ((
		select pp_id, nama_pp, 
		(SUM(CASE WHEN DATEDIFF(tgl_minutasi,tgl_putusan) >=0 THEN 1 ELSE 0 END)) as total,
		SUM(IF(
		DATEDIFF(tgl_minutasi,tgl_putusan) = 0, 5,
		IF(DATEDIFF(tgl_minutasi, tgl_putusan) <= 3, 3, 
		IF(DATEDIFF(tgl_minutasi, tgl_putusan) <= 7, 1,
		IF(DATEDIFF(tgl_minutasi, tgl_putusan) > 7, 0, 0)
		))
	)) as minut
	from perkara as pk
	where left(pk.tgl_minutasi,7) = ?
	group by pp_id
)) as t ON pp.pp_id = t.pp_id

LEFT JOIN (
SELECT pp_id, nama_pp, 
(SUM(CASE WHEN DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) >=0 THEN 1 ELSE 0 END)) as total_konsep,
SUM(IF(
DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) = 0, 5,
IF(DATEDIFF(tgl_kirim_konsep, tgl_terima_berkas_pp) <= 3, 3, 
IF(DATEDIFF(tgl_kirim_konsep, tgl_terima_berkas_pp) <= 7, 1,
IF(DATEDIFF(tgl_kirim_konsep, tgl_terima_berkas_pp) > 7, 0, 0)
))
)) as konsep
from perkara as pk
where left(pk.tgl_kirim_konsep,7) = ?
group by pp_id
) as t2 ON pp.pp_id = t2.pp_id

LEFT JOIN (
SELECT pp_id, hari_kerja, AVG(nilai_telaah) as nilai_telaah FROM e_laporan WHERE periode = ? GROUP BY pp_id
) as t3 ON pp.pp_id = t3.pp_id

LEFT JOIN (
SELECT COUNT(id) hit_singkron, pp_id FROM singkronisasi where left(tgl_singkron,7) = ? GROUP BY pp_id
) as t4 ON pp.pp_id = t4.pp_id) as tMain $order
", [$period,$period,$period,$period])->result_array();
	}

	function proses($column, $orders, $filter){
		$filterTelaahSingkron = "YEAR(ss.tgl_singkron) = YEAR(NOW())";
		$filterTelaah = "YEAR(el.tgl_telaah) = YEAR(NOW())";
		$filterPenunjukan = "YEAR(tgl_penunjukan_pp) = YEAR(NOW())";

		$tgl_start = $filter['tgl_start'];
		$tgl_end = $filter['tgl_end'];

		if(validateDate($tgl_start) && validateDate($tgl_end)){
			$filterTelaah = "el.tgl_telaah >= '".toSQLDate($tgl_start)."' AND el.tgl_telaah <= '".toSQLDate($tgl_end)."'";

			$filterTelaahSingkron = "ss.tgl_singkron >= '".toSQLDate($tgl_start)."' AND ss.tgl_singkron <= '".toSQLDate($tgl_end)."'";

			$filterPenunjukan = "tgl_penunjukan_pp >= '".toSQLDate($tgl_start)."' AND tgl_penunjukan_pp <= '".toSQLDate($tgl_end)."'";
		}

		$order = "";
		if(count($column) > 0){
			$orderIndex = (int) $orders[0]['column'];
			$orderDir = $orders[0]['dir'];
			$orderColumnName = $column[$orderIndex]['data'];

			if($orderDir != 'disabled'){
				$order = " ORDER BY $orderColumnName $orderDir";
			}
		}

		return $this->db->query("select *, (jumlah - minutasi) as sisa from (
			SELECT pp_id, nama_pp, 
			count(perkara_id) as jumlah,
			(SUM(CASE WHEN tgl_minutasi IS NOT NULL THEN 1 ELSE 0 END)) as minutasi,
			
			SUM(IF(DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) = 0 AND DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) <=3, 1, 0)) as konsep_p,

			SUM(IF(DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) > 0 AND DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) <=3, 1, 0)) as konsep_h,

			SUM(IF(DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) >=4 AND DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) <=7, 1, 0)) as konsep_k,
			SUM(IF(DATEDIFF(tgl_kirim_konsep,tgl_terima_berkas_pp) > 7, 1, 0)) as konsep_m,

			SUM(IF(DATEDIFF(tgl_minutasi,tgl_putusan) = 0, 1, 0)) as minut_p,

			SUM(IF(DATEDIFF(tgl_minutasi,tgl_putusan) > 0 AND DATEDIFF(tgl_minutasi,tgl_putusan) <=3, 1, 0)) as minut_h,
			SUM(IF(DATEDIFF(tgl_minutasi,tgl_putusan) >=4 AND DATEDIFF(tgl_minutasi,tgl_putusan) <=7, 1, 0)) as minut_k,
			SUM(IF(DATEDIFF(tgl_minutasi,tgl_putusan) > 7, 1, 0)) as minut_m,

			( SELECT count(pp_id) as jml FROM singkronisasi as ss WHERE 
			".$filterTelaahSingkron." 
			AND ss.pp_id = perkara.pp_id ) as t_singkron,

			( SELECT AVG(nilai_telaah) as jml FROM e_laporan as el WHERE 
			".$filterTelaah." 
			AND el.pp_id = perkara.pp_id ) as t_laporan
			
			FROM perkara
			WHERE pp_id IS NOT NULL AND 
			".$filterPenunjukan."
			GROUP BY pp_id
		) as t $order")->result_array();
	}

	function nama($hakimId){
		if($hakimId == '') return '';

		$res = $this->find($hakimId);

		return ($res && $res['nama']) ? $res['nama'] : '';
	}

	function table($start, $perpage = 0, $columns, $order, $filter = []){
		
		$this->db->select('SQL_CALC_FOUND_ROWS *', false);
		$this->db->order_by('id','DESC');

		if(isset($perpage) && $perpage > 0){
			$this->db->limit($perpage, $start);
		}

		$query = $this->db->get('panitera as h');

		return $query->result_array();
	}

	function total(){
		$query = $this->db->query("SELECT FOUND_ROWS() as cnt;");

		$res = $query->row();

		if($res){
			return (int) $res->cnt;
		}else{
			return 0;
		}
	}

	function add($data){
		return $this->db->insert('panitera', $data);
	}

	function update($data, $id){
		return $this->db->update('panitera', $data, array('id' => $id));
	}

	function delete($id){
		return $this->db->delete('panitera', array('id' => $id));
	}

	function valid_nip($str, $id = false){
		$this->db->select('count(id) as cnt');
		$this->db->where('nip', $str);

		if($id){
			$this->db->where('id !=', $id);
		}

		return $this->db->get('panitera')->row()->cnt == 0;
	}
}