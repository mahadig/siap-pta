<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class E_laporan_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function checkSingkron($userId, $date){
		$this->db->select('count(id) as jumlah');
		$this->db->from('singkronisasi');
		$this->db->where('pp_id', $userId);
		$this->db->where('tgl_singkron', $date);

		return $this->db->get()->row()->jumlah;
	}

	function singkron($data){
    	return $this->db->insert('singkronisasi', $data);
    }
    
	function satker($q){
		$this->db->select('id, id as nip, nama_singkat as nama');
		$this->db->like('id', $q);
		$this->db->or_like('nama_singkat', $q);

		return $this->db->get('satker')->result_array();
	}

	function findSatker($perkaraId){
		$this->db->where('id', $perkaraId);

		return $this->db->get('satker')->row_array();
	}

	function findBySatker($satkerId, $bulan = 0, $tahun = 0){
		if($bulan != 0 && $tahun != 0){
			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;
		}else if($tahun != 0){
			$period = $tahun.'-'.date('m');
		}else{
			$period = date('Y-m');
		}

		$this->db->select('SQL_CALC_FOUND_ROWS s.id as satker_id,hari_kerja, el.id as data_id,nama_singkat,pp_id, nama_pp, tgl_konfirmasi, tgl_telaah, keterangan, elNilai.*', false);

		$this->db->where('s.id', $satkerId);
		$this->db->group_start();
		$this->db->where('periode', $period);
		$this->db->or_where('periode is null', null);
		$this->db->group_end();

		// $this->db->join('e_laporan as el', 'el.satker_id = s.id', 'left');
		$this->db->join('e_laporan as el', "el.satker_id = s.id AND (`periode` = '$period' OR `periode` is null )", 'left');
		$this->db->join('e_laporan_nilai as elNilai', 'elNilai.e_laporan_id = el.id', 'left');

		return $this->db->get('satker as s')->row_array();
	}

	function find($id){
		$this->db->where('id', $id);

		return $this->db->get('e_laporan')->row_array();
	}

	function namaSatker($satkerId){
		if($satkerId == '') return '';

		$res = $this->findSatker($satkerId);

		return ($res && $res['nama_singkat']) ? $res['nama_singkat'] : '';
	}

	function add($data){
    	$this->db->insert('e_laporan', $data);

    	return $this->db->insert_id();
    }

	function saveNilai($data){
    	return $this->db->replace('e_laporan_nilai', $data);
    }

    function update($data, $id, $period){
    	return $this->db->update('e_laporan', $data, [
	    	'satker_id' => $id,
	    	'periode' => $period
	    ]);
    }

    function delete($id){
    	return $this->db->delete('e_laporan', array('id' => $id));
    }

    function table($start, $perpage = 0, $columns, $order, $filter = []){

    	$select = [];

    	for($i = 1; $i <= 22; $i++){
    		$select[] = "elNilai.lipa".$i;
    	}

		$this->db->select('SQL_CALC_FOUND_ROWS s.id as satker_id, hari_kerja, el.id as data_id,nama_singkat, nama_pp, tgl_konfirmasi, tgl_telaah, keterangan, ROUND(('.implode('+', $select).') / 22,2) as nilai', false);
		// $this->db->order_by('nama_singkat','ASC');

		if(count($columns) > 0){
			$orderIndex = (int) $order[0]['column'];
			$orderDir = $order[0]['dir'];
			$orderColumnName = $columns[$orderIndex]['data'];

			$this->db->order_by($orderColumnName, $orderDir);
		}

		$where = "";

		if(!isset($filter['pp_id']) || !$filter['pp_id']){
			if(is_array($filter) && isset($filter['bulan']) && isset($filter['tahun'])){
				$bulan = intval($filter['bulan']);
				$tahun = intval($filter['tahun']);

				if($bulan != 0 && $tahun != 0){
					$zBulan = str_pad($filter['bulan'], 2, '0', STR_PAD_LEFT);
					$period = $tahun.'-'.$zBulan;
				}else if($tahun != 0){
					$period = $tahun.'-'.date('m');
				}else{
					$period = date('Y-m');
				}
			}else{
				$period = date('Y-m');
			}

			// $this->db->where('periode', $period);
			// $this->db->or_where('periode is null', null);
			// $this->db->or_where('e_laporan_id is null', null);

			$this->db->join('e_laporan as el', "el.satker_id = s.id AND (`periode` = '$period' OR `periode` is null )", 'left');
			$this->db->join('e_laporan_nilai as elNilai', 'elNilai.e_laporan_id = el.id', 'left');

			$query = $this->db->get('satker as s');
		}else{
			if(isset($filter['tgl_start']) && isset($filter['tgl_end'])){
				$tgl_start = $filter['tgl_start'];
				$tgl_end = $filter['tgl_end'];

				if(validateDate($tgl_start) && validateDate($tgl_end)){
					$tgl_start = toSQLDate($tgl_start);
					$tgl_end = toSQLDate($tgl_end);

					$this->db->where('tgl_telaah >= ', $tgl_start);
					$this->db->where('tgl_telaah <= ', $tgl_end);
				}
			}

			$this->db->where('pp_id', $filter['pp_id']);
			$this->db->where('periode is not null', null);

			$this->db->join('satker as s', 'el.satker_id = s.id', 'left');

			$query = $this->db->get('e_laporan as el');
		}

		return $query->result_array();
	}

	function tableSingkron($start, $perpage = 0, $columns, $order, $filter = []){
		$this->db->select('SQL_CALC_FOUND_ROWS *', false);
		$this->db->order_by('tgl_singkron','desc');

		$where = "";

		if(isset($filter)){
			if(!$filter['pp_id']){
				$this->db->where('pp_id', $filter['pp_id']);
			}

			$tgl_start = $filter['tgl_start'];
			$tgl_end = $filter['tgl_end'];

			if(validateDate($tgl_start) && validateDate($tgl_end)){
				$tgl_start = toSQLDate($tgl_start);
				$tgl_end = toSQLDate($tgl_end);

				$this->db->where('tgl_telaah >= ', $tgl_start);
				$this->db->where('tgl_telaah <= ', $tgl_end);
			}
		}

		$query = $this->db->get('singkronisasi');

			return $query->result_array();
	}

	function total(){
        $query = $this->db->query("SELECT FOUND_ROWS() as cnt;");

        $res = $query->row();

        if($res){
            return (int) $res->cnt;
        }else{
            return 0;
        }
    }
}