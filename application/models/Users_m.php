<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function grup(){
		return $this->db->get('grup')->result_array();
	}

	function jabatan(){
		$this->db->where('aktif', 1);

		return $this->db->get('jabatan')->result_array();
	}

	function namaGrup($grupId){
		if($grupId == '') return '';

		$this->db->where('id', $grupId);

		$res = $this->db->get('grup')->row_array();

		return ($res && $res['nama']) ? $res['nama'] : '';
	}

	function namaJabatan($grupId){
		if($grupId == '') return '';

		$this->db->where('id', $grupId);

		$res = $this->db->get('jabatan')->row_array();

		return ($res && $res['nama_jabatan']) ? $res['nama_jabatan'] : '';
	}

	function any($q){
		$this->db->select('id as id, nama, username');
		$this->db->like('username', $q);
		$this->db->or_like('nama', $q);

		return $this->db->get('users')->result_array();
	}

	function find($userId){
		$this->db->select('users.id, username,users.nama,jabatan_id,users.status,grup_id,grup_text, ht.nama as nama_hakim, p.nama as nama_pp, hakim_id, panitera_id');
		$this->db->join('user_hakim as uh','uh.user_id = users.id','left');
		$this->db->join('hakim_tinggi as ht ',' ht.id = uh.hakim_id','left');
		$this->db->join('user_panitera as up ',' up.user_id = users.id','left');
		$this->db->join('panitera as p ',' p.id = up.panitera_id','left');

		$this->db->where('users.id', $userId);

		return $this->db->get('users')->row_array();
	}

	function nama($hakimId){
		if($hakimId == '') return '';

		$res = $this->find($hakimId);

		return ($res && $res['nama']) ? $res['nama'] : '';
	}

	function table($start, $length, $column, $order){
		// if(isset($columns) && count($columns) > 0){
		// 	$orderIndex = (int) $order[0]['column'];
		// 	$orderDir = $order[0]['dir'];
		// 	$orderColumnName = $columns[$orderIndex]['data'];

		// 	foreach ($columns as $key => $column) {
		// 		$searchArr = $column['search'];
		// 		if($searchArr['value'] != ''){
		// 			switch ($column['data']) {
		// 				case 'id_jabatan':
		// 					$this->db->where('id_jabatan', $searchArr['value']);
		// 					break;
		// 				case 'aktif':
		// 					$this->db->where('aktif', $searchArr['value']);
		// 					break;
		// 				case 'id_gol_ruang':
		// 					$this->db->where('id_gol_ruang', $searchArr['value']);
		// 					break;
		// 				default:
		// 					$this->db->like($column['data'], $searchArr['value']);
		// 					break;
		// 			}
		// 		}
		// 	}

		// 	$this->db->order_by($orderColumnName, $orderDir);
		// }

		$this->db->select('SQL_CALC_FOUND_ROWS *', false);
		$this->db->order_by('id','DESC');

		if(isset($perpage) && $perpage > 0){
			$this->db->limit($perpage, $start);
		}
	
		$query = $this->db->get('users as h');

		return $query->result_array();
	}

	function total(){
        $query = $this->db->query("SELECT FOUND_ROWS() as cnt;");

        $res = $query->row();

        if($res){
            return (int) $res->cnt;
        }else{
            return 0;
        }
    }

    function add($data){
    	$this->db->insert('users', $data);

    	return $this->db->insert_id();
    }

    function saveHakim($data){
    	return $this->db->insert('user_hakim', $data);
    }

    function savePanitera($data){
    	return $this->db->insert('user_panitera', $data);
    }

    function clearRelation($id){ 
    	$this->db->delete('user_hakim', array('user_id' => $id));
    	$this->db->delete('user_panitera', array('user_id' => $id));
    }

    function update($data, $id){
    	return $this->db->update('users', $data, array('id' => $id));
    }

    function delete($id){
    	$this->clearRelation($id);
    	
    	return $this->db->delete('users', array('id' => $id));
    }

    function valid_username($str, $id = false){
    	$this->db->select('count(id) as cnt');
    	$this->db->where('username', $str);

    	if($id){
    		$this->db->where('id !=', $id);
    	}

    	return $this->db->get('users')->row()->cnt == 0;
    }

    function valid_password($str, $id = false){
    	$this->db->select('count(id) as cnt');
    	$this->db->where('password', $str);
    	$this->db->where('id', $id);

    	return $this->db->get('users')->row()->cnt > 0;
    }

    function validateLogin($username, $password){
		  $q = $this->db->query("
		  	SELECT u.id, username, u.nama, jabatan_id, u.grup_text, u.grup_id, uh.hakim_id, up.panitera_id, status, tgl_singkron
 			FROM users as u
			LEFT JOIN user_panitera as up ON up.user_id = u.id AND u.grup_id = 4
				LEFT JOIN user_hakim as uh ON uh.user_id = u.id AND u.grup_id = 3
				LEFT JOIN singkronisasi as s  on up.panitera_id = s.pp_id and tgl_singkron = STR_TO_DATE(now(), '%Y-%m-%d')
		  	where username=? AND password = ?", [ 
		  	$username, $password ]);

		  return $q;
	}
}