<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hakim_m extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}

	function any($q){
		$this->db->select('id as id, nama, nip');
		$this->db->like('nip', $q);
		$this->db->or_like('nama', $q);

		return $this->db->get('hakim_tinggi')->result_array();
	}

	function find($hakimId){
		$this->db->where('id', $hakimId);

		return $this->db->get('hakim_tinggi')->row_array();
	}

	function nama($hakimId){
		if($hakimId == '') return '';

		$res = $this->find($hakimId);

		return ($res && $res['nama']) ? $res['nama'] : '';
	}

	function table($start, $perpage = 0, $columns, $order, $filter = []){
		// if(isset($columns) && count($columns) > 0){
		// 	$orderIndex = (int) $order[0]['column'];
		// 	$orderDir = $order[0]['dir'];
		// 	$orderColumnName = $columns[$orderIndex]['data'];

		// 	foreach ($columns as $key => $column) {
		// 		$searchArr = $column['search'];
		// 		if($searchArr['value'] != ''){
		// 			switch ($column['data']) {
		// 				case 'id_jabatan':
		// 					$this->db->where('id_jabatan', $searchArr['value']);
		// 					break;
		// 				case 'aktif':
		// 					$this->db->where('aktif', $searchArr['value']);
		// 					break;
		// 				case 'id_gol_ruang':
		// 					$this->db->where('id_gol_ruang', $searchArr['value']);
		// 					break;
		// 				default:
		// 					$this->db->like($column['data'], $searchArr['value']);
		// 					break;
		// 			}
		// 		}
		// 	}

		// 	$this->db->order_by($orderColumnName, $orderDir);
		// }

		$this->db->select('SQL_CALC_FOUND_ROWS *', false);
		$this->db->order_by('id','DESC');

		if(isset($perpage) && $perpage > 0){
			$this->db->limit($perpage, $start);
		}
	
		$query = $this->db->get('hakim_tinggi as h');

		return $query->result_array();
	}

	function total(){
        $query = $this->db->query("SELECT FOUND_ROWS() as cnt;");

        $res = $query->row();

        if($res){
            return (int) $res->cnt;
        }else{
            return 0;
        }
    }

    function add($data){
    	return $this->db->insert('hakim_tinggi', $data);
    }

    function update($data, $id){
    	return $this->db->update('hakim_tinggi', $data, array('id' => $id));
    }

    function delete($id){
    	return $this->db->delete('hakim_tinggi', array('id' => $id));
    }

    function valid_nip($str, $id = false){
    	$this->db->select('count(id) as cnt');
    	$this->db->where('nip', $str);

    	if($id){
    		$this->db->where('id !=', $id);
    	}

    	return $this->db->get('hakim_tinggi')->row()->cnt == 0;
    }
}