
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <base href="<?php echo base_url() ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIAP PTA - PTA Jawa Barat</title>
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" rel="stylesheet">
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <!-- <link href="<?php echo base_url() ?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet"> -->
    <!--c3 CSS -->
    <!-- <link href="<?php echo base_url() ?>assets/plugins/c3-master/c3.min.css" rel="stylesheet"> -->
    <!--Toaster Popup message CSS -->
    <!-- <link href="<?php echo base_url() ?>assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet"> -->
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <!-- <link href="<?php echo base_url() ?>assets/css/pages/dashboard1.css" rel="stylesheet"> -->
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url() ?>assets/css/colors/green-dark.css" id="theme" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">

   <!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/datatable.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/custom.css">
<!--     <link href="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-datepicker.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">SIAP PTA</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url() ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url() ?>assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url() ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url() ?>assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item hidden-sm-down"><span></span></li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <!-- <li class="nav-item hidden-xs-down search-box"> <a class="nav-link hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li> -->
                       
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url() ?>assets/images/noimage.png" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            
                                            <div class="u-text">
                                                <h4><?php echo $this->session->userdata('nama') ?></h4>
                                                <p class="text-muted"><?php echo $this->session->userdata('grup') ?></p>
                                        </div>
                                    </li>
                                    <li><a href="<?php echo base_url() ?>settings/users/profile"><i class="ti-user"></i> Atur Akun</a></li>
                                    <li><a href="<?php echo base_url() ?>auth/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- 
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">Menu Utama</li> -->
                        <li> <a class="waves-effect waves-dark" href="<?php echo base_url() ?>" aria-expanded="false"><span class="hide-menu">Dashboard</span></a>
                        </li>
                        
                        <li> <a class="waves-effect waves-dark" href="<?php echo base_url() ?>perkara" aria-expanded="false"><span class="hide-menu">Daftar Perkara</span></a>
                        </li>

                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url() ?>kinerja_majelis" aria-expanded="false"><span class="hide-menu">Kinerja Majelis</span></a>
                        </li>

                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url() ?>kinerja_konseptor" aria-expanded="false"><span class="hide-menu">Kinerja Konseptor</span></a>
                        </li>

                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url() ?>kinerja_pp" aria-expanded="false"><span class="hide-menu">Kinerja PP</span></a>
                        </li>

                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url() ?>e_laporan" aria-expanded="false"><span class="hide-menu">E-Laporan</span></a>
                        </li>
                        
                        <li> <a class="waves-effect waves-dark" href="<?php echo base_url() ?>monitoring_perkara" aria-expanded="false"><span class="hide-menu">Monitoring Perkara</span></a>
                        </li>


                        
                        <?php if(onlyAdmin()){ ?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><span class="hide-menu">Pengaturan </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo base_url() ?>settings/hakim">Hakim Tinggi</a></li>
                                <li><a href="<?php echo base_url() ?>settings/panitera">Panitera/PP</a></li>
                                <li><a href="<?php echo base_url() ?>settings/users">Pengguna</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">