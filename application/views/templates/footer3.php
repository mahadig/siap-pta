            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © <?php
            if(date('Y') == "2020"){
                echo '2020';
            }else{
                echo '2020 - ' . date('Y');
            }
            ?> Pengadilan Tinggi Agama Jawa Barat </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
</div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <!-- <script src="<?php echo base_url() ?>assets/js/waves.js"></script> -->
    <!--Menu sidebar -->
    <script src="<?php echo base_url() ?>assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bundle.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script> -->
    <!--morris JavaScript -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script> -->
    <!--c3 JavaScript -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/d3/d3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/c3-master/c3.min.js"></script> -->
    <!-- Popup message jquery -->
    <!-- <script src="<?php echo base_url() ?>assets/plugins/toast-master/js/jquery.toast.js"></script> -->
    <!-- Chart JS -->


                        <?php 
    if(isset($scripts)){
        foreach ($scripts as $row) { ?>
                <script src="<?php echo base_url().$row ?>" type="text/javascript"></script>
            <?php 
        }
    } ?>

    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    
</body>

</html>