

            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Kinerja Majelis</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Kinerja Majelis</li>
                        </ol>
                    </div>
                </div>


                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="formFilter">
                                    <div class="row filter-period filter-gutters mt-2">
                                        <div class="col-md-5 col-sm-12">
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Penetapan Majelis : </span>
                                                </div>
                                                <input type="text" class="singledate form-control form-control-sm" placeholder="Dari Tgl." autocomplete="off"  id="tgl_start">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">s/d</span>
                                                </div>
                                                <input type="text" placeholder="Sampai Tgl." class="singledate form-control form-control-sm" autocomplete="off" id="tgl_end">


                                            </div> 
                                        </div>
                                        <div class="col-md-auto col-sm-12">
                                            <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
                                        </div>
                                        <div class="col-md-auto b-l col-sm-12">
                                            <button type="button" id="btnExcel" class="btn btn-success btn-sm">Excel</button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                                <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                                <thead>
                                  <tr>
                                    <th class="text-center" rowspan="2">No.</th>
                                    <th class="text-center" rowspan="2">Ketua Majelis</th>
                                    <th class="text-center" rowspan="2">Perkara Diterima</th>
                                    <th class="text-center" rowspan="2">Perkara Selesai</th>
                                    <th class="text-center" colspan="3">Lama Selesai</th>
                                    <th class="text-center" rowspan="2">Kinerja</th>
                                    <th class="text-center" rowspan="2">Sisa<br/>Perkara</th>
                                  </tr>
                                  <tr>
                                    <th class="text-center">1-2 Bln</th>
                                    <th class="text-center">2-3 Bln</th>
                                    <th class="text-center">> 3 Bln</th>  
                                  </tr>
                                </thead>
                              </table>
                            
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->

