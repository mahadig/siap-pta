
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Daftar Panitera/PP</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Daftar Panitera</li>
            </ol>
        </div>
    </div>

    <?php if($this->session->flashdata('success') != null) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Sales overview chart -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <?php if(onlyAdmin()){ ?> 
                <div class="card-body">
                    <a href="<?php echo base_url() ?>settings/panitera/add" class="btn btn-primary btn-sm">Tambah Panitera/PP</a>
                </div>
            <?php } ?>

            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_2">
                <thead>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">NIP</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Aktif</th>
                </tr>
            </thead>
            <tfoot>
              <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NIP</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Status</th>
                <th class="text-center">Aktif</th>
            </tr>
        </tfoot>
    </table>
    
</div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->

