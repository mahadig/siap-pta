
<script type="text/javascript">
    var params = {
        filter: <?php echo json_encode($params) ?>
    }
</script>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">E-Laporan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">E-Laporan</li>
            </ol>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body pb-2">
                    <h3 class="card-title mb-1"><span class="lstick"></span><?php echo $panitera['nama'] ?></h3>
                    <h6 class="card-subtitle2 mb-0"> <?php eissetor($perfText) ?></h6>
                </div>
                <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                    <thead>
                      <tr>
                        <th class="text-center">No.</th>
                        <th class="text-center">Nama Satker</th>
                        <th class="text-center">Penelaah</th>
                        <th class="text-center">Tanggal Konfirmasi</th>
                        <th class="text-center">Tanggal Telaah</th>
                        <th class="text-center">Keterangan</th>
                    </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Nama Satker</th>
                    <th class="text-center">Penelaah</th>
                    <th class="text-center">Tanggal Konfirmasi</th>
                    <th class="text-center">Tanggal Telaah</th>
                    <th class="text-center">Keterangan</th>
                </tr>
            </tfoot>
        </table>

    </div>
</div>
</div>
</div>

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->

