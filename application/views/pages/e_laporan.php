

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">E-Laporan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">E-Laporan</li>
            </ol>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="formFilter">
                        <div class="row filter-gutters">
                            <div class="col-md-auto col-sm-12">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Periode Tgl. Telaah</span>
                                    </div>
                                    <!-- <select class="form-control form-control-sm select2" id="filter_period">
                                        <option value="tgl_register">Register</option>
                                        <option value="tgl_minutasi">Minutasi</option>
                                    </select> -->

                                    <select class="form-control form-control-sm select-tahun select2" name="bulan" id="bulan">
                                        <option value="">Bulan</option>
                                        <?php optionBulan($bulan) ?>
                                    </select>
                                    <!-- <select class="form-control form-control-sm select-tahun select2">
                                    <option value="">2020</option>
                                    </select> -->
                                    <input class="form-control form-control-sm select-tahun text-center" id="tahun" value="<?php echo $tahun ?>" />
                                </div> 
                            </div>

                            <div class="col-md-auto col-sm-12">
                                <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
                            </div>

                            <!-- <div class="col-md-auto b-l col-sm-12">
                                <button type="button" id="btnExcel" class="btn btn-success btn-sm">Excel</button>
                            </div> -->
                            <?php if(accessPP()){ ?>
                                <div class="col-md-auto b-l col-sm-12">
                                    <a href="<?php echo base_url() ?>e_laporan/check_singkronisasi" class="btn <?php echo $this->session->userdata('singkron') == 'Y' ? 'btn-success' : 'btn-danger' ?> btn-sm">Check Singkronisasi</a>
                                </div>
                            <?php } ?>

                        </div>



                    </form>
                </div>
                <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                    <thead>
                      <tr>
                        <th class="text-center">No.</th>
                        <th class="text-center">Nama Satker</th>
                        <th class="text-center">Penelaah</th>
                        <th class="text-center">Hari Kerja</th>
                        <th class="text-center">Tanggal Konfirmasi</th>
                        <th class="text-center">Tanggal Telaah</th>
                        <th class="text-center">Nilai Telaah</th>
                        <th class="text-center">Keterangan</th>
                    </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Nama Satker</th>
                    <th class="text-center">Penelaah</th>
                    <th class="text-center">Hari Kerja</th>
                    <th class="text-center">Tanggal Konfirmasi</th>
                    <th class="text-center">Tanggal Telaah</th>
                    <th class="text-center">Nilai Telaah</th>
                    <th class="text-center">Keterangan</th>
                </tr>
            </tfoot>
        </table>

    </div>
</div>
</div>
</div>

<?php if($this->session->flashdata('successSingkron') != null) { ?>
<script type="text/javascript">
    window.open("http://kabayan.pta-bandung.go.id/e_laporan/imah_panitera");
</script>
<?php } ?>

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->

