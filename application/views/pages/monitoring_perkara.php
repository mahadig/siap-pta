
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Monitoring Perkara</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Daftar Perkara</li>
            </ol>
        </div>
    </div>

    <?php if($this->session->flashdata('success') != null) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?php echo $this->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form id="formFilter">
                    <div class="card-body pb-0">

                        <div class="row filter-gutters">
                            <div class="col-md-2 col-sm-12">
                                <input placeholder="Nomor Perkara PTA" id="nomor_perkara_banding" class="form-control form-control-sm"/>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <input placeholder="Nomor Perkara PA" id="nomor_perkara_pa" class="form-control form-control-sm"/>
                            </div>

                            <div class="col-md-3 col-sm-12">
                                <select class="form-control form-control-sm kt-select2" id="km_id" name="km_id">
                                    <option></option>
                                    <option value="<?php eissetor($perkara['km_id']) ?>" selected="selected"><?php eissetor($perkara['nama_km']) ?></option>
                                </select>
                            </div>

                            <div class="col-md-auto col-sm-12">
                                <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
                            </div>

                        </div>

                    </div>
                    

                    <hr class="mb-0">
                    

                    <table class="table m-0 table-striped- table-bordered table-hover table-checkable" id="kt_table_2">
                        <thead>
                          <tr>
                            <th class="text-center" rowspan="2">No.</th>
                            <th class="text-center" rowspan="2">No. Perkara PTA</th>
                            <th class="text-center" rowspan="2">No. Perkara PA</th>
                            <th class="text-center" rowspan="2">Lama Proses Keseluruhan</th>
                            <th class="text-center" rowspan="2">Proses<br/>Pember<br/>kasan</th>
                            <th class="text-center" rowspan="2">PMH</th>
                            <th class="text-center" rowspan="2">Penunjukan<br/>PP</th>
                            <th class="text-center" colspan="4">Penyelesaian Perkara</th>
                            <th class="text-center" colspan="6">Proses Administrasi</th>

                        </tr>
                        <tr>
                            <th class="text-center">PHS</th>
                            <th class="text-center">Konsep Rangka Putusan</th>
                            <th class="text-center">Lama Sidang</th>
                            <th class="text-center">Minutasi</th>
                            <th class="text-center">Penyerahan ke Panmud</th>
                            <th class="text-center">Pengiriman Berkas ke Satker Pengaju</th>
                            <th class="text-center">Penyerahan ke Meja 3</th>
                            <th class="text-center">Proses Anoni<br/>masi</th>
                            <th class="text-center">Proses Upload</th>
                            <th class="text-center">Proses Arsip</th>
                        </tr>
                    </thead>
                    <!-- <tfoot>
                        <tr>
                            <th class="text-center" rowspan="2">No.</th>
                            <th class="text-center" rowspan="2">No. Perkara PTA</th>
                            <th class="text-center" rowspan="2">No. Perkara PA</th>
                            <th class="text-center" rowspan="2">Lama Proses Keseluruhan</th>
                            <th class="text-center" rowspan="2">Proses<br/>Pember<br/>kasan</th>
                            <th class="text-center" rowspan="2">PMH</th>
                            <th class="text-center" rowspan="2">Penunjukan<br/>PP</th>
                            <th class="text-center" colspan="4">Proses Majelis</th>
                            <th class="text-center" colspan="6">Proses Administrasi</th>

                        </tr>
                        <tr>
                            <th class="text-center">PHS</th>
                            <th class="text-center">Konsep Rangka Putusan</th>
                            <th class="text-center">Lama Sidang</th>
                            <th class="text-center">Minutasi</th>
                            <th class="text-center">Penyerahan ke Panmud</th>
                            <th class="text-center">Pengiriman Berkas ke Satker Pengaju</th>
                            <th class="text-center">Penyerahan ke Meja 3</th>
                            <th class="text-center">Proses Anoni<br/>masi</th>
                            <th class="text-center">Proses Upload</th>
                            <th class="text-center">Proses Arsip</th>
                        </tr>
                    </tfoot> -->
                </table>
            </form>

        </div>
    </div>
</div>
</div>

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->

<template id="filter">
    <div class="row filter-period mt-2">
        <div class="col-md-auto col-sm-12">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Filter</span>
                </div>

                <select class="form-control form-control-sm select2" id="filter_period" name="tanggal">
                    <option value="">Jenis </option>
                    <option value="tgl_register">Register</option>
                    <option value="tgl_minutasi">Minutasi</option>
                </select>

                <input type="text" class="singledate form-control form-control-sm" autocomplete="off" value="01/01/<?php echo date('Y') ?>" id="tgl_start">
                <input type="text" class="singledate form-control form-control-sm" value="<?php echo date('d/m/Y') ?>" autocomplete="off" id="tgl_end">

            </div> 
        </div>
    </div>
</template>