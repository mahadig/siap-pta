

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Rincian Kinerja Perkara</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>perkara">Perkara</a></li>
                <li class="breadcrumb-item active">Kinerja</li>
            </ol>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-between">
                        <div class="col-auto">
                            <h4 class="card-title mb-0"><?php echo issetor($nomor_perkara_banding, '-') ?></h4>
                            <h6 class="mb-2 text-muted"><?php echo $nomor_perkara_pa ?></h6>
                        </div>

                        <div class="col-auto pull-right">
                            

                            <?php if(checkAccess()) { ?>
                                <a href="<?php echo base_url() ?>perkara/edit/<?php echo $perkara_id ?>" class="btn btn-primary btn-sm">Ubah Data Perkara</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="card-body pb-0 pt-0">
                <div class="text-center bg-light b-l b-r b-t">
                    <div class="row">
                        <div class="col-6  p-20 b-r">
                            <h6>Ketua Majelis</h6>
                            <h6><?php echo issetor($nama_km, '-') ?></h6>
                            <!-- <h4 class="m-b-0 font-medium"><?php echo $nama_km ?></h4> -->
                        </div>

                        <div class="col-6  p-20 b-r">
                            <h6>Panitera/PP</h6>
                            <h6><?php echo issetor($nama_pp, '-') ?></h6>
                        </div>
                    </div>
                </div>
                    <div class="text-center b-t bg-light  b-l b-r b-b">
                    <div class="row">
                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                            <h6>Tgl. Register</h6>
                            <h6><?php echo eissetor2(getDateShort($tgl_register), '-') ?></h6>
                            </div>
                        </div>

                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                            <h6>Tgl. Putus</h6>
                            <h6><?php echo eissetor2(getDateShort($tgl_putusan), '-') ?></h6>
                            </div>
                        </div>

                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                            <h6>Tgl. Minutasi</h6>
                            <h6><?php echo eissetor2(getDateShort($tgl_minutasi), '-') ?></h6>
                            </div>
                        </div>

                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                            <h6>Jenis Putus</h6>
                            <h6><?php echo eissetor2($jenis_putus_text, '-') ?></h6>
                            </div>
                        </div>

                    </div>
                </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <div class="card <?php boxColor($proses_all, 60, 90) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Proses Keseluruhan
                                                
                                            </h6>
                                            <h1 class="text-white"><?php echo ($proses_all!= null) ? $proses_all.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_kirim_berkas, ($tabayun == 1 ? 45 : 30), ($tabayun == 1 ? 60 : 45)) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Pengiriman Berkas
                                                
                                            </h6>
                                            <h1 class="text-white"><?php echo ($proses_kirim_berkas!= null) ? $proses_kirim_berkas.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_phs, 3, 7) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Penentuan Hari Sidang
                                            </h6>
                                            <h1 class="text-white"><?php echo ($proses_phs!= null) ? $proses_phs.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                        <div class="col-4">
                            <div class="card <?php boxColor($proses_sidang, 60, 90) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Lama Persidangan
                                                </h6>
                                            <h1 class="text-white"><?php echo ($proses_sidang!= null) ? $proses_sidang.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card <?php boxColor($proses_minut, 1, 5) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Minutasi
                                                </h6>
                                            <h1 class="text-white"><?php echo ($proses_minut!= null) ? $proses_minut.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_serah, 1, 5) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Serah Ke Panmud Banding</h6>
                                            <h1 class="text-white"><?php echo ($proses_serah!= null) ? $proses_serah.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_kirim, 3, 7) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Pengiriman Berkas Ke PA</h6>
                                            <h1 class="text-white"><?php echo ($proses_kirim!= null) ? $proses_kirim.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_meja3, 3, 7) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Serah Meja 3</h6>
                                            <h1 class="text-white"><?php echo ($proses_meja3!= null) ? $proses_meja3.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_anonim, 3, 7) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Anonimasi</h6>
                                            <h1 class="text-white"><?php echo ($proses_anonim!= null) ? $proses_anonim.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card <?php boxColor($proses_upload, 3, 7) ?> text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Upload</h6>
                                            <h1 class="text-white"><?php echo ($proses_upload!= null) ? $proses_upload.' <small>Hari</small>' : '-' ?>
                                        </h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

