<?php

$title = "Tambah Perkara";

$isEdit = false;

if(isset($perkara['perkara_id']) && $perkara['perkara_id']){
    $isEdit = true;
    $title = "Ubah Perkara";
}

if(!accessAdminOperator()){
    $title = "Detil Perkara";
}

?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="post" novalidate autocomplete="off" >

                        <?php if($isEdit){ ?>
                            <div class="mb-3 text-right">

                                <?php if(accessAdminOperator()){ ?> 
                                <input type="submit" name="submit" class="btn btn-sm btn-primary" value="Simpan">
                                <?php } ?>

                                <a href="<?php echo base_url() ?>perkara/kinerja/<?php echo $perkara['perkara_id'] ?>" class="btn btn-secondary btn-sm">Lihat Kinerja</a>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-md-12 customForm">
                                <div class="form-group row <?php echo form_error('nomor_perkara_banding') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-4">Nomor Perkara PTA</label>
                                    <div class="col-8">
                                    
                                        <?php if(accessAdminOperator()){ ?>
                                            <div class="controls">
                                                <input type="text" value="<?php eissetor($perkara['nomor_perkara_banding']) ?>" class="form-control form-control-sm" id="nomor_perkara_banding" name="nomor_perkara_banding">

                                                <?php if(form_error('nomor_perkara_banding') != null) { ?> 
                                                <div class="help-block error-block"><?php echo form_error('nomor_perkara_banding') ?></div>
                                                <?php } ?>
                                            </div>
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['nomor_perkara_banding']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group row <?php echo form_error('nomor_perkara_pa') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-4">Nomor Perkara PA</label>
                                    <div class="col-8">
                                        
                                        <?php if(accessAdminOperator()){ ?>
                                            <div class="controls"><input type="text" value="<?php eissetor($perkara['nomor_perkara_pa']) ?>" required data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm" name="nomor_perkara_pa"></div>
                                            
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['nomor_perkara_pa']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>

                                        <?php if(form_error('nomor_perkara_pa') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('nomor_perkara_pa') ?></div>
                                            <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Nama Pembanding</label>
                                    <div class="col-8">
                                        <?php if(accessAdminOperator()){ ?>
                                            <div class="controls">
                                                <input type="text" value="<?php eissetor($perkara['nama_pembanding']) ?>" class="form-control form-control-sm" required data-validation-required-message="Kolom ini wajib diisi" name="nama_pembanding">
                                            </div>
                                            
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['nama_pembanding']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>

                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Nama Terbanding</label>
                                    <div class="col-8">
                                        <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                            <input type="text" value="<?php eissetor($perkara['nama_terbanding']) ?>" class="form-control form-control-sm"  required data-validation-required-message="Kolom ini wajib diisi" name="nama_terbanding">
                                        </div>
                                            
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['nama_terbanding']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>

                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Nama PA Asal</label>
                                    <div class="col-8">
                                        <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                            <input type="text" value="<?php eissetor($perkara['nama_satker']) ?>" required data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm" name="nama_satker">
                                        </div>
                                            
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['nama_satker']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                <label class="control-label text-right col-4">Tabayun</label>
                                    <div class="col-8">
                                        <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                <input type="checkbox" name="tabayun" value="1" id="basic_checkbox_2" class="filled-in" <?php echo isset($perkara['tabayun']) && $perkara['tabayun'] == 1 ? 'checked' : '' ?> />
                                                <label class="mt-2" for="basic_checkbox_2">Ya</label>
                                                </div>
                                            
                                        <?php }else{ ?>
                                            <input type="text" value="<?php echo (isset($perkara['tabayun']) && $perkara['tabayun'] == 1) ? "Ya" : "Tidak"; ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Tanggal Mohon Banding</label>
                                    <div class="col-4">

                                        <?php if(accessAdminOperator()){ ?>
                                            <div class="controls">
                                                <div class='input-group'>
                                                    <input type='text' value="<?php eissetor($perkara['tgl_mohon_banding']) ?>" name="tgl_mohon_banding"  required data-validation-required-message="Kolom ini wajib diisi" id="tgl_mohon_banding" class="form-control singledate form-control-sm" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <span class="ti-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }else{ ?>
                                            <input type="text" value="<?php eissetor($perkara['tgl_mohon_banding']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        <?php } ?>

                                        
                                    </div>
                                </div>

                                <?php if($isEdit){ ?>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Penerimaan Berkas</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_kirim_berkas']) ?>" name="tgl_kirim_berkas"   id="tgl_kirim_berkas" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_kirim_berkas']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Berkas Kembali (KL)</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['berkas_kembali_kl']) ?>" name="berkas_kembali_kl"   id="berkas_kembali_kl" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['berkas_kembali_kl'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Register</label>
                                        <div class="col-4">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_register']) ?>" name="tgl_register"   id="tgl_register" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_register'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Penetapan Majelis</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_penetapan_majelis']) ?>" name="tgl_penetapan_majelis" id="tgl_penetapan_majelis" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_penetapan_majelis'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Penunjukan PP</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_penunjukan_pp']) ?>" name="tgl_penunjukan_pp" id="tgl_penunjukan_pp" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_penunjukan_pp'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal PHS</label>
                                        <div class="col-4">
                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_phs']) ?>" name="tgl_phs"  id="tgl_phs" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_phs'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Terima Berkas PP</label>
                                        <div class="col-4">
                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_terima_berkas_pp']) ?>" name="tgl_terima_berkas_pp"  id="tgl_terima_berkas_pp" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_terima_berkas_pp'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Kerangka Konsep</label>
                                        <div class="col-4">
                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_kirim_konsep']) ?>" name="tgl_kirim_konsep"  id="tgl_kirim_konsep" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_kirim_konsep'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Sidang Pertama</label>
                                        <div class="col-4">

                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_sidang_pertama']) ?>" name="tgl_sidang_pertama"   id="tgl_sidang_pertama" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_sidang_pertama'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Putusan Sela</label>
                                        <div class="col-4">

                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_putusan_sela']) ?>" name="tgl_putusan_sela"   id="tgl_putusan_sela" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_putusan_sela'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Putusan</label>
                                        <div class="col-4">

                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_putusan']) ?>" name="tgl_putusan"   id="tgl_putusan" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_putusan'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Jenis Putus</label>
                                        <div class="col-4">

                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <select name="jenis_putus_id" id="jenis_putusan" class="form-control form-control-sm kt-select2">
                                                        <option value="">Jenis Putusan</option>
                                                            <?php foreach ($jenis_putusan as $row) {

                                                                $selected = ($row['id'] == $perkara['jenis_putus_id']) ? "selected" : '';

                                                                echo "<option ".$selected." value='".$row['id']."'>".$row['jenis_putusan']."</option>";
                                                            } ?>
                                                        </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['jenis_putus_text'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Minutasi</label>
                                        <div class="col-4">

                                            <?php if(accessMajelis()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_minutasi']) ?>" name="tgl_minutasi"   id="tgl_minutasi" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_minutasi'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Diserahkan Ke Panmud Banding</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_serah_panmud']) ?>" name="tgl_serah_panmud"   id="tgl_serah_panmud" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_serah_panmud'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tgl pengiriman berkas ke PA Asal</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_kirim_pa']) ?>" name="tgl_kirim_pa"   id="tgl_kirim_pa" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_kirim_pa'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal diserahkan ke Meja 3</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_serah_meja3']) ?>" name="tgl_serah_meja3"   id="tgl_serah_meja3" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_serah_meja3'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Upload</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_upload']) ?>" name="tgl_upload"   id="tgl_upload" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_upload'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Anonimasi</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_anonimasi']) ?>" name="tgl_anonimasi"   id="tgl_anonimasi" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_anonimasi'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Tanggal Arsip</label>
                                        <div class="col-4">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <div class='input-group'>
                                                        <input type='text' value="<?php eissetor($perkara['tgl_arsip']) ?>" name="tgl_arsip"   id="tgl_arsip" class="form-control singledate form-control-sm" />
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <span class="ti-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['tgl_arsip'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Ketua Majelis</label>
                                        <div class="col-8">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="km_id" name="km_id">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['km_id']) ?>" selected="selected"><?php eissetor($perkara['nama_km']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['nama_km'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Hakim Anggota 1</label>
                                        <div class="col-5">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="hakim_1" name="hakim_1">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['hakim_1']) ?>" selected="selected"><?php eissetor($perkara['hakim_1_nama']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['hakim_1_nama'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                        <div class="col-3">
                                            <div class="controls">
                                                <input type="checkbox" name="resume_1" value="1" id="resume_1" class="filled-in" <?php echo isset($perkara['resume_1']) && $perkara['resume_1'] == 1 ? 'checked' : '' ?> />
                                                <label class="mt-2" for="resume_1">Resume Perkara</label>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Hakim Anggota 2</label>
                                        <div class="col-5">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="hakim_2" name="hakim_2">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['hakim_2']) ?>" selected="selected"><?php eissetor($perkara['hakim_2_nama']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['hakim_2_nama'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                        <div class="col-3">
                                            <div class="controls">
                                                <input type="checkbox" name="resume_2" value="1" id="resume_2" class="filled-in" <?php echo isset($perkara['resume_2']) && $perkara['resume_2'] == 1 ? 'checked' : '' ?> />
                                                <label class="mt-2" for="resume_2">Resume Perkara</label>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Hakim Anggota 3</label>
                                        <div class="col-5">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="hakim_3" name="hakim_3">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['hakim_3']) ?>" selected="selected"><?php eissetor($perkara['hakim_3_nama']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['hakim_3_nama'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                        <div class="col-3">
                                            <div class="controls">
                                                <input type="checkbox" name="resume_3" value="1" id="resume_3" class="filled-in" <?php echo isset($perkara['resume_3']) && $perkara['resume_3'] == 1 ? 'checked' : '' ?> />
                                                <label class="mt-2" for="resume_3">Resume Perkara</label>
                                                </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Hakim Anggota 4</label>
                                        <div class="col-5">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="hakim_4" name="hakim_4">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['hakim_4']) ?>" selected="selected"><?php eissetor($perkara['hakim_4_nama']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['hakim_4_nama'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                        <div class="col-3">
                                            <div class="controls">
                                                <input type="checkbox" name="resume_4" value="1" id="resume_3" class="filled-in" <?php echo isset($perkara['resume_4']) && $perkara['resume_4'] == 1 ? 'checked' : '' ?> />
                                                <label class="mt-2" for="resume_4">Resume Perkara</label>
                                                </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Konseptor</label>
                                        <div class="col-8">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="konseptor_id" name="konseptor_id">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['konseptor_id']) ?>" selected="selected"><?php eissetor($perkara['nama_konseptor']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['nama_konseptor'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Paniter/PP</label>
                                        <div class="col-8">
                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <select class="form-control form-control-sm kt-select2" id="pp_id" name="pp_id">
                                                    <option></option>
                                                    <option value="<?php eissetor($perkara['pp_id']) ?>" selected="selected"><?php eissetor($perkara['nama_pp']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['nama_pp'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-4">Keterangan</label>
                                        <div class="col-8">

                                            <?php if(accessAdminOperator()){ ?>
                                                <div class="controls">
                                                    <textarea class="form-control" name="keterangan" rows="3"><?php eissetor($perkara['keterangan']) ?></textarea>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="controls">
                                                    <textarea class="form-control-plaintext form-control-sm" rows="3"><?php eissetor($perkara['keterangan'], '-') ?></textarea>
                                                </div>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>



                                <?php } ?>

                                <?php if(accessAdminOperator() || accessMajelis() || accessAdminOperator()){ ?>
                                    <div class="text-right mt-3">
                                        <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
