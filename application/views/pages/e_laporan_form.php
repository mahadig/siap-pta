<?php

$title = "Telaah E-Laporan";

$isEdit = false;

if(isset($row['id'])){
    $isEdit = true;
    $title = "Telaah E-Laporan";
}

?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="post" novalidate autocomplete="off" >
                        <div class="row">
                            <div class="col-md-12 customForm">
                                <div class="form-group row">
                                    <label class="control-label text-right col-4">
                                        Nama Satker
                                    </label>
                                    <div class="col-8">
                                        <div class="controls">
                                            <input type="text" value="<?php eissetor($row['nama_singkat']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">
                                        Periode Telaah
                                    </label>
                                    <div class="col-8">
                                        <div class="controls">
                                            <input type="text" value="<?php echo $period ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">
                                        Nama Penelaah
                                    </label>
                                    <div class="col-8">
                                        <div class="controls">
                                            <select class="form-control form-control-sm kt-select2" id="pp_id" name="pp_id">
                                                <option></option>
                                                <option value="<?php eissetor($row['pp_id']) ?>" selected="selected"><?php eissetor($row['nama_pp']) ?></option>
                                            </select>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Hari Kerja</label>
                                    <div class="col-4">

                                        <div class="controls">
                                            <input type='number' value="<?php eissetor($row['hari_kerja']) ?>" name="hari_kerja" class="form-control form-control-sm" />
                                        </div>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Tanggal Konfirmasi</label>
                                    <div class="col-4">

                                        <div class="controls">
                                            <div class='input-group'>
                                                <input type='text' value="<?php eissetor($row['tgl_konfirmasi']) ?>" name="tgl_konfirmasi" id="tgl_konfirmasi" class="form-control singledate form-control-sm" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="ti-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Tanggal Telaah</label>
                                    <div class="col-4">

                                        <div class="controls">
                                            <div class='input-group'>
                                                <input type='text' value="<?php eissetor($row['tgl_telaah']) ?>" name="tgl_telaah" id="tgl_telaah" class="form-control singledate form-control-sm" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <span class="ti-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">Keterangan</label>
                                    <div class="col-8">

                                        <div class="controls">
                                            <textarea class="form-control" name="keterangan" rows="3"><?php eissetor($row['keterangan']) ?></textarea>
                                        </div>


                                    </div>
                                </div>

                                <input type="submit" name="submit" class="btn btn-success mt-3" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
