<?php

$title = "Penilaian Telaah E-Laporan";

$isEdit = false;

if(isset($row['id'])){
    $isEdit = true;
    $title = "Penilaian Telaah E-Laporan";
}

?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <?php if($this->session->flashdata('success') != null) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <?php } ?>
                    <form method="post" novalidate autocomplete="off" >
                        <div class="row">
                            <div class="col-md-12 customForm">


                                <div class="form-group row">
                                    <label class="control-label text-right col-4">
                                        Nama Satker
                                    </label>
                                    <div class="col-8">
                                        <div class="controls">
                                            <input type="text" value="<?php eissetor($row['nama_singkat']) ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-4">
                                        Periode Telaah
                                    </label>
                                    <div class="col-8">
                                        <div class="controls">
                                            <input type="text" value="<?php echo $period ?>" readonly class="form-control-plaintext form-control-sm"/>
                                        </div>
                                    </div>
                                </div>

                                <?php for($i = 1; $i <= 22;$i++){ ?>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-4"><?php echo "Nilai Lipa $i" ?></label>
                                        <div class="col-4">

                                            <div class="controls">
                                                <input type="text" class="form-control form-control-sm" 
                                                value="<?php echo $row['lipa'.$i] ?>"
                                                name="nilai[lipa<?php echo $i ?>]">
                                            </div>


                                        </div>
                                    </div>
                                <?php } ?>

                                <input type="submit" name="submit" class="btn btn-success mt-3" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
