
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Dasbor</h3>
        </div>
        <div class="col-md-7">
            <form id="formFilter">
                <div class="row filter-period filter-gutters justify-content-end mt-2">
                    <div class="col-md-auto col-sm-12">
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Filter</span>
                            </div>

                            <select class="form-control form-control-sm select2" id="filter_period" name="tanggal">
                                <option value="tgl_minutasi">Minutasi</option>
                                <option value="tgl_register">Register</option>
                            </select>

                            <input type="text" placeholder="Dari Tgl." value="01/01<?php echo date('/Y') ?>" class="singledate form-control form-control-sm" autocomplete="off" id="tgl_start">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">s/d</span>
                            </div>
                            <input type="text" placeholder="Sampai Tgl." value="<?php echo date('d/m/Y') ?>" class="singledate form-control form-control-sm" autocomplete="off" id="tgl_end">

                        </div> 

                    </div>

                    <div class="col-md-auto col-sm-12">
                            <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
                        </div>
                </div>
            </form>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row filter-gutters">
        <div class="col-lg-3">
            <div class="card bg-danger text-white mb-2">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="stats">
                            <h1 class="text-white" id="p_beban">0</h1>
                            <h6 class="text-white">Beban Perkara</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card bg-success-dark text-white mb-2">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="stats">
                            <h1 class="text-white" id="p_selesai">0</h1>
                            <h6 class="text-white">Perkara Selesai</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card bg-dark text-white mb-2">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="stats">
                            <h1 class="text-white" id="p_sisa">0</h1>
                            <h6 class="text-white">Sisa Perkara</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card bg-info text-white mb-2">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="stats">
                            <h1 class="text-white" id="p_persentase">0%</h1>
                            <h6 class="text-white">Persentase</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Stats box -->
    <!-- ============================================================== -->
    <div class="row filter-gutters">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div>
                            <h3 class="card-title m-b-5"><span class="lstick"></span>Statistik Berdasarkan Jenis Putus </h3>
                        </div>
                        <!-- <div class="ml-auto">
                            <select class="custom-select b-0">
                                <option selected="">January 2017</option>
                                <option value="1">February 2017</option>
                                <option value="2">March 2017</option>
                                <option value="3">April 2017</option>
                            </select>
                        </div> -->
                    </div>
                </div>

                
                <div class="card-body">
                    <canvas id="chart1" height="150"></canvas>
                </div>

                <div class="bg-dark stats-bar">
                    <div class="row filter-gutters">
                        <div class="col-lg-3 col-md-3">
                            <div class="py-3 text-center active">
                                <h6 class="text-white">Dikuatkan</h6>
                                <h3 id="st_1" class="text-white m-b-0">0</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="py-3 text-center">
                                <h6 class="text-white">Dibatalkan</h6>
                                <h3 id="st_2" class="text-white m-b-0">0</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="py-3 text-center">
                                <h6 class="text-white">Tidak dapat diterima</h6>
                                <h3 id="st_3" class="text-white m-b-0">0</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="py-3 text-center">
                                <h6 class="text-white">Dicabut</h6>
                                <h3 id="st_4" class="text-white m-b-0">0</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- visit charts-->

        <!-- ============================================================== -->
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><span class="lstick"></span>Informasi</h4>

                    <div class="list-group informasi">

                        <a href="<?php echo base_url() ?>perkara/lists?type=selesai&perf=green&list=kinerja_majelis&dsb&" class="list-group-item bg-success text-white d-flex justify-content-between align-items-center">
                            Perkara Selesai 1-2 Bulan
                            <span id="p_green" class="badge badge-dark p-2">14</span>
                        </a>

                        <a href="<?php echo base_url() ?>perkara/lists?type=selesai&perf=yellow&list=kinerja_majelis&dsb&" class="list-group-item bg-warning text-white d-flex justify-content-between align-items-center">
                            Perkara Selesai 2-3 Bulan
                            <span id="p_yellow" class="badge badge-dark p-2">14</span>
                        </a>

                        <a href="<?php echo base_url() ?>perkara/lists?type=selesai&perf=red&list=kinerja_majelis&dsb&" class="list-group-item bg-danger text-white d-flex justify-content-between align-items-center">
                            Perkara Selesai > 3 Bulan
                            <span id="p_red" class="badge badge-dark p-2">14</span>
                        </a>

                        <a href="<?php echo base_url() ?>perkara/lists?type=belum_minut&list=kinerja_majelis&dsb&" class="list-group-item text-dark bg-light d-flex justify-content-between align-items-center">
                            Perkara Belum Minutasi
                            <span id="p_belum_minut" class="badge badge-dark p-2">14</span>
                        </a>

                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->

    </div>
</div>