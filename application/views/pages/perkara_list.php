<script type="text/javascript">
    var params = {
        filter: <?php echo json_encode($params) ?>
    }
</script>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>

    <?php if($this->session->flashdata('success') != null) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <?php echo $this->session->flashdata('success'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Sales overview chart -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body pb-2">
                <?php if(isset($hakim) && $hakim){ ?>
        <h3 class="card-title mb-1"><span class="lstick"></span><?php echo $hakim['nama'] ?></h3>
        <?php } ?>
                
                <h6 class="card-subtitle2 mb-0"> <?php eissetor($perfText) ?></h6>
            </div>
            <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                <thead>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">No. Perkara PTA</th>
                    <th class="text-center">No. Perkara PA</th>
                    <th class="text-center">Pebanding</th>
                    <th class="text-center">Terbanding</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tfoot>
              <tr>
                <th class="text-center">No.</th>
                <th class="text-center">No. Perkara PTA</th>
                <th class="text-center">No. Perkara PA</th>
                <th class="text-center">Pebanding</th>
                <th class="text-center">Terbanding</th>
                <th class="text-center">Aksi</th>
            </tr>
        </tfoot>
    </table>

</div>
</div>
</div></div>

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->

<template id="filter">
    <form id="formFilter">
    <div class="row filter-gutters mt-2">
        <div class="col-auto">
            <input placeholder="Nomor Perkara PTA" id="nomor_perkara_banding" class="form-control form-control-sm"></input>
        </div>
        <div class="col-auto">
            <input placeholder="Nomor Perkara PA" id="nomor_perkara_pa" class="form-control form-control-sm"></input>
        </div>

        <div class="col-1">
            <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
        </div>
    </div>
</form>
</template>
