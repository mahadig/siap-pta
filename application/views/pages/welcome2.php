
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Dasbor</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active">Dasbor</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h3 class="card-title m-b-5"><span class="lstick"></span>Keadaan Perkara </h3>
                                        <h6 class="card-subtitle mb-0">Tahun <?php echo date('Y') ?></h6></div>
                                    <div class="ml-auto">
                                        <ul class="list-inline mb-0">
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-primary m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0"><?php echo $overview->jml ?></h2>
                                                        <h6 class="text-muted">Beban Perkara</h6></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-success m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0"><?php echo $overview->selesai ?></h2>
                                                        <h6 class="text-muted">Perkara Selesai</h6></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-warning m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0"><?php echo $overview->sisa ?></h2>
                                                        <h6 class="text-muted">Sisa</h6></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="d-flex">
                                                    <i class="fa fa-circle font-10 m-r-10 text-muted m-t-10"></i>
                                                    <div>
                                                        <h2 class="m-b-0"><?php echo $overview->rasio + 0 ?>%</h2>
                                                        <h6 class="text-muted">Persentase</h6></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Stats box -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12">
<div class="card">
    <input type="hidden" id="tahun">
    <input type="hidden" id="bulan">
                            <div class="card-body">
                                <div>
                                        <h3 class="card-title m-b-5"><span class="lstick"></span>Kinerja Majelis </h3>
                                        <h6 class="card-subtitle mb-0">Berdasarkan Tanggal Minutasi Tahun <?php echo date('Y') ?></h6></div>
                                </div>
                                <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                                <thead>
                                  <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Ketua Majelis</th>
                                    <th class="text-center">Perkara Selesai</th>
                                    <th class="text-center">1-2 Bln</th>
                                    <th class="text-center">2-3 Bln</th>
                                    <th class="text-center">> 3 Bln</th>
                                    <th class="text-center">Kinerja</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                  <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Ketua Majelis</th>
                                    <th class="text-center">Perkara Selesai</th>
                                    <th class="text-center">1-2 Bln</th>
                                    <th class="text-center">2-3 Bln</th>
                                    <th class="text-center">> 3 Bln</th>
                                    <th class="text-center">Kinerja</th>
                                  </tr>
                                </tfoot>
                              </table>
                            
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->

            </div>