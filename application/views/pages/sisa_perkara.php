
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Sisa Perkara</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Daftar Perkara</li>
                        </ol>
                    </div>
                </div>

<?php if($this->session->flashdata('success') != null) { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
  <?php echo $this->session->flashdata('success'); ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
        <?php } ?>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            

                                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_2">
                                <thead>
                                  <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Majelis</th>
                                    <th class="text-center">Beban Perkara</th>
                                    <th class="text-center">Selesai</th>
                                    <th class="text-center">Sisa</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                  <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Majelis</th>
                                    <th class="text-center">Beban Perkara</th>
                                    <th class="text-center">Selesai</th>
                                    <th class="text-center">Sisa</th>
                                  </tr>
                                </tfoot>
                              </table>
                            
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->

