

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Rincian Kinerja Majelis</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>/kinerja_majelis">Kinerja Majelis</a></li>
                <li class="breadcrumb-item active">Rincian</li>
            </ol>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="p-l-20">
                            <h4 class="card-title mb-0"><?php echo $majelis['nama'] ?></h4>
                            <h6 class="mb-2 text-muted">NIP. <?php echo $majelis['nip'] ?></h6>
                        </div>
                    </div>
                </div>

                <div class="card-body pb-0 pt-0">
                <div class="text-center bg-light b-l b-r b-t b-b">
                    <div class="row">
                        <div class="col-3  p-20 b-r">
                            <h6>Beban Perkara</h6>
                            <h4 class="m-b-0 font-medium"><?php echo $beban ?></h4>
                        </div>

                        <div class="col-3  p-20 b-r">
                            <h6>Perkara Selesai</h6>
                            <h4 class="m-b-0 font-medium"><?php echo $k_all['total'] ?></h4>
                        </div>

                        <div class="col-3  p-20 b-r">
                            <h6>Sisa Perkara</h6>
                            <h4 class="m-b-0 font-medium"><?php echo $beban - $k_all['total'] ?></h4>
                        </div>

                        <div class="col-3  p-20">
                            <h6>Rasio Penyelesaian</h6>
                            <h4 class="m-b-0 font-medium"><?php echo round((float) ($k_all['total'] / $beban) * 100, 2) ?>%</h4>
                        </div>
                    </div>
                </div>
                    <div class="text-center mt-3 b-t bg-light  b-l b-r b-b">
                    <div class="row">
                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                            <h6>1 - 2 Bulan</h6>
                            <h4 class="m-b-0 font-medium"><?php echo $k_all['hijau'] ?></h4>
                            </div>
                        </div>

                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                                <h6>2 - 3 Bulan</h6>
                                <h4 class="m-b-0 font-medium"><?php echo $k_all['kuning'] ?></h4>
                            </div>
                        </div>

                        <div class="col-3 b-r">
                            <div class=" p-3 ">
                                <h6>> 3 Bulan</h6>
                                <h4 class="m-b-0 font-medium"><?php echo $k_all['merah'] ?></h4>
                            </div>
                        </div>

                        <div class="col-3 ">
                            <div class=" p-3 ">
                                <h6>Kinerja</h6>
                                <h4 class="m-b-0 font-medium"><?php echo $k_all['kinerja'] + 0 ?>%</h4>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Pengiriman Berkas
                                                <br/><small>Rata-Rata <?php echo $k_kirimBerkas['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_kirimBerkas['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Penentuan Hari Sidang
                                                <br/><small>Rata-Rata <?php echo $k_phs['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_phs['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Lama Persidangan
                                                <br/><small>Rata-Rata <?php echo $k_sidang['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_sidang['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Minutasi
                                                <br/><small>Rata-Rata <?php echo $k_minut['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_minut['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Serah Ke Panmud Banding

                                                <br/><small>Rata-Rata <?php echo $k_serah['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_serah['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Pengiriman Berkas Ke PA

                                                <br/><small>Rata-Rata <?php echo $k_kirim['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_kirim['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Serah Meja 3

                                                <br/><small>Rata-Rata <?php echo $k_meja3['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_meja3['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Anonimasi

                                                <br/><small>Rata-Rata <?php echo $k_anonim['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_anonim['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card bg-info text-white">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="stats">
                                            <h6 class="text-white">Upload

                                                <br/><small>Rata-Rata <?php echo $k_upload['avg'] ?> hari</small>
                                            </h6>
                                            <h1 class="text-white"><?php echo $k_upload['kinerja'] + 0 ?>%</h1>
                                        </div>
                                        <div class="stats-icon text-right ml-auto"><i class="mdi mdi-send-clock display-5 op-3 text-dark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

