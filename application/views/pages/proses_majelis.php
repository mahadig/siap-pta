

            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">Proses Majelis</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Proses Majelis</li>
                        </ol>
                    </div>
                </div>


                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="formFilter">
                                    <div class="row filter-period filter-gutters mt-2">
                                        <div class="col-md-5 col-sm-12">
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Periode Penetapan PP</span>
                                                </div>
                                                <input type="text" placeholder="Dari Tgl." class="singledate form-control form-control-sm" autocomplete="off" id="tgl_start">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">s/d</span>
                                                </div>
                                                <input type="text" placeholder="Sampai Tgl." class="singledate form-control form-control-sm" autocomplete="off" id="tgl_end">

                                            </div> 
                                        </div>
                                        <div class="col-md-auto col-sm-12">
                                            <button class="btn btn-sm btn-dark" type="reset" id="btnClear">Clear</button>
                                        </div>
                                        <!-- <div class="col-md-auto b-l col-sm-12">
                                            <button type="button" id="btnExcel" class="btn btn-success btn-sm">Excel</button>
                                        </div> -->
                                    </div>
                                </form>
                                </div>
                                <table class="table table-striped- table-bordered table-hover table-checkable" style="width: 100%" id="kt_table_2">
                                <thead>
                                  <tr>
                                    <th class="text-center" rowspan="2">No.</th>
                                    <th class="text-center" rowspan="2">Nama PP</th>
                                    <th class="text-center" colspan="3">Perkara</th>
                                    <th class="text-center" colspan="4">Konsep Rangka Putusan</th>
                                    <th class="text-center" colspan="4">Minutasi</th>
                                    <!-- <th class="text-center" colspan="4">Kinerja</th> -->
                                  </tr>
                                  <tr>
                                    <th class="text-center">Diterima</th>
                                    <th class="text-center">Selesai</th>
                                    <th class="text-center">Sisa</th>
                                    <th class="text-center">0 Hari</th>
                                    <th class="text-center">1-3 Hari</th>
                                    <th class="text-center">4-7 Hari</th>
                                    <th class="text-center">> 7 Hari</th>
                                    <th class="text-center">0 Hari</th>
                                    <th class="text-center">1-3 Hari</th>
                                    <th class="text-center">4-7 Hari</th>
                                    <th class="text-center">> 7 Hari</th>
                                  </tr>
                                </thead>
                                
                              </table>
                            
                        </div>
                    </div>
                </div>
</div>
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->

