<?php

$title = "Tambah Pengguna";

$isEdit = false;

if(isset($user['id'])){
    $isEdit = true;
    $title = "Ubah Pengguna";
}

if($isProfile){
    $title = "Pengaturan Akun";
}

?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

<?php if($this->session->flashdata('success') != null) { ?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <?php echo $this->session->flashdata('success'); ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<?php } ?>

                    <form method="post" novalidate autocomplete="off" >
                        <div class="row">
                            <div class="col-md-12 customForm">
                                <div class="form-group row <?php echo form_error('username') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-3">Username</label>
                                    <div class="col-9">
                                        <div class="controls">
                                            <input type="text" autocomplete="new-password" value="<?php eissetor($user['username']) ?>" required data-validation-required-message="Kolom ini wajib diisi"  class="form-control form-control-sm" id="nomor_perkara_banding" name="username">

                                            <?php if(form_error('username') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('username') ?></div>
                                            <?php } ?>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="form-group row <?php echo form_error('password') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-3">
                                    <?php echo $isProfile ? "Password Lama" : "Password" ?>
                                </label>
                                    <div class="col-9">
                                        <div class="controls"><input  autocomplete="new-password" type="password" <?php echo ($isEdit) ? '' : 'required data-validation-required-message="Kolom ini wajib diisi" ' ?> class="form-control form-control-sm" name="password">
                                        <?php if($isEdit) { ?> 
                                            <small class="form-control-feedback"> Kosongkan jika tidak ingin mengganti password</small>
                                            <?php } ?>

                                            <?php if(form_error('password') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('password') ?></div>
                                            <?php } ?>
                                    </div>

                                    </div>
                                </div>

                                <?php if($isProfile){ ?>
                                <div class="form-group row <?php echo form_error('password2') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-3">Password Baru</label>
                                    <div class="col-9">
                                        <div class="controls"><input value="<?php eissetor($user['password2']) ?>" autocomplete="new-password" type="password" <?php echo ($isEdit) ? '' : 'required data-validation-required-message="Kolom ini wajib diisi" ' ?> class="form-control form-control-sm" name="password2">
                                        <?php if(form_error('password2') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('password2') ?></div>
                                            <?php } ?>
                                    </div>

                                    </div>
                                </div>

                                <div class="form-group row <?php echo form_error('password2_confirm') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-3">Ulangi Password Baru</label>
                                    <div class="col-9">
                                        <div class="controls"><input value="<?php eissetor($user['password2_confirm']) ?>" autocomplete="new-password" type="password" <?php echo ($isEdit) ? 'data-validation-match-match="password2" data-validation-match-message = "Password Baru tidak sama"' : 
                                        'required data-validation-required-message="Kolom ini wajib diisi" ' ?> class="form-control form-control-sm" name="password2_confirm">
                                        <?php if(form_error('password2_confirm') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('password2_confirm') ?></div>
                                            <?php } ?>
                                    </div>

                                    </div>
                                </div>
                                <?php } ?>

                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Nama Lengkap</label>
                                    <div class="col-9">
                                        <div class="controls"><input type="text"  value="<?php eissetor($user['nama']) ?>" required data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm" name="nama"></div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Jabatan</label>
                                    <div class="col-9">
                                        <div class="controls">

                                        <select name="jabatan_id"  style="width: 100%" id="jabatan_id" class="form-control form-control-sm select2">
                                            <option value="">Pilih Jabatan</option>
                                            <?php
                                            foreach ($jabatan as $item) {
                                                $selected = "";
                                                if($item['id'] == $user['jabatan_id']) $selected = "selected";

                                            echo '<option '.$selected.' value="'.$item['id'].'">'.$item['nama_jabatan'].'</option>';
                                            }
                                            ?>
                                        </select>

                                        </div>
                                    </div>
                                </div>

                                <?php if(onlyAdmin() && !$isProfile){ ?> 
                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Grup</label>
                                    <div class="col-9">
                                        <select name="grup_id"  style="width: 100%" id="grup" class="form-control form-control-sm select2">
                                            <option value="">Pilih Grup Pengguna</option>
                                            <?php
                                            foreach ($grup as $item) {
                                                $selected = "";
                                                if($item['id'] == $user['grup_id']) $selected = "selected";

                                            echo '<option '.$selected.' value="'.$item['id'].'">'.$item['nama'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row <?php echo !isset($user['grup_id']) || $user['grup_id'] != 3 ? 'hide' : '' ?>" id="hakim_block">
                                        <label class="control-label text-right col-3">Ketua Majelis</label>
                                        <div class="col-9">
                                            <?php if(checkAccess()){ ?>
                                                <div class="controls">
                                                    <select  style="width: 100%" data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm kt-select2" id="km_id" name="hakim_id">
                                                    <option></option>
                                                    <option value="<?php eissetor($user['hakim_id']) ?>" selected="selected"><?php eissetor($user['nama_hakim']) ?></option>
                                                </select>
                                                </div>
                                            <?php }else{ ?>
                                                <input type="text" value="<?php eissetor($perkara['nama_km'], '-') ?>" readonly class="form-control-plaintext form-control-sm"/>
                                            <?php } ?>

                                            
                                        </div>
                                    </div>

                                    <div class="form-group row <?php echo !isset($user['grup_id'])|| $user['grup_id'] != 4 ? 'hide' : '' ?>" id="pp_block">
                                        <label class="control-label text-right col-3">Paniter/PP</label>
                                        <div class="col-9">
                                            <div class="controls">
                                                <select style="width: 100%" required data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm kt-select2" id="pp_id" name="pp_id">
                                                <option></option>
                                                <option value="<?php eissetor($user['panitera_id']) ?>" selected="selected"><?php eissetor($user['nama_pp']) ?></option>
                                            </select>
                                            </div>

                                            
                                        </div>
                                    </div>


                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Status</label>
                                    <div class="col-3">
                                        <div class="controls">
                                            <select class="form-control form-control-sm" name="status">
                                                <option <?php echo isset($user['status']) && $user['status'] == 1 ? 'selected' : '' ?> value="1">Aktif</option>
                                                <option <?php echo isset($user['status']) && $user['status'] != 1 ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                                <input type="submit" name="submit" class="btn btn-success mt-3" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
