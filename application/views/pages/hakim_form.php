<?php

$title = "Tambah Hakim Tinggi";

$isEdit = false;

if(isset($id)){
    $isEdit = true;
    $title = "Ubah Hakim Tinggi";
}

?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><?php echo $title ?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?php echo $title ?></li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales overview chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="post" novalidate autocomplete="off" >
                        <div class="row">
                            <div class="col-md-12 customForm">
                                <div class="form-group row <?php echo form_error('nip') != null ? 'error' : '' ?>">
                                    <label class="control-label text-right col-3">NIP</label>
                                    <div class="col-9">
                                        <div class="controls">
                                            <input type="text" value="<?php eissetor($nip) ?>" required data-validation-required-message="Kolom ini wajib diisi"  class="form-control form-control-sm" id="nomor_perkara_banding" name="nip">

                                            <?php if(form_error('nip') != null) { ?> 
                                            <div class="help-block error-block"><?php echo form_error('nip') ?></div>
                                            <?php } ?>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Nama Lengkap</label>
                                    <div class="col-9">
                                        <div class="controls"><input type="text"  value="<?php eissetor($nama) ?>" required data-validation-required-message="Kolom ini wajib diisi" class="form-control form-control-sm" name="nama"></div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-3">Status</label>
                                    <div class="col-3">
                                        <div class="controls">
                                            <select class="form-control form-control-sm" name="status">
                                                <option <?php echo isset($status) && $status == 1 ? 'selected' : '' ?> value="1">Aktif</option>
                                                <option <?php echo isset($status) && $status != 1 ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <input type="submit" name="submit" class="btn btn-success mt-3" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
