<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Konseptor extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('kinerja_m');
	}

	public function index(){
		$footer['scripts'] = [
			
			
			
			
			'assets/js/konseptor.js'
		];

		$this->render('konseptor', [], $footer);
	}

}