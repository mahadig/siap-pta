<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/excel/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class Excel_kinerja_hakim extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('kinerja_m');
		$this->load->model('hakim_m');
	}

	public function index() {
        $mainTitle = "KINEJA HAKIM";

        $columns = $this->input->get('columns');
        $filter = $this->input->get('filter');
        $order = $this->input->get('order');

        $tgl_start = $filter['tgl_start'];
        $tgl_end = $filter['tgl_end'];

        if(validateDate($tgl_start) && validateDate($tgl_end)){
            $excelTitle = "$mainTitle $tgl_start - $tgl_end";
        }else{
            $tahun = date('Y');
            
            $excelTitle = "$mainTitle TAHUN $tahun";
        }

        $data = $this->kinerja_m->overall(false, false, $columns, $order, $filter);

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('PTA Jawa Barat')->setTitle($mainTitle);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->mergeCells('B1:J1');
        $spreadsheet->getActiveSheet()->setCellValue('B1', strtoupper($excelTitle));

        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray([
            'font' => [
            	'name' => "Arial Narrow",
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $index = 3;

        $spreadsheet->getActiveSheet()
			->setCellValue("B".$index,"No")
			->setCellValue("C".$index,"Ketua Majelis")
			->setCellValue("D".$index,"Perkara\nDiterima")
			->setCellValue("E".$index,"Perkara\Selesai")
			->setCellValue("F".$index,"Lama Selesai")
			->setCellValue("I".$index,"Kinerja")
			->setCellValue("J".$index,"Sisa\nPerkara");

        $spreadsheet->getActiveSheet()
			->setCellValue("F".($index+1),"1-2 Bln")
			->setCellValue("G".($index+1),"2-3 Bln")
			->setCellValue("H".($index+1),"> 3 Bln");


		$spreadsheet->getActiveSheet()->mergeCells('F'.$index.':H'.$index);
		$spreadsheet->getActiveSheet()->mergeCells('B'.$index.':B'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('C'.$index.':C'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('D'.$index.':D'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('E'.$index.':E'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('I'.$index.':I'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('J'.$index.':J'.($index+1));


			$spreadsheet->getActiveSheet()->getStyle('A'.$index.':J'.($index+1))
			->getAlignment()->setWrapText(true);

        // $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(22);

        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':J'.($index+1))->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10,
                'name' => "Arial Narrow"
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ]
        ]);


        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':J'.($index+1))->applyFromArray($this->header);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(13);

		
		$index +=2;
        $no = 1;
        foreach ($data as $row) {
			$html = new PhpOffice\PhpSpreadsheet\Helper\Html();
			// $cell_value = $html->toRichTextObject("<font face='Arial Narrow' size='10'>".$row->keterangan.'</font>');

        	$spreadsheet->getActiveSheet()
                ->setCellValue("B".$index, $no)
                ->setCellValue("C".$index, $row['nama_km'])
				->setCellValue("D".$index, $row['jumlah'])
				->setCellValue("E".$index, $row['perkara_selesai'])
				->setCellValue("F".$index, $row['hijau'])
				->setCellValue("G".$index, $row['kuning'])
				->setCellValue("H".$index, $row['merah'])
				->setCellValue("I".$index, $row['kinerja'])
				->setCellValue("J".$index, $row['jumlah'] - $row['perkara_selesai']);

                // ->setCellValue('K'.$index, '')
                // ->setCellValue('L'.$index, $cell_value);

            $spreadsheet->getActiveSheet()->getStyle('A'.$index.':J'.$index)
			->getAlignment()->setWrapText(true);

            setlocale(LC_ALL, 'id_ID.UTF-8');

    //         $spreadsheet->getActiveSheet()
				// ->getStyle('D'.$index)
				// ->getNumberFormat()
				// ->setFormatCode("#,##0");

            $spreadsheet->getActiveSheet()->getStyle('B'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('C'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT)->setWrapText(true);

            $spreadsheet->getActiveSheet()->getStyle('D'.$index.':J'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);

			// $spreadsheet->getActiveSheet()->getStyle('L'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT);
			// $spreadsheet->getActiveSheet()->getRowDimension($index)->setRowHeight(22);
			$spreadsheet->getActiveSheet()->getStyle('B'.$index.':J'.$index)->applyFromArray($this->border);
        	
        	$index++;
        	$no++;
        }

        $spreadsheet->getActiveSheet()->getStyle('B5:J'.($index - 1))
    		->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B1:J'.($index-1));
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);
		$spreadsheet->getActiveSheet()->getPageMargins()->setHeader(0.1);
		$spreadsheet->getActiveSheet()->getPageMargins()->setFooter(0.1);
        
        $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
		$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 4);

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$mainTitle.' '.(date('d/m/Y')).'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

    }

}