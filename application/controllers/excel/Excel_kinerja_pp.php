<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/excel/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class Excel_kinerja_pp extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('kinerja_m');
		$this->load->model('panitera_m');
	}

	public function index() {
        $mainTitle = "KINERJA PANITERA/PP";

        $columns = $this->input->get('columns');
        $filter = $this->input->get('filter');
        $order = $this->input->get('order');

        $bulan = intval($filter['bulan']);
        $tahun = intval($filter['tahun']);
        $period = $tahun.'-'.str_pad($bulan, 2, '0', STR_PAD_LEFT);

        if(validateDate($period, 'Y-m')){
            $excelTitle = $mainTitle. getBulanName($bulan) .' '.$tahun;
        }else{
            $tahun = date('Y');
            
            $excelTitle = $mainTitle.' '.$tahun;
        }

        // $data = $this->kinerja_m->overall(false, false, $columns, $order, $filter);
        $data = $this->panitera_m->kinerja($columns, $order, $filter);

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('PTA Jawa Barat')->setTitle($mainTitle);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->mergeCells('B1:H1');
        $spreadsheet->getActiveSheet()->setCellValue('B1', strtoupper($excelTitle));

        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray([
            'font' => [
            	'name' => "Arial Narrow",
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $index = 3;

        $spreadsheet->getActiveSheet()
			->setCellValue("B".$index,"No")
			->setCellValue("C".$index,"Nama Panitera/PP")
			->setCellValue("D".$index,"Kinerja")
			->setCellValue("H".$index,"Nilai Akhir");

        $spreadsheet->getActiveSheet()
			->setCellValue("D".($index+1),"Konsep Rangka Putusan")
			->setCellValue("E".($index+1),"Minutasi")
			->setCellValue("F".($index+1),"Telaah SIngkronisasi")
            ->setCellValue("G".($index+1),"Telaah Laporan");


		$spreadsheet->getActiveSheet()->mergeCells('D'.$index.':G'.$index);
		$spreadsheet->getActiveSheet()->mergeCells('B'.$index.':B'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('C'.$index.':C'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('H'.$index.':H'.($index+1));
		

			$spreadsheet->getActiveSheet()->getStyle('A'.$index.':H'.($index+1))
			->getAlignment()->setWrapText(true);

        // $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(22);

        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':H'.($index+1))->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10,
                'name' => "Arial Narrow"
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ]
        ]);


        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':H'.($index+1))->applyFromArray($this->header);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);

		
		$index +=2;
        $no = 1;
        foreach ($data as $row) {
			$html = new PhpOffice\PhpSpreadsheet\Helper\Html();
			// $cell_value = $html->toRichTextObject("<font face='Arial Narrow' size='10'>".$row->keterangan.'</font>');

        	$spreadsheet->getActiveSheet()
                ->setCellValue("B".$index, $no)
                ->setCellValue("C".$index, $row['nama_pp'])
				->setCellValue("D".$index, $row['k_konsep'])
				->setCellValue("E".$index, $row['k_minut'])
				->setCellValue("F".$index, $row['nilai_singkron'])
				->setCellValue("G".$index, $row['nilai_telaah'])
				->setCellValue("H".$index, $row['nilai_akhir']);

                // ->setCellValue('K'.$index, '')
                // ->setCellValue('L'.$index, $cell_value);

            $spreadsheet->getActiveSheet()->getStyle('A'.$index.':H'.$index)
			->getAlignment()->setWrapText(true);

            setlocale(LC_ALL, 'id_ID.UTF-8');

    //         $spreadsheet->getActiveSheet()
				// ->getStyle('D'.$index)
				// ->getNumberFormat()
				// ->setFormatCode("#,##0");

            $spreadsheet->getActiveSheet()->getStyle('B'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('C'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT)->setWrapText(true);

            $spreadsheet->getActiveSheet()->getStyle('D'.$index.':H'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);

			// $spreadsheet->getActiveSheet()->getStyle('L'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT);
			// $spreadsheet->getActiveSheet()->getRowDimension($index)->setRowHeight(22);
			$spreadsheet->getActiveSheet()->getStyle('B'.$index.':H'.$index)->applyFromArray($this->border);
        	
        	$index++;
        	$no++;
        }

        $spreadsheet->getActiveSheet()->getStyle('B5:H'.($index - 1))
    		->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B1:H'.($index-1));
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);
		$spreadsheet->getActiveSheet()->getPageMargins()->setHeader(0.1);
		$spreadsheet->getActiveSheet()->getPageMargins()->setFooter(0.1);
        
        $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
		$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 4);

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$mainTitle.' '.(date('d/m/Y')).'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

    }

}