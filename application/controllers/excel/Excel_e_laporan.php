<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/excel/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class Excel_e_laporan extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('e_laporan_m');
	}

	public function index() {
        $mainTitle = "KINERJA TELAAH E-LAPORAN ";

        $columns = $this->input->get('columns');
        $filter = $this->input->get('filter');
        $order = $this->input->get('order');

        $bulan = intval($filter['bulan']);
        $tahun = intval($filter['tahun']);
        $period = $tahun.'-'.str_pad($bulan, 2, '0', STR_PAD_LEFT);

        if(validateDate($period, 'Y-m')){
        	$excelTitle = $mainTitle. getBulanName($bulan) .' '.$tahun;
        }else{
        	$tahun = date('Y');
        	
			$excelTitle = $mainTitle.' '.$tahun;
        }
// var_dump($tgl_start);
// return;
        $data = $this->e_laporan_m->table(null, null, $columns, $order, $filter);
        // $excelTitleFooter .= count($data)." buah";
// var_dump($data);
// return false;
        // $groupData = [];

        // foreach ($data as $row) {
        //     $groupData[$row->tgl_diterima][] = $row;
        // }

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('PTA Jawa Barat')
        ->setTitle('KINERJA KONSEPTOR');
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->mergeCells('B1:G1');

        $spreadsheet->getActiveSheet()
        ->setCellValue('B1', strtoupper($excelTitle));

        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray([
            'font' => [
            	'name' => "Arial Narrow",
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $index = 3;

        $spreadsheet->getActiveSheet()
			->setCellValue("B".$index,"No")
			->setCellValue("C".$index,"Nama Satker")
			->setCellValue("D".$index,"Penelaah")
            ->setCellValue("E".$index,"Tgl. Konfirmasi")
            ->setCellValue("F".$index,"Tgl. Telaah")
            ->setCellValue("G".$index,"Ket.");


		$spreadsheet->getActiveSheet()->getStyle('A'.$index.':G'.$index)
			->getAlignment()->setWrapText(true);

        // $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(22);

        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':G'.$index)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10,
                'name' => "Arial Narrow"
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ]
        ]);


        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':G'.$index)->applyFromArray($this->header);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(22);

		
		$index++;
        $no = 1;
        foreach ($data as $row) {
			$html = new PhpOffice\PhpSpreadsheet\Helper\Html();
			// $cell_value = $html->toRichTextObject("<font face='Arial Narrow' size='10'>".$row->keterangan.'</font>');

        	$spreadsheet->getActiveSheet()
                ->setCellValue("B".$index, $no)
                ->setCellValue("C".$index, $row['nama_singkat'])
				->setCellValue("D".$index, $row['nama_pp'])
                ->setCellValue("E".$index, getDateShort($row['tgl_konfirmasi']))
                ->setCellValue("F".$index, getDateShort($row['tgl_telaah']))
                ->setCellValue("G".$index, $row['keterangan']);

            if(!$row['nama_pp']){
                $spreadsheet->getActiveSheet()->getStyle('B'.$index.':G'.$index)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('fff69f');
            }

                // ->setCellValue('K'.$index, '')
                // ->setCellValue('L'.$index, $cell_value);

            $spreadsheet->getActiveSheet()->getStyle('A'.$index.':G'.$index)
			->getAlignment()->setWrapText(true);

            setlocale(LC_ALL, 'id_ID.UTF-8');

            $spreadsheet->getActiveSheet()->getStyle('B'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('C'.$index.':D'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT)->setWrapText(true);

            $spreadsheet->getActiveSheet()->getStyle('E'.$index.':F'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('G'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT);

			$spreadsheet->getActiveSheet()->getStyle('B'.$index.':G'.$index)
                ->applyFromArray($this->border);
        	
        	$index++;
        	$no++;
        }

        $spreadsheet->getActiveSheet()->getStyle('B5:G'.($index - 1))
    		->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B1:G'.($index-1));
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);
		$spreadsheet->getActiveSheet()->getPageMargins()->setHeader(0.1);
		$spreadsheet->getActiveSheet()->getPageMargins()->setFooter(0.1);
        
        $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
		$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 4);

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$excelTitle.' '.(date('d/m/Y')).'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

    }

}