<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/excel/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class Excel_perkara extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('perkara_m');
		$this->load->model('hakim_m');
	}

	public function index() {
        $filter = $this->input->get('filter');

        $tgl_start = $filter['tgl_start'];
        $tgl_end = $filter['tgl_end'];

        if(validateDate($tgl_start) && validateDate($tgl_end)){
        	$excelTitle = 'DATA BANDING '. $tgl_start .'-'.$tgl_end;
            $fileName = 'DATA BANDING '. $tgl_start .'-'.$tgl_end;
        }else{
        	$tahun = date('Y');
        	
			$excelTitle = 'DATA BANDING TAHUN '.$tahun;
			$fileName = 'DATA BANDING TAHUN '.$tahun;
        }
// var_dump($tgl_start);
// return;
        $data = $this->perkara_m->monitoring(false, false, false, false, $filter);
        // $excelTitleFooter .= count($data)." buah";
// var_dump($data);
// return false;
        // $groupData = [];

        // foreach ($data as $row) {
        //     $groupData[$row->tgl_diterima][] = $row;
        // }

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('PTA Jawa Barat')
        ->setTitle('LAPORAN PERKARA');
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->mergeCells('B1:Z1');

        $spreadsheet->getActiveSheet()
        ->setCellValue('B1', strtoupper($excelTitle));

        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray([
            'font' => [
            	'name' => "Arial Narrow",
                'bold' => true,
                'size' => 18
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        if(isset($filter['km_id']) && $filter['km_id'] != ''){
        	$spreadsheet->getActiveSheet()
        		->setCellValue('B2', "Ketua Majelis: " . $this->hakim_m->nama($filter['km_id']));
			$spreadsheet->getActiveSheet()->mergeCells('B2:Z2');
	        $spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray([
	            'font' => [
	            	'name' => "Arial Narrow",
	                'bold' => true,
	                'size' => 18
	            ],
	            'alignment' => [
	                'horizontal' => Alignment::HORIZONTAL_CENTER,
	            ]
	        ]);
			$index = 4;
        }else{
        	$index = 3;
        }

        $spreadsheet->getActiveSheet()
			->setCellValue("B".$index,"No")
			->setCellValue("C".$index,"Nomor Perkara PTA")
			->setCellValue("D".$index,"Nomor Perkara PA")
			->setCellValue("E".$index,"Lama Proses Keseluruhan")
			->setCellValue("F".$index,"Proses Pemberkasan")
			->setCellValue("G".$index,"Kode PA Asal")
			->setCellValue("H".$index,"Tanggal Permohonan Banding")
			->setCellValue("I".$index,"Tanggal Kirim Berkas")
			->setCellValue("J".$index,"Berkas Kembali (KL)")
			->setCellValue("K".$index,"Tanggal Register")
			->setCellValue("L".$index,"Tanggal Penetapan Majelis")
			->setCellValue("M".$index,"Tanggal Penunjukan PP")
			->setCellValue("N".$index,"Tanggal PHS")
			->setCellValue("O".$index,"Tanggal Sidang Pertama")
			->setCellValue("P".$index,"Tanggal Putusan Sela")
			->setCellValue("Q".$index,"Tanggal Putusan")
			->setCellValue("R".$index,"Tanggal Minutasi")
			->setCellValue("S".$index,"Tanggal Diserahkan Ke Panmud Banding")
			->setCellValue("T".$index,"Tgl pengiriman berkas ke PA Asal")
			->setCellValue("U".$index,"Tanggal diserahkan ke Meja ".$index)
			->setCellValue("V".$index,"Tanggal Upload")
			->setCellValue("W".$index,"Tanggal Anonimasi")
			->setCellValue("X".$index,"KETUA MAJELIS")
			->setCellValue("Y".$index,"PP")
			->setCellValue("Z".$index,"Keterangan");

			$spreadsheet->getActiveSheet()->getStyle('A'.$index.':Z'.$index)
			->getAlignment()->setWrapText(true);

        // $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(22);

        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':Z'.$index)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10,
                'name' => "Arial Narrow"
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ]
        ]);


        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':Z'.$index)->applyFromArray($this->header);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(90);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(90);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        // $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(15);

		
		$index ++;
        $no = 1;
        foreach ($data as $row) {
			$html = new PhpOffice\PhpSpreadsheet\Helper\Html();
			// $cell_value = $html->toRichTextObject("<font face='Arial Narrow' size='10'>".$row->keterangan.'</font>');

        	$spreadsheet->getActiveSheet()
                ->setCellValue("B".$index, $no)
                ->setCellValue("C".$index, $row['nomor_perkara_banding'])
				->setCellValue("D".$index, $row['nomor_perkara_pa'])
				->setCellValue("E".$index, $row['nama_pembanding'])
				->setCellValue("F".$index, $row['nama_terbanding'])
				->setCellValue("G".$index, $row['nama_satker'])
				->setCellValue("H".$index, getDateShort($row['tgl_mohon_banding']))
				->setCellValue("I".$index, getDateShort($row['tgl_kirim_berkas']))
				->setCellValue("J".$index, getDateShort($row['berkas_kembali_kl']))
				->setCellValue("K".$index, getDateShort($row['tgl_register']))
				->setCellValue("L".$index, getDateShort($row['tgl_penetapan_majelis']))
				->setCellValue("M".$index, getDateShort($row['tgl_penunjukan_pp']))
				->setCellValue("N".$index, getDateShort($row['tgl_phs']))
				->setCellValue("O".$index, getDateShort($row['tgl_sidang_pertama']))
				->setCellValue("P".$index, getDateShort($row['tgl_putusan_sela']))
				->setCellValue("Q".$index, getDateShort($row['tgl_putusan']))
				->setCellValue("R".$index, getDateShort($row['tgl_minutasi']))
				->setCellValue("S".$index, getDateShort($row['tgl_serah_panmud']))
				->setCellValue("T".$index, getDateShort($row['tgl_kirim_pa']))
				->setCellValue("U".$index, getDateShort($row['tgl_serah_meja3']))
				->setCellValue("V".$index, getDateShort($row['tgl_upload']))
				->setCellValue("W".$index, getDateShort($row['tgl_anonimasi']))
				->setCellValue("X".$index, $row['nama_km'])
				->setCellValue("Y".$index, $row['nama_pp'])
				->setCellValue("Z".$index, $row['keterangan']);

                // ->setCellValue('K'.$index, '')
                // ->setCellValue('L'.$index, $cell_value);

            $spreadsheet->getActiveSheet()->getStyle('A'.$index.':Z'.$index)
			->getAlignment()->setWrapText(true);

            setlocale(LC_ALL, 'id_ID.UTF-8');

    //         $spreadsheet->getActiveSheet()
				// ->getStyle('D'.$index)
				// ->getNumberFormat()
				// ->setFormatCode("#,##0");

            $spreadsheet->getActiveSheet()->getStyle('B'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle('C'.$index.':G'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT)->setWrapText(true);

            $spreadsheet->getActiveSheet()->getStyle('H'.$index.':W'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);

			// $spreadsheet->getActiveSheet()->getStyle('L'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT);
			// $spreadsheet->getActiveSheet()->getRowDimension($index)->setRowHeight(22);
			$spreadsheet->getActiveSheet()->getStyle('B'.$index.':Z'.$index)->applyFromArray($this->border);
        	
        	$index++;
        	$no++;
        }

        $spreadsheet->getActiveSheet()->getStyle('B5:Z'.($index - 1))
    		->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B2:Z'.($index+4));
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);
		$spreadsheet->getActiveSheet()->getPageMargins()->setHeader(0.1);
		$spreadsheet->getActiveSheet()->getPageMargins()->setFooter(0.1);
        
        $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
		$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 4);

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		// $spreadsheet->getActiveSheet()->getHeaderFooter()
  //   		->setOddFooter('&L&K919191Tanggal Cetak: ' . date('d/m/Y') . '&R&K919191Halaman &P');

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$fileName.' '.(date('d/m/Y')).'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

    }

}