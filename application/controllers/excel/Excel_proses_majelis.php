<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/excel/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class Excel_proses_majelis extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('panitera_m');
		$this->load->model('hakim_m');
	}

	public function index() {
        $columns = $this->input->get('columns');
        $filter = $this->input->get('filter');
        $order = $this->input->get('order');

        $tgl_start = $filter['tgl_start'];
        $tgl_end = $filter['tgl_end'];

        if(validateDate($tgl_start) && validateDate($tgl_end)){
        	$excelTitle = 'PROSES MAJELIS '. $tgl_start .'-'.$tgl_end;
            $fileName = 'PROSES MAJELIS '. $tgl_start .'-'.$tgl_end;
        }else{
        	$tahun = date('Y');
        	
			$excelTitle = 'PROSES MAJELIS TAHUN '.$tahun;
			$fileName = 'PROSES MAJELIS TAHUN '.$tahun;
        }
// var_dump($tgl_start);
// return;
        $data = $this->panitera_m->proses($columns, $order, $filter);
        // $excelTitleFooter .= count($data)." buah";

        // $groupData = [];

        // foreach ($data as $row) {
        //     $groupData[$row->tgl_diterima][] = $row;
        // }

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('PTA Jawa Barat')
        ->setTitle('KINERJA PANITERA/PP');
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $spreadsheet->getActiveSheet()->mergeCells('B1:O1');

        $spreadsheet->getActiveSheet()
        ->setCellValue('B1', strtoupper($excelTitle));

        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray([
            'font' => [
            	'name' => "Arial Narrow",
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $index = 3;

        $spreadsheet->getActiveSheet()
			->setCellValue("B".$index,"No")
			->setCellValue("C".$index,"Nama Panitera/PP")
			->setCellValue("D".$index,"Perkara")
            ->setCellValue("G".$index,"Konsep Rangka Putusan")
            ->setCellValue("J".$index,"Minutasi")
            ->setCellValue("M".$index,"Telaah\nSingkronisasi")
            ->setCellValue("N".$index,"Telaah\nLaporan")
            ->setCellValue("O".$index,"Skor\nTotal")
			;

        $spreadsheet->getActiveSheet()
			->setCellValue("D".($index+1),"Diterima")
			->setCellValue("E".($index+1),"Selesai")
			->setCellValue("F".($index+1),"Sisa")

            ->setCellValue("G".($index+1),"1-3 Hari")
            ->setCellValue("H".($index+1),"4-7 Hari")
            ->setCellValue("I".($index+1),"> 7 Hari")
            
            ->setCellValue("J".($index+1),"1-3 Hari")
            ->setCellValue("K".($index+1),"4-7 Hari")
            ->setCellValue("L".($index+1),"> 7 Hari");


		$spreadsheet->getActiveSheet()->mergeCells('D'.$index.':F'.$index);
        $spreadsheet->getActiveSheet()->mergeCells('G'.$index.':I'.$index);
        $spreadsheet->getActiveSheet()->mergeCells('J'.$index.':L'.$index);
		$spreadsheet->getActiveSheet()->mergeCells('B'.$index.':B'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('C'.$index.':C'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('M'.$index.':M'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('N'.$index.':N'.($index+1));
		$spreadsheet->getActiveSheet()->mergeCells('O'.$index.':O'.($index+1));

		$spreadsheet->getActiveSheet()->getStyle('A'.$index.':O'.($index+1))
			->getAlignment()->setWrapText(true);

        // $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(22);

        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':O'.($index+1))->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10,
                'name' => "Arial Narrow"
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ]
        ]);


        $spreadsheet->getActiveSheet()->getStyle('B'.$index.':O'.($index+1))->applyFromArray($this->header);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(13);

		
		$index +=2;
        $no = 1;
        foreach ($data as $row) {
			$html = new PhpOffice\PhpSpreadsheet\Helper\Html();
			// $cell_value = $html->toRichTextObject("<font face='Arial Narrow' size='10'>".$row->keterangan.'</font>');

        	$spreadsheet->getActiveSheet()
                ->setCellValue("B".$index, $no)
                ->setCellValue("C".$index, $row['nama_pp'])
				->setCellValue("D".$index, $row['jumlah'])
				->setCellValue("E".$index, $row['minutasi'])
				->setCellValue("F".$index, $row['sisa'])
				->setCellValue("G".$index, $row['konsep_h'])
				->setCellValue("H".$index, $row['konsep_k'])
                ->setCellValue("I".$index, $row['konsep_m'])
                ->setCellValue("J".$index, $row['minut_h'])
                ->setCellValue("K".$index, $row['minut_k'])
				->setCellValue("L".$index, $row['minut_m'])
                ->setCellValue("M".$index, $row['t_singkron'])
                ->setCellValue("N".$index, $row['t_laporan'])
				->setCellValue("O".$index, '-');

                // ->setCellValue('K'.$index, '')
                // ->setCellValue('L'.$index, $cell_value);

            $spreadsheet->getActiveSheet()->getStyle('A'.$index.':O'.$index)
			->getAlignment()->setWrapText(true);

            setlocale(LC_ALL, 'id_ID.UTF-8');

    //         $spreadsheet->getActiveSheet()
				// ->getStyle('D'.$index)
				// ->getNumberFormat()
				// ->setFormatCode("#,##0");

            $spreadsheet->getActiveSheet()->getStyle('B'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $spreadsheet->getActiveSheet()->getStyle('C'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT)->setWrapText(true);

            $spreadsheet->getActiveSheet()->getStyle('D'.$index.':O'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);

			// $spreadsheet->getActiveSheet()->getStyle('L'.$index)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_LEFT);
			// $spreadsheet->getActiveSheet()->getRowDimension($index)->setRowHeight(22);
			$spreadsheet->getActiveSheet()->getStyle('B'.$index.':O'.$index)->applyFromArray($this->border);
        	
        	$index++;
        	$no++;
        }

        $spreadsheet->getActiveSheet()->getStyle('B5:O'.($index - 1))
    		->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B1:O'.($index-1));
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setRight(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0.23);
		$spreadsheet->getActiveSheet()->getPageMargins()->setBottom(0.5);
		$spreadsheet->getActiveSheet()->getPageMargins()->setHeader(0.1);
		$spreadsheet->getActiveSheet()->getPageMargins()->setFooter(0.1);
        
        $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
		$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(false);
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(4, 4);


		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$fileName.' '.(date('d/m/Y')).'.xlsx"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;

    }

}