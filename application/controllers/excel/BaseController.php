<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class BaseController extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('username')){
            redirect(base_url() . 'auth/login');
        }

        $this->border = [
            'font' => [
                    'size' => 10,
                    'name' => "Arial Narrow"
                ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ]
            ],
        ];

        $this->subtitleStyle = [
                'font' => [
                    'bold' => true,
                    'size' => 10,
                    'name' => "Arial Narrow"
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ]
            ];

        $this->header = array_merge($this->border, [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => '000000'],
                'size' => 10
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'rotation' => 90,
                'color' => ['rgb' => 'F5F5F5'],
            ]
        ]);

        $this->subHeader = array_merge($this->border, [
            'font' => [
                'bold' => true
            ]
        ]);
        
    }
}