<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/api/Rest.php';

class Users extends Rest {

    function __construct()
    {
        parent::__construct();

        $this->load->model('users_m');
    }

    function index_get(){
        // $q = $this->input->get('q');

        $start = (int) $this->input->post('start');
        $length = (int) $this->input->post('length');

        $column = $this->input->post('columns');
        $order = $this->input->post('order');

        $sort = "id";

        $res = $this->users_m->table($start, $length, $column, $order);

        $total = $this->users_m->total();

        $data['iTotalDisplayRecords']  = count($res);
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        // $data['meta']['field'] = $sort['field'];

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function cari_get(){
        $q = $this->input->get('q');

        $res = $this->users_m->any($q);

        $this->response($res, REST_Controller::HTTP_OK);
    }

    function delete_get(){
        $this->restrict();
        
        $id = (int) $this->input->get('id');

        if($id > 0){
            $this->users_m->delete($id);

            $this->response([], REST_Controller::HTTP_OK);
        }

        $this->response([], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
}