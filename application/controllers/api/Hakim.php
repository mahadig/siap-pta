<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/api/Rest.php';

class Hakim extends Rest {

    function __construct()
    {
        parent::__construct();

        $this->load->model('hakim_m');
    }

    function index_get(){
        // $q = $this->input->get('q');

        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');

        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "id";

        $res = $this->hakim_m->table($start, $length, $column, $order);

        $total = $this->hakim_m->total();

        $data['iTotalDisplayRecords']  = $total;
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        // $data['meta']['field'] = $sort['field'];

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function cari_get(){
        $q = $this->input->get('q');

        $res = $this->hakim_m->any($q);

        $this->response($res, REST_Controller::HTTP_OK);
    }

    function delete_get(){
        $this->restrict();
        
        $id = (int) $this->input->get('id');

        if($id > 0){
            $this->hakim_m->delete($id);

            $this->response([], REST_Controller::HTTP_OK);
        }

        $this->response([], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
}