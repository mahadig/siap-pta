<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/api/Rest.php';

class Dasbor extends Rest {

    function __construct()
    {
        parent::__construct();

        $this->load->model('perkara_m');
    }

    function index_get(){
        $filter_period = $this->input->get('filter_period');
        $tgl_start = $this->input->get('tgl_start');
        $tgl_end = $this->input->get('tgl_end');

        $res = $this->perkara_m->overview($filter_period, $tgl_start, $tgl_end);

        $stats = [];

        /** LABEL **/
        $arr = explode('/', $tgl_start);
        $sMonth = intval($arr[1]);
        $sYear = intval($arr[2]);

        $jenisPutusan = $this->perkara_m->jenis_putusan();


        for ($i=1; $i <= 12 ; $i++) { 
            $stats['label'][] = getBulanNameShort($sMonth) . ' ' . substr($sYear, 2,4);
            $sMonth++;

            if($sMonth > 12){
                $sMonth = 1;
                $sYear ++;
            }
        }

        /** RESET sYear **/
        foreach ($jenisPutusan as $row1) {
            $sMonth = intval($arr[1]);
            $sYear = intval($arr[2]);

            for ($i=1; $i <= 12 ; $i++) { 
                $period = $sYear .'-' . str_pad($sMonth, 2, "0", STR_PAD_LEFT);
                $label = getBulanNameShort($sMonth) . ' ' . substr($sYear, 2,4);;

                $stats["putus_".$row1['id']][] = 0;

                foreach ($res['stats'] as $row2) {
                    if($row1['id'] == $row2['jenis_putus_id'] ){
                        if($row2['periode'] == $period){
                            $stats["putus_".$row1['id']][$i-1] = intval($row2['jml']);
                        }
                    }
                }

                $sMonth++;

                if($sMonth > 12){
                    $sMonth = 1;
                    $sYear ++;
                }
            }
        }

        $putusanCount = [];
        foreach ($res['stats'] as $row2) {
            if(isset($putusanCount['putusan_'.$row2['jenis_putus_id']])){
                $putusanCount['putusan_'.$row2['jenis_putus_id']] += intval($row2['jml']);
            }else{
                $putusanCount['putusan_'.$row2['jenis_putus_id']] = intval($row2['jml']);
            }
        }

        $res['stats'] = $stats;
        $res['stats']['count'] = $putusanCount;

        $this->response($res, REST_Controller::HTTP_OK);
    }

}