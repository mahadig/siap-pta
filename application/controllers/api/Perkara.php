<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/api/Rest.php';

class Perkara extends Rest {

    function __construct()
    {
        parent::__construct();

        $this->load->model('perkara_m');
    }

    function index_get(){
        // $q = $this->input->get('q');

        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');
        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "perkara_id";

        $filter = $this->input->get('filter');
        $res = $this->perkara_m->table($start, $length, $column, $order, $filter);
        $total = $this->perkara_m->total();

        $data['iTotalDisplayRecords']  = $total;
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        $data['authorized_delete'] = accessAdminOperator();
        // $data['meta']['field'] = $sort['field'];

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    private function getJmlOf($arr, $str){
        foreach ($arr as $row) {
            if($row['km_id'] == $str) {
                return $row['jml'];
                break;
            }
        }

        return false;
    }

    // function sisa_perkara_get(){
    //     $start = (int) $this->input->post('start');
    //     $length = (int) $this->input->post('length');

    //     $column = $this->input->post('columns');
    //     $order = $this->input->post('order');

    //     $sort = "perkara_id";

    //     $res = $this->perkara_m->sisa();

    //     $data['iTotalDisplayRecords']  = count($res);
    //     $data['iTotalRecords'] = count($res);
    //     $data['sEcho'] = 0;

    //     $data['aaData'] = $res;

    //     $this->response($data, REST_Controller::HTTP_OK);
    // }

    function monitoring_perkara_get(){
        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');

        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "perkara_id";

        $filter = $this->input->get('filter');

        $res = $this->perkara_m->monitoring($start, $length, $column, $order, $filter);

        $total = $this->perkara_m->total();

        $data['iTotalDisplayRecords']  = $total;
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        $data['authorized_delete'] = accessAdminOperator();

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function kinerja_hakim_get(){
        $this->load->model('kinerja_m');

        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');

        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "perkara_id";

        $filter = $this->input->get('filter');

        $res = $this->kinerja_m->kinerjaHakim($start, $length, $column, $order, $filter);

        $data['iTotalDisplayRecords']  = count($res);
        $data['iTotalRecords'] = count($res);
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        $data['authorized_delete'] = accessAdminOperator();

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function kinerja_perkara_get(){
        $this->load->model('kinerja_m');

        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');

        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "perkara_id";

        $filter = $this->input->get('filter');

        $res = $this->kinerja_m->overall($start, $length, $column, $order, $filter);

        $data['iTotalDisplayRecords']  = count($res);
        $data['iTotalRecords'] = count($res);
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        $data['authorized_delete'] = accessAdminOperator();

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function kinerja_konseptor_get(){
        $this->load->model('kinerja_m');

        $column = $this->input->get('columns');
        $order = $this->input->get('order');

        $sort = "perkara_id";

        $filter = $this->input->get('filter');

        $res = $this->kinerja_m->kinerjaKonseptor($column, $order, $filter);

        $data['iTotalDisplayRecords']  = count($res);
        $data['iTotalRecords'] = count($res);
        $data['sEcho'] = 0;
        $data['authorized'] = checkAccess();
        $data['authorized_delete'] = accessAdminOperator();

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function delete_get(){
        accessAdminOperator();
        
        $id = (int) $this->input->get('id');
        

        if($id > 0){
            $this->perkara_m->delete($id);

            $this->response([], REST_Controller::HTTP_OK);
        }

        $this->response([], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
}