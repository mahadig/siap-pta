<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/api/Rest.php';

class E_laporan extends Rest {

    function __construct()
    {
        parent::__construct();

        $this->load->model('e_laporan_m');
    }

    function index_post(){
        // $q = $this->input->get('q');

        $start = (int) $this->input->post('start');
        $length = (int) $this->input->post('length');

        $column = $this->input->post('columns');
        $order = $this->input->post('order');

        $filter = $this->input->get('filter');

        $sort = "id";

        $res = $this->e_laporan_m->table($start, $length, $column, $order, $filter);
        $total = $this->e_laporan_m->total();

        $data['iTotalDisplayRecords']  = $total;
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        $data['authorized'] = accessAdminOperator();
        // $data['meta']['field'] = $sort['field'];

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function singkron_post(){
        // $q = $this->input->get('q');

        $start = (int) $this->input->post('start');
        $length = (int) $this->input->post('length');

        $column = $this->input->post('columns');
        $order = $this->input->post('order');

        $filter = $this->input->get('filter');

        $sort = "id";

        $res = $this->e_laporan_m->tableSingkron($start, $length, $column, $order, $filter);

        $total = $this->e_laporan_m->total();

        $data['iTotalDisplayRecords']  = $total;
        $data['iTotalRecords'] = $total;
        $data['sEcho'] = 0;
        // $data['meta']['field'] = $sort['field'];

        $data['aaData'] = $res;

        $this->response($data, REST_Controller::HTTP_OK);
    }

    function satker_get(){
        $q = $this->input->get('q');

        $res = $this->e_laporan_m->satker($q);

        $this->response($res, REST_Controller::HTTP_OK);
    }

}