<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ob_start('ob_gzhandler');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Rest extends REST_Controller {
    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('username')){
            redirect(base_url() . 'auth/login');
        }
    }

    function restrict($access = ""){
        $userAccess = $this->session->userdata('hak_akses');

        if(!$access){
            
            if($userAccess != 0 && $userAccess < 6){
                $this->response(["msg" => "UNAUTHORIZED" ], REST_Controller::HTTP_UNAUTHORIZED);
                return true;
            }

        }else{

            if($userAccess != $access){
                $this->response(["msg" => "UNAUTHORIZED" ], REST_Controller::HTTP_UNAUTHORIZED);
                return true;
            }

        }

    }

}