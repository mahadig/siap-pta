<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Settings_panitera extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->restrict("admin");

		$this->load->model('panitera_m');
	}

	public function index()
	{
		$footer['scripts'] = [
			
			
			'assets/js/panitera.js'
		];

		$this->render('panitera', [], $footer);
	}

	public function edit($id){
		

		if($_POST){
			$this->save($id);
		}else{
			$this->showForm($id);
		}
	}

	public function add(){
		
		
		if($_POST){
			$this->save();
		}else{
			$this->showForm();
		}
	}

	private function showForm($paniteraId = false, $panitera = []){
		$footer['scripts'] = [
			'assets/js/validation.js',
			'assets/js/panitera_form.js'
		];

		if($paniteraId){
			$panitera = $this->panitera_m->find($paniteraId);

			if(!$panitera) 
redirectOrBack(base_url().'settings/panitera?404');
		}

		$this->render('panitera_form', $panitera, $footer);
	}

	function nip_check($val, $id){
		$res = $this->panitera_m->valid_nip($val, $id);

		if(!$res) $this->form_validation->set_message('nip_check', 'NIP '.$val.' sudah ada sebelumnya');

		return $res;
	}

	private function save($id = false){

		$this->form_validation->set_rules('nip', 'NIP', 'required|callback_nip_check['.$id.']');
		$this->form_validation->set_rules('nama', 'Nama', 'required');

		if ($this->form_validation->run() == FALSE){

            $this->showForm(false, [
            	'nip' => set_value('nip'),
            	'nama' => set_value('nama'),
            	'status' => set_value('status'),
            ]);

        } else {

        	$data['nip'] = $this->input->post('nip');
        	$data['nama'] = $this->input->post('nama');
        	$data['status'] = $this->input->post('status');

        	if($data['status'] != 1) $data['status'] = 0;

        	if($id){

        		$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = "admin";

				$simpan = $this->panitera_m->update($data, $id);
				$this->session->set_flashdata('success', 'Data Panitera/PP berhasil diperbarui.');

        	}else{

        		$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = "admin";

				$simpan = $this->panitera_m->add($data);
				$this->session->set_flashdata('success', 'Data Panitera/PP baru berhasil ditambahkan.');

        	}

        	
redirectOrBack(base_url().'settings/panitera');

        }
	}
}