<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Singkronisasi extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('panitera_m');
	}

	public function index(){
		$footer['scripts'] = [
			// 
			// 
			// 'assets/plugins/moment/moment.js',
			// 'assets/plugins/daterangepicker/daterangepicker.js',
			// 'assets/js/proses_majelis.js'
		];

		$this->render('proses_majelis', [], $footer);
	}

}