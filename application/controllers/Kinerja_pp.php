<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Kinerja_pp extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('kinerja_m');
	}

	public function index(){
		$footer['scripts'] = [
			'assets/js/kinerja_pp.js'
		];

		$this->render('kinerja_pp', [], $footer);
	}

	public function detail($id = false){

		if(!$id) redirect(base_url().'kinerja_pp');

		$footer['scripts'] = [
			// 'assets/plugins/Chart.js/Chart.min.js',
			'assets/js/kinerja_pp_detail.js'
		];

		$this->load->model('hakim_m');

		$year = date('Y');

		$data['majelis'] = $this->hakim_m->find($id, $year);

		if(!$data['majelis']) redirect(base_url().'kinerja_pp?404');

		$data['beban'] = $this->kinerja_m->beban($id, $year);
		$data['k_all'] = $this->kinerja_m->k_all($id, $year);
		$data['k_kirimBerkas'] = $this->kinerja_m->k_kirimBerkas($id, $year);
		$data['k_phs'] = $this->kinerja_m->k_phs($id, $year);
		$data['k_sidang'] = $this->kinerja_m->k_sidang($id, $year);
		$data['k_minut'] = $this->kinerja_m->k_minut($id, $year);
		$data['k_serah'] = $this->kinerja_m->k_serah($id, $year);
		$data['k_kirim'] = $this->kinerja_m->k_kirim($id, $year);
		$data['k_meja3'] = $this->kinerja_m->k_meja3($id, $year);
		// $data['k_meja3'] = $this->kinerja_m->k_meja3($id, $year);
		$data['k_anonim'] = $this->kinerja_m->k_anonim($id, $year);
		$data['k_upload'] = $this->kinerja_m->k_upload($id, $year);

		// $data['kinerja'] = $this->kinerja_m->detailKinerja($id);
// echo json_encode($data);
		$this->render('kinerja_pp_detail', $data, $footer);	
	}
}