<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login()
	{
		if($this->session->userdata('id')){
			redirect(base_url());
		}else{
			$this->load->view('login');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url() . 'auth/login');
	}

	public function validate(){
		
		$this->form_validation->set_rules('username','username','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');
		
		if($this->form_validation->run()===TRUE){
			
			$this->load->model('users_m');

			$username= trim($this->input->post('username'));
			$password= trim($this->input->post('password'));

			$password = hash("sha512", md5($this->input->post('password')));
			
			$result = $this->users_m->validateLogin($username, $password);
			$row = $result->row();

			if($row){

			if($row->status == 0){
				$this->session->set_flashdata('error', 'Akun Anda telah di blok/tidak memiliki Akses ke halaman ini.');
				redirect(base_url() . 'auth/login');
			}else{
				$data = array(
	                'id' => $row->id,
	                'panitera_id' => $row->panitera_id,
	                'hakim_id' => $row->hakim_id,
	                'username' => $row->username,
	                'nama' => $row->nama,
	                'jabatan' => $row->jabatan,
	                'grup' => $row->grup_text,
	                'grup_id' => $row->grup_id,
	                'status' => $row->status,
	                'singkron' => $row->tgl_singkron != null ? 'Y' : 'T'
	            ); 

	            $this->session->set_userdata($data);

				redirect(base_url());
			}

				
			}else{
				$this->session->set_flashdata('error', 'Username atau Password Anda Salah');

				redirect(base_url() . 'auth/login');
			}

		}else{
			redirect(base_url() . 'auth/login?login=gagal');
		}
	}
}
