<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Welcome extends BaseController {

	public function index()
	{
		$footer['scripts'] = [	
			'assets/js/dasbor.js'
		];

		$this->load->model('perkara_m');
		$this->render('welcome', [], $footer);
	}

}
