<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Settings_users extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('users_m');
		$this->load->model('hakim_m');
		$this->load->model('panitera_m');
	}

	public function profile(){
		$id = $this->session->userdata('id');
		if($_POST){
			$this->save($id, true);
		}else{
			$this->showForm($id, [], true);
		}
	}

	public function index()
	{
		$this->restrict("admin");

		$footer['scripts'] = [
			
			
			'assets/js/users.js'
		];


		$this->render('users', [], $footer);
	}

	public function edit($id){
		$this->restrict("admin");

		if($_POST){
			$this->save($id);
		}else{
			$this->showForm($id);
		}
	}

	public function add(){
		$this->restrict("admin");

		if($_POST){
			$this->save();
		}else{
			$this->showForm();
		}
	}

	private function showForm($usersId = false, $user = [], $isProfile = false){
		$footer['scripts'] = [
			'assets/js/validation.js',
			'assets/js/users_form.js'
		];

		if($usersId){
			$user = array_merge($user, $this->users_m->find($usersId));

			if(!$user) 
				redirectOrBack(base_url().'settings/users?404');
		}

		$data['user'] = $user;
		$data['isProfile'] = $isProfile;
		$data['grup'] = $this->users_m->grup();
		$data['jabatan'] = $this->users_m->jabatan();

		$this->render('users_form', $data, $footer);
	}

	function username_check($val, $id){
		$res = $this->users_m->valid_username($val, $id);

		if(!$res) $this->form_validation->set_message('username_check', 'Username "'.$val.'"" sudah terdaftar sebelumnya');

		return $res;
	}

	function password_check($val, $id){
		$pass = hash("sha512", md5($val));

		$res = $this->users_m->valid_password($pass, $id);

		if(!$res) $this->form_validation->set_message('password_check', 'Kata Sandi salah, silahkan ulangi.');

		return $res;
	}

	private function save($id = false, $isProfile = false){

		$this->form_validation->set_rules('username', 'Username', 'required|callback_username_check['.$id.']');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		
		$data['username'] = $this->input->post('username');
		$data['nama'] = $this->input->post('nama');
		// $data['jabatan'] = $this->input->post('jabatan');

		if(!$isProfile){
			$this->restrict("admin");

			$this->form_validation->set_rules('grup_id', 'Grup', 'required');
			
			$data['status'] = $this->input->post('status');
			
			if($data['status'] != 1) $data['status'] = 0;

			$data['grup_id'] = $this->input->post('grup_id');
			$data['grup_text'] = $this->users_m->namaGrup($data['grup_id']);

			$data['jabatan_id'] = $this->input->post('jabatan_id');
			$data['jabatan_text'] = $this->users_m->namaJabatan($data['jabatan_id']);
		}

		$password = $this->input->post('password');
		$password2 = $this->input->post('password2');
		$password2_confirm = $this->input->post('password2_confirm');

		if(!$id){
			$this->form_validation->set_rules('password', 'Password', 'required');
		} else {
			if($password2){
				$this->form_validation->set_rules('password', 'Password Lama', 'required|callback_password_check['.$id.']');
				$this->form_validation->set_rules('password2', 'Password Baru', 'required');
				$this->form_validation->set_rules('password2_confirm', 'Konfirmasi Password', 'matches[password2]', [ 'matches' => 'Konfirmasi Password tidak sama dengan Password baru']);
			}
		}


		if ($this->form_validation->run() == FALSE){
// echo validation_errors();
			$this->showForm($id, [
				'username' => set_value('username'),
				'nama' => set_value('nama'),
				'password2' => set_value('password2'),
				'password2_confirm' => set_value('password2_confirm'),
				'jabatan' => set_value('jabatan'),
				'grup_id' => set_value('grup_id'),
				'hakim_id' => set_value('hakim_id'),
				'nama_hakim' => $this->hakim_m->nama(set_value('hakim_id')),
				'pp_id' => set_value('pp_id'),
				'nama_pp' => $this->panitera_m->nama(set_value('pp_id')),
				'status' => set_value('status'),
			], $isProfile);

		} else {

			if($password){
				if($isProfile){
					$data['password'] = hash("sha512", md5($password2));
				}else{
					$data['password'] = hash("sha512", md5($password));
				}
			}

			if($id){
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = "admin";

				$simpan = $this->users_m->update($data, $id);

				$this->session->set_flashdata('success', 'Data Pengguna berhasil diperbarui.');
			}else{

				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = "admin";

				$id = $this->users_m->add($data);
				$this->session->set_flashdata('success', 'Data Pengguna baru berhasil ditambahkan.');
			}

			if(!$isProfile){
				$this->restrict("admin");

				$this->users_m->clearRelation($id);

				$paniteraId = $this->input->post('pp_id');
				$hakimId = $this->input->post('hakim_id');

				if($hakimId && $data['grup_id'] == 3){
					$hakim['user_id'] = $id;
					$hakim['hakim_id'] = $hakimId;

					$this->users_m->saveHakim($hakim);
				}

				if($paniteraId && $data['grup_id'] == 4){
					$panitera['user_id'] = $id;
					$panitera['panitera_id'] = $paniteraId;

					$this->users_m->savePanitera($panitera);
				}
			}

			if($isProfile){

				redirectOrBack(base_url().'settings/users/profile');
			}else{
				
				redirect(base_url().'settings/users');
			}

		}
	}
}