<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class Perkara extends BaseController {

	public function __construct(){
		parent::__construct();

		$this->load->model('perkara_m');
	}

	public function index()
	{
		$footer['scripts'] = [
			'assets/js/perkara.js'
		];

		$footer['js_main'] = "DaftarPerkara";

		$this->render('perkara', [], $footer);
	}

	public function lists()
	{

		$footer['scripts'] = [
			'assets/js/perkara_list.js'
		];

		$this->load->model('hakim_m');
		$this->load->model('panitera_m');

		$type = $this->input->get('type');
		$perf = $this->input->get('perf');
		$list = $this->input->get('list');
		
		$tgl_start = $this->input->get('tgl_start');
		$tgl_end = $this->input->get('tgl_end');

		// if(!$data['hakim'])  
			// redirectOrBack(base_url().'perkara');

		if($list == "kinerja_majelis"){
			$hakimId = $this->input->get('hakim_id');
			$data['hakim'] = $this->hakim_m->find($hakimId);

			$data['params']['km_id'] = $hakimId;

			if($type == "selesai"){
				$data['title'] = "Daftar Perkara Selesai";
				$data['perfText'] = "Perkara Selesai";
			}else if($type == "sisa"){
				$data['title'] = "Daftar Sisa Perkara";
				$data['perfText'] = "Sisa Perkara";
			}else if($type == "belum_minut"){
				$data['title'] = "Daftar Perkara Belum Minutasi";
				$data['perfText'] = "Perkara putus belum diminutasi";
			}else{
				$data['title'] = "Daftar Perkara Diterima";
				$data['perfText'] = "Perkara Diterima";
			}

			if($perf == "green"){
				$data['perfText'] = "Penyelesaian Perkara 1-2 Bulan ";
			}else if($perf == "yellow"){
				$data['perfText'] = "Penyelesaian Perkara 2-3 Bulan ";
			}else if($perf == "red"){
				$data['perfText'] = "Penyelesaian Perkara > 3 Bulan ";
			}

			if(validateDate($tgl_start) && validateDate($tgl_end)){
				$data['perfText'] .= ' - Periode Penetapan Majelis: '.$tgl_start.' s/d '.$tgl_end;
			}else{
				$data['perfText'] .= ' - Penetapan Majelis Tahun '.date('Y');
			}

		}else if($list == "konseptor"){
			$hakimId = $this->input->get('hakim_id');
			$data['hakim'] = $this->hakim_m->find($hakimId);

			$data['params']['konseptor_id'] = $hakimId;

			$data['title'] = "Daftar Perkara Dikonsep";
			$data['perfText'] = "";
	
			if(validateDate($tgl_start) && validateDate($tgl_end)){
				$data['perfText'] .= 'Periode Pengiriman Konsep: '.$tgl_start.' s/d '.$tgl_end;
			}else{
				$data['perfText'] .= 'Pengiriman Konsep Tahun '.date('Y');
			}
		}else if($list == "kinerja_pp"){

			$paniteraId = $this->input->get('pp_id');
			$data['hakim'] = $this->panitera_m->find($paniteraId);
			$data['params']['pp_id'] = $paniteraId;

			if($type == "selesai"){
				$data['title'] = "Daftar Perkara Selesai";
				$data['perfText'] = "Perkara Selesai";
			}else if($type == "sisa"){
				$data['title'] = "Daftar Sisa Perkara";
				$data['perfText'] = "Sisa Perkara";
			}else if($type == "konsep_h"){
				$data['title'] = "Daftar Perkara Dikonsep";
				$data['perfText'] = "Konsep Rangka Putusan 1-3 Hari";
			}else if($type == "konsep_k"){
				$data['title'] = "Daftar Perkara Dikonsep";
				$data['perfText'] = "Konsep Rangka Putusan 4-7 Hari";
			}else if($type == "konsep_m"){
				$data['title'] = "Daftar Perkara Dikonsep";
				$data['perfText'] = "Konsep Rangka Putusan > 7 Hari";
			}else if($type == "minut_h"){
				$data['title'] = "Daftar Perkara Minutasi";
				$data['perfText'] = "Minutasi 1-3 Hari";
			}else if($type == "minut_k"){
				$data['title'] = "Daftar Perkara Minutasi";
				$data['perfText'] = "Minutasi 4-7 Hari";
			}else if($type == "minut_m"){
				$data['title'] = "Daftar Perkara Minutasi";
				$data['perfText'] = "Minutasi > 7 Hari";
			}else{
				$data['title'] = "Daftar Perkara Diterima";
				$data['perfText'] = "Daftar Perkara Diterima";
			}
	
			if(validateDate($tgl_start) && validateDate($tgl_end)){
				$data['perfText'] .= ' - Periode Penunjukan PP: '.$tgl_start.' s/d '.$tgl_end;
			}else{
				$data['perfText'] .= ' - Penunjukan PP Tahun '.date('Y');
			}
		}


		
		$data['params']['type'] = $type;
		$data['params']['perf'] = $perf;
		$data['params']['list'] = $list;
		$data['params']['tgl_start'] = $tgl_start;
		$data['params']['tgl_end'] = $tgl_end;

		$register = $this->input->get('register');
		$minutasi = $this->input->get('minutasi');

		$this->render('perkara_list', $data, $footer);
	}

	public function kinerja($perkaraId){
		if(!$perkaraId) 
			redirectOrBack(base_url().'perkara');

		$this->load->model('perkara_m');
		
		$perkara = $this->perkara_m->kinerja($perkaraId);

		if(!$perkara) 
			redirectOrBack(base_url().'?404');

		$footer['scripts'] = [
			'assets/js/kinerja_majelis_detail.js'
		];

		$this->render('kinerja_perkara_detail', $perkara, $footer);	
	}

	public function add(){
		$this->restrict();

		if($_POST){
			$this->save();
		}else{
			$this->showForm();
		}
	}

	public function edit($perkaraId){
		if($_POST){
			$this->restrict();

			$this->save($perkaraId);
		}else{
			$this->showForm($perkaraId, !checkAccess());
		}
	}

	function noperkarapa_check($val, $id){
		$res = $this->perkara_m->valid_noperkarapa($val, $id);

		if(!$res) 
			$this->form_validation->set_message('noperkarapa_check', 'Nomor Perkara PA '.$val.' sudah ada');

		return $res;
	}

	function noperkarapta_check($val, $id){
		if($val == '') return true;

		$res = $this->perkara_m->valid_noperkarapta($val, $id);

		if(!$res) 
			$this->form_validation->set_message('noperkarapta_check', 'Nomor Perkara PTA '.$val.' sudah ada');

		return $res;
	}

	private function save($id = false){
		$this->load->model('hakim_m');
		$this->load->model('panitera_m');

	
		if(accessAdminOperator()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('nomor_perkara_banding', 'Nomor Perkara PTA', 'callback_noperkarapta_check['.$id.']');
			$this->form_validation->set_rules('nomor_perkara_pa', 'Nomor Perkara PA', 'required|callback_noperkarapa_check['.$id.']');
			$this->form_validation->set_rules('nama_pembanding', 'Nama Pembanding', 'required');
			$this->form_validation->set_rules('nama_terbanding', 'Nama Terbanding', 'required');
			$this->form_validation->set_rules('nama_satker', 'Nama PA Asal', 'required');
			$this->form_validation->set_rules('tgl_mohon_banding', 'Tgl. Mohon Banding', 'required');
		}

		if (accessAdminOperator() && $this->form_validation->run() == FALSE){

			/** VALIDATION FALSE **/

			$this->showForm($id, [
				'perkara_id' => $id,
				'nomor_perkara_pa' => set_value('nomor_perkara_pa'),
				'nomor_perkara_banding' => set_value('nomor_perkara_banding'),
				'nama_pembanding' => set_value('nama_pembanding'),
				'nama_terbanding' => set_value('nama_terbanding'),
				'nama_satker' => set_value('nama_satker'),
				'tgl_mohon_banding' => set_value('tgl_mohon_banding'),
				'tgl_kirim_berkas' => set_value('tgl_kirim_berkas'),
				'berkas_kembali_kl' => set_value('berkas_kembali_kl'),
				'tgl_register' => set_value('tgl_register'),
				'tgl_penetapan_majelis' => set_value('tgl_penetapan_majelis'),
				'tgl_penunjukan_pp' => set_value('tgl_penunjukan_pp'),
				'tgl_phs' => set_value('tgl_phs'),
				'tgl_terima_berkas_pp' => set_value('tgl_terima_berkas_pp'),
				'tgl_kirim_konsep' => set_value('tgl_kirim_konsep'),
				'tgl_sidang_pertama' => set_value('tgl_sidang_pertama'),
				'tgl_putusan_sela' => set_value('tgl_putusan_sela'),
				'tgl_putusan' => set_value('tgl_putusan'),
				
				'jenis_putus_id' => set_value('jenis_putus_id'),
				'jenis_putus_nama' => $this->perkara_m->jenis_putusan_nama(set_value('jenis_putus_id')),

				'tgl_minutasi' => set_value('tgl_minutasi'),
				'tgl_serah_panmud' => set_value('tgl_serah_panmud'),
				'tgl_kirim_pa' => set_value('tgl_kirim_pa'),
				'tgl_serah_meja3' => set_value('tgl_serah_meja3'),
				'tgl_upload' => set_value('tgl_upload'),
				'tgl_anonimasi' => set_value('tgl_anonimasi'),
				'tgl_arsip' => set_value('tgl_arsip'),
				'km_id' => set_value('km_id'),
				'nama_km' => $this->hakim_m->nama(set_value('km_id')),
				'konseptor_id' => set_value('konseptor_id'),
				'nama_konseptor' => $this->hakim_m->nama(set_value('konseptor_id')),
				'pp_id' => set_value('pp_id'),
				'nama_pp' => $this->panitera_m->nama(set_value('pp_id')),
				'keterangan' => set_value('keterangan'),

				'hakim_1' => set_value('hakim_1'),
				'hakim_1_nama' => $this->hakim_m->nama(set_value('hakim_1')),
				'resume_1' => set_value('resume_1'),
				'hakim_2' => set_value('hakim_2'),
				'hakim_2_nama' => $this->hakim_m->nama(set_value('hakim_2')),
				'resume_2' => set_value('resume_2'),
			]);

		} else {

			$nomor_perkara_pa = $this->input->post('nomor_perkara_pa');
			$nomor_perkara_banding = $this->input->post('nomor_perkara_banding');
			$nama_pembanding = $this->input->post('nama_pembanding');
			$nama_terbanding = $this->input->post('nama_terbanding');
			$nama_satker = $this->input->post('nama_satker');
			$tabayun = $this->input->post('tabayun');
			$tgl_mohon_banding = $this->input->post('tgl_mohon_banding');
			$tgl_kirim_berkas = $this->input->post('tgl_kirim_berkas');
			$berkas_kembali_kl = $this->input->post('berkas_kembali_kl');
			$tgl_register = $this->input->post('tgl_register');
			$tgl_penetapan_majelis = $this->input->post('tgl_penetapan_majelis');
			$tgl_penunjukan_pp = $this->input->post('tgl_penunjukan_pp');
			$tgl_phs = $this->input->post('tgl_phs');
			$tgl_terima_berkas_pp = $this->input->post('tgl_terima_berkas_pp');
			$tgl_kirim_konsep = $this->input->post('tgl_kirim_konsep');
			$tgl_sidang_pertama = $this->input->post('tgl_sidang_pertama');
			$tgl_putusan_sela = $this->input->post('tgl_putusan_sela');
			$tgl_putusan = $this->input->post('tgl_putusan');
			$jenis_putusan = $this->input->post('jenis_putus_id');
			$tgl_minutasi = $this->input->post('tgl_minutasi');
			$tgl_serah_panmud = $this->input->post('tgl_serah_panmud');
			$tgl_kirim_pa = $this->input->post('tgl_kirim_pa');
			$tgl_serah_meja3 = $this->input->post('tgl_serah_meja3');
			$tgl_upload = $this->input->post('tgl_upload');
			$tgl_anonimasi = $this->input->post('tgl_anonimasi');
			$tgl_arsip = $this->input->post('tgl_arsip');
			$km_id = $this->input->post('km_id');
			$konseptor_id = $this->input->post('konseptor_id');
			$pp_id = $this->input->post('pp_id');
			$keterangan = $this->input->post('keterangan');

			$data = [
				'nomor_perkara_banding' => $nomor_perkara_banding,
				'nomor_perkara_pa' => $nomor_perkara_pa,
				'nama_pembanding' => $nama_pembanding,
				'nama_terbanding' => $nama_terbanding,
				'nama_satker' => $nama_satker,
				'tabayun' => $tabayun != 1 ? 0 : 1,
				'tgl_mohon_banding' => toSQLDate($tgl_mohon_banding)
			];

			if($id){
				$data = array_merge($data, [
					'tgl_kirim_berkas' => toSQLDate($tgl_kirim_berkas),
					'berkas_kembali_kl' => toSQLDate($berkas_kembali_kl),
					'tgl_register' => toSQLDate($tgl_register),
					'tgl_penetapan_majelis' => toSQLDate($tgl_penetapan_majelis),
					'tgl_penunjukan_pp' => toSQLDate($tgl_penunjukan_pp),
					'tgl_phs' => toSQLDate($tgl_phs),
					'tgl_terima_berkas_pp' => toSQLDate($tgl_terima_berkas_pp),
					'tgl_kirim_konsep' => toSQLDate($tgl_kirim_konsep),
					'tgl_sidang_pertama' => toSQLDate($tgl_sidang_pertama),
					'tgl_putusan_sela' => toSQLDate($tgl_putusan_sela),
					'tgl_putusan' => toSQLDate($tgl_putusan),
					'jenis_putus_id' => $jenis_putusan,
					'jenis_putus_text' => $this->perkara_m->jenis_putusan_nama($jenis_putusan),
					'tgl_minutasi' => toSQLDate($tgl_minutasi),
					'tgl_serah_panmud' => toSQLDate($tgl_serah_panmud),
					'tgl_kirim_pa' => toSQLDate($tgl_kirim_pa),
					'tgl_serah_meja3' => toSQLDate($tgl_serah_meja3),
					'tgl_upload' => toSQLDate($tgl_upload),
					'tgl_anonimasi' => toSQLDate($tgl_anonimasi),
					'tgl_arsip' => toSQLDate($tgl_arsip),
					'km_id' => $km_id,
					'pp_id' => $pp_id,
					'konseptor_id' => $konseptor_id,
					'nama_km' => $this->hakim_m->nama($km_id),
					'nama_pp' => $this->panitera_m->nama($pp_id),
					'nama_konseptor' => $this->hakim_m->nama($konseptor_id),
					'keterangan' => $keterangan,
				]);
			}

			if(!$id && accessAdminOperator()){
				/** Add **/

				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = "admin";

				$perkaraId = $this->perkara_m->add($data);

				if($perkaraId){
					$this->saveHakimAnggota($perkaraId);
				}

				$this->session->set_flashdata('success', 'Perkara baru berhasil ditambahkan.');
			}else{
				/** Edit **/

				$data = array_filter($data);

				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('username');

				$simpan = $this->perkara_m->update($data, $id);

				if($simpan){
					$this->saveHakimAnggota($id);
				}

				$this->session->set_flashdata('success', 'Perkara berhasil diperbarui.');
			}

			
			if(!$id){
				redirectOrBack(base_url().'perkara');
			}else{
				redirect(base_url().'perkara');
			}

		}

	}

	private function saveHakimAnggota($perkaraId){
		$hakimAgt = [];

		$hakim1 = $this->input->post('hakim_1');
		$hakim1nama = $this->hakim_m->nama($hakim1);
		$resume1 = $this->input->post('resume_1');

		if($hakim1nama){
			$hakimAgt[] = [
				'perkara_id' => $perkaraId,
				'urutan' => '1',
				'hakim_id' => $hakim1,
				'hakim_nama' => $hakim1nama,
				'resume' => $resume1 == "1" ? "Y" : "T"
			];
		}

		$hakim2 = $this->input->post('hakim_2');
		$hakim2nama = $this->hakim_m->nama($hakim2);
		$resume2 = $this->input->post('resume_2');

		if($hakim2nama){
			$hakimAgt[] = [
				'perkara_id' => $perkaraId,
				'urutan' => '2',
				'hakim_id' => $hakim2,
				'hakim_nama' => $hakim2nama,
				'resume' => $resume2 == "1" ? "Y" : "T"
			];
		}

		$hakim3 = $this->input->post('hakim_3');
		$hakim3nama = $this->hakim_m->nama($hakim3);
		$resume3 = $this->input->post('resume_3');

		if($hakim3nama){
			$hakimAgt[] = [
				'perkara_id' => $perkaraId,
				'urutan' => '3',
				'hakim_id' => $hakim3,
				'hakim_nama' => $hakim3nama,
				'resume' => $resume3 == "1" ? "Y" : "T"
			];
		}

		$hakim4 = $this->input->post('hakim_4');
		$hakim4nama = $this->hakim_m->nama($hakim4);
		$resume4 = $this->input->post('resume_4');

		if($hakim3nama){
			$hakimAgt[] = [
				'perkara_id' => $perkaraId,
				'urutan' => '4',
				'hakim_id' => $hakim4,
				'hakim_nama' => $hakim4nama,
				'resume' => $resume4 == "1" ? "Y" : "T"
			];
		}

		$this->perkara_m->saveHakimAnggota($perkaraId, $hakimAgt);
	}

	private function showForm($perkaraId = false, $perkara = [], $isReadOnly = false){
		$footer['scripts'] = [
			'assets/js/validation.js',
			'assets/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js',
			'assets/js/perkara_form.js'
		];

		$this->load->model('perkara_m');

		$data['jenis_putusan'] = $this->perkara_m->jenis_putusan();

		if($perkaraId && !$_POST){
			$perkara = $this->perkara_m->find($perkaraId);

			$hakimAgt = $this->perkara_m->getHakimAnggota($perkaraId);

			if($hakimAgt){
				foreach ($hakimAgt as $rha) {
					$perkara['hakim_' . $rha['urutan']] = $rha['hakim_id'];
					$perkara['hakim_' . $rha['urutan'] . '_nama'] = $rha['hakim_nama'];
					$perkara['resume_' . $rha['urutan']] = $rha['resume'] == 'Y' ? 1 : 0;
				}
			}

			if(!$perkara) 
				redirectOrBack(base_url().'perkara?404');

			$perkara['tgl_mohon_banding'] = getDateShort($perkara['tgl_mohon_banding']);
			$perkara['tgl_kirim_berkas'] = getDateShort($perkara['tgl_kirim_berkas']);
			$perkara['berkas_kembali_kl'] = getDateShort($perkara['berkas_kembali_kl']);
			$perkara['tgl_register'] = getDateShort($perkara['tgl_register']);
			$perkara['tgl_penetapan_majelis'] = getDateShort($perkara['tgl_penetapan_majelis']);
			$perkara['tgl_penunjukan_pp'] = getDateShort($perkara['tgl_penunjukan_pp']);
			$perkara['tgl_phs'] = getDateShort($perkara['tgl_phs']);
			$perkara['tgl_terima_berkas_pp'] = getDateShort($perkara['tgl_terima_berkas_pp']);
			$perkara['tgl_kirim_konsep'] = getDateShort($perkara['tgl_kirim_konsep']);
			$perkara['tgl_sidang_pertama'] = getDateShort($perkara['tgl_sidang_pertama']);
			$perkara['tgl_putusan_sela'] = getDateShort($perkara['tgl_putusan_sela']);
			$perkara['tgl_putusan'] = getDateShort($perkara['tgl_putusan']);
			$perkara['tgl_minutasi'] = getDateShort($perkara['tgl_minutasi']);
			$perkara['tgl_serah_panmud'] = getDateShort($perkara['tgl_serah_panmud']);
			$perkara['tgl_kirim_pa'] = getDateShort($perkara['tgl_kirim_pa']);
			$perkara['tgl_serah_meja3'] = getDateShort($perkara['tgl_serah_meja3']);
			$perkara['tgl_upload'] = getDateShort($perkara['tgl_upload']);
			$perkara['tgl_anonimasi'] = getDateShort($perkara['tgl_anonimasi']);
			$perkara['tgl_arsip'] = getDateShort($perkara['tgl_arsip']);

			$perkara['km_id'] = $perkara['km_id'] == 0 ? "" : $perkara['km_id'];
			$perkara['pp_id'] = $perkara['pp_id'] == 0 ? "" : $perkara['pp_id'];

		}

		$data['perkara'] = $perkara;

		// var_dump($perkara);

		$this->render('perkara_form', $data , $footer);
	}

	// public function sisa_perkara(){
	// 	$footer['scripts'] = [
	// 		
	// 		
	// 		'assets/js/sisa_perkara.js'
	// 	];

	// 	$this->render('sisa_perkara', [], $footer);
	// }

	public function monitoring_perkara(){
		$footer['scripts'] = [
			
			
			
			
			
			'assets/js/monitoring_perkara.js'
		];

		$this->render('monitoring_perkara', [], $footer);
	}



	

}
