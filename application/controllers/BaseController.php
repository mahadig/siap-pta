<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseController extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('username')){
            redirect(base_url() . 'auth/login');
        }

        $this->userAccess = intval($this->session->userdata('grup_id'));
        
    }

    function render($view, $data = [], $scripts = []){
        $this->load->view('templates/header');
        $this->load->view('pages/' . $view, $data);
        $this->load->view('templates/footer', $scripts);
    }

    function accessAdminOperator(){

        if(!accessAdminOperator()){
            redirect(base_url());
        }

    }

    function accessMajelis(){
        
        if(!accessMajelis()){
            redirect(base_url());
        }
    }

    function accessMeja3(){
        
        if(!accessMeja3()){
            redirect(base_url());
        }
    }

    function restrict(){
        $userAccess = intval($this->session->userdata('grup_id'));
        
        if(!$userAccess){
            redirect(base_url());
        }
    }
}