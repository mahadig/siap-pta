<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/BaseController.php';

class E_laporan extends BaseController {

	public function __construct(){
		parent::__construct();

		// $this->restrict();

		$this->load->model('e_laporan_m');
		$this->load->model('panitera_m');
	}

	public function index()
	{
		$bulan = intval($this->input->get('bulan'));
		$tahun = intval($this->input->get('tahun'));

		if($bulan <=0 || $bulan > 12){
			$bulan = date('m');
		}

		if(!isValidYear($tahun)){
			$tahun = date('Y');
		}

		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;

		$footer['scripts'] = [
			'assets/plugins/moment/moment.js',
			'assets/js/e_laporan.js'
		];

		$this->render('e_laporan', $data, $footer);
	}

	public function singkron_lists()
	{
		$footer['scripts'] = [
			'assets/plugins/moment/moment.js',
			
			
			'assets/js/singkron_lists.js'
		];

		$paniteraId = $this->input->get('pp_id');

		$data['panitera'] = $this->panitera_m->find($paniteraId);

		$tgl_start = $this->input->get('tgl_start');
		$tgl_end = $this->input->get('tgl_end');

		if(validateDate($tgl_start) && validateDate($tgl_end)){
			$data['perfText'] = 'Periode Telaah E-Laporan: '.$tgl_start.' s/d '.$tgl_end;
		}else{
			$data['perfText'] = 'Periode Telaah E-Laporan '.date('Y');
		}

		$data['params']['pp_id'] = $paniteraId;
		$data['params']['tgl_start'] = $tgl_start;
		$data['params']['tgl_end'] = $tgl_end;

		$this->load->model('panitera_m');

		$this->render('singkron_lists', $data, $footer);
	}

	public function lists()
	{
		$footer['scripts'] = [
			'assets/plugins/moment/moment.js',
			'assets/js/e_laporan_list.js'
		];

		$paniteraId = $this->input->get('pp_id');

		$data['panitera'] = $this->panitera_m->find($paniteraId);

		$tgl_start = $this->input->get('tgl_start');
		$tgl_end = $this->input->get('tgl_end');

		if(validateDate($tgl_start) && validateDate($tgl_end)){
			$data['perfText'] = 'Periode Telaah E-Laporan: '.$tgl_start.' s/d '.$tgl_end;
		}else{
			$data['perfText'] = 'Periode Telaah E-Laporan '.date('Y');
		}

		$data['params']['pp_id'] = $paniteraId;
		$data['params']['tgl_start'] = $tgl_start;
		$data['params']['tgl_end'] = $tgl_end;

		$this->load->model('panitera_m');

		$this->render('e_laporan_list', $data, $footer);
	}

	public function edit($satkerId){
		$this->accessAdminOperator();

		if($_POST){
			$this->save($satkerId);
		}else{
			$this->showForm($satkerId);
		}
	}

	public function nilai($satkerId){
		$this->accessAdminOperator();

		if($_POST){
			$this->saveNilai($satkerId);
		}else{
			$this->showNilaiForm($satkerId);
		}
	}

	public function check_singkronisasi(){
		$date = date('Y-m-d');
		$userId = $this->session->userdata('panitera_id');

		if(!$userId) redirect(base_url().'e_laporan?forbidden');

		$check = $this->e_laporan_m->checkSingkron($userId, $date);
		
		if(!$check){
			
			$data = [
				'pp_id' => $userId,
				'nama_pp' => $this->panitera_m->nama($userId),
				'tgl_singkron' => $date,
				'created_at' => date('Y-m-d H:i:s')
			];

			$this->e_laporan_m->singkron($data);
		}

		$this->session->set_userdata('singkron', 'Y');

		$this->session->set_flashdata('successSingkron', 'oke');

		redirect(base_url().'e_laporan');
	}

	private function showNilaiForm($satkerId = false){
		$this->accessAdminOperator();

		$footer['scripts'] = [
			'assets/js/validation.js',
			'assets/js/e_laporan_form.js'
		];

		$bulan = intval($this->input->get('bulan'));
		$tahun = intval($this->input->get('tahun'));

		if($bulan != 0 && $tahun != 0){
			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;
		}else if($tahun != 0){
			$period = $tahun.'-'.date('m');
		}else{
			$period = date('Y-m');
		}

		$arrPeriod = explode("-", $period);

		$data = [];
		$data['period'] = getBulanName($arrPeriod[1]) . ' ' . $arrPeriod[0];

		if($satkerId){
			$data['row'] = $this->e_laporan_m->findBySatker($satkerId, $bulan, $tahun);

			if($data['row']){
				$data['row']['nama_pp'] = $this->panitera_m->nama($data['row']['pp_id']);
				$data['row']['tgl_konfirmasi'] = getDateShort($data['row']['tgl_konfirmasi']);
				$data['row']['tgl_telaah'] = getDateShort($data['row']['tgl_telaah']);
			}
		}

		$this->render('e_laporan_nilai', $data, $footer);
	}

	private function showForm($satkerId = false, $hakim = []){
		$this->accessAdminOperator();

		$footer['scripts'] = [
			'assets/js/validation.js',
			'assets/js/e_laporan_form.js'
		];

		$bulan = intval($this->input->get('bulan'));
		$tahun = intval($this->input->get('tahun'));

		if($bulan != 0 && $tahun != 0){
			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;
		}else if($tahun != 0){
			$period = $tahun.'-'.date('m');
		}else{
			$period = date('Y-m');
		}

		$arrPeriod = explode("-", $period);

		$data = [];
		$data['period'] = getBulanName($arrPeriod[1]) . ' ' . $arrPeriod[0];

		if($satkerId){
			$data['row'] = $this->e_laporan_m->findBySatker($satkerId, $bulan, $tahun);

			if($data['row']){
				$data['row']['nama_pp'] = $this->panitera_m->nama($data['row']['pp_id']);
				$data['row']['tgl_konfirmasi'] = getDateShort($data['row']['tgl_konfirmasi']);
				$data['row']['tgl_telaah'] = getDateShort($data['row']['tgl_telaah']);
			}
		}

		$this->render('e_laporan_form', $data, $footer);
	}

	private function saveNilai($id = false){
		$this->accessAdminOperator();

			$bulan = intval($this->input->get('bulan'));
			$tahun = intval($this->input->get('tahun'));


        	$data['satker_id'] = $id;
        	$data['nama_satker'] = $this->e_laporan_m->namaSatker($id);

        	$nilai = $this->input->post('nilai');

			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;

			$nilai = array_map(function($r){
				return intval($r);
			}, $nilai);

			$intNilai = array_values($nilai);
			$data['nilai_telaah'] = array_sum($intNilai) / count($intNilai);


			if(validateDate($period, 'Y-m')){
				$dataNilai = false;

	        	$check = $this->e_laporan_m->findBySatker($id, $bulan, $tahun);

	        	if(isset($nilai) && is_array($nilai) && count($nilai) == 22){

	        		$dataNilai = $nilai;

	        	}

	        	if($check['data_id']){

	        		$data['updated_at'] = date('Y-m-d H:i:s');
					$data['updated_by'] = $this->session->userdata('username');

					$simpan = $this->e_laporan_m->update($data, $id, $period);

					if($dataNilai){
						$dataNilai['e_laporan_id'] = $check['data_id'];

						$this->e_laporan_m->saveNilai($dataNilai);
					}

					$this->session->set_flashdata('success', 'Data Telaah berhasil diperbarui.');

	        	}else{
					$data['periode'] = $period;
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$data['created_by'] = $this->session->userdata('username');

					$simpan = $this->e_laporan_m->add($data);

					if($dataNilai){
						$dataNilai['e_laporan_id'] = $simpan;
						$this->e_laporan_m->saveNilai($dataNilai);
					}

					$this->session->set_flashdata('success', 'Data Telaah baru berhasil ditambahkan.');
	        	}			
			}

			if($dataNilai){
				$this->session->set_flashdata('success', 'Penilaian telaah laporan berhasil disimpan.');

				redirect(base_url().'e_laporan/nilai/'.$id.'?bulan='.$bulan.'&tahun='.$tahun);
			}else{
				redirect(base_url().'e_laporan');
			}
	}

	private function save($id = false){
		$this->accessAdminOperator();
		
			$bulan = intval($this->input->get('bulan'));
			$tahun = intval($this->input->get('tahun'));


        	$data['satker_id'] = $id;
        	$data['nama_satker'] = $this->e_laporan_m->namaSatker($id);
        	$data['pp_id'] = intval($this->input->post('pp_id'));
        	$data['nama_pp'] = $this->panitera_m->nama($data['pp_id']);
        	$data['hari_kerja'] = intval($this->input->post('hari_kerja'));
        	$data['tgl_konfirmasi'] = toSQLDate( $this->input->post('tgl_konfirmasi') );
        	$data['tgl_telaah'] = toSQLDate( $this->input->post('tgl_telaah') );
        	$data['keterangan'] = $this->input->post('keterangan');

			$zBulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
			$period = $tahun.'-'.$zBulan;

			if(validateDate($period, 'Y-m')){
	        	$check = $this->e_laporan_m->findBySatker($id, $bulan, $tahun);

	        	if($check['data_id']){

	        		$data['updated_at'] = date('Y-m-d H:i:s');
					$data['updated_by'] = $this->session->userdata('username');

					$simpan = $this->e_laporan_m->update($data, $id, $period);
					$this->session->set_flashdata('success', 'Data Telaah berhasil diperbarui.');

	        	}else{
					
					$data['periode'] = $period;
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$data['created_by'] = $this->session->userdata('username');

					$simpan = $this->e_laporan_m->add($data);
					$this->session->set_flashdata('success', 'Data Telaah baru berhasil ditambahkan.');

	        	}			
			}

			redirect(base_url()."e_laporan?bulan=$bulan&tahun=$tahun");
	}
}