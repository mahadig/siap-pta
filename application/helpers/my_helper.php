<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function redirectOrBack($val){
    if(isset($_SERVER['HTTP_REFERER'])){
        redirect($_SERVER['HTTP_REFERER']);
    }else{
        redirect($val);
    }
}

function elixir($file)
    {
        static $manifest = null;

        if (is_null($manifest)) {
            $manifest = json_decode(file_get_contents('dist/rev-manifest.json'), true);
        }

        if (isset($manifest[$file])) {
            return '/'.$manifest[$file];
        }

        throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
    }

function initial($name)
    {
        $words = explode(' ', trim($name));
        if (count($words) >= 3) {
            $words = array_slice($words, 0, 3);
            $nWords = $words;
            unset($nWords[count($nWords) -1]);
            return strtoupper(implode(' ', $nWords) . ' ' . substr($words[count($words) -1], 0, 1)) . '.';
        }
        // return makeInitialsFromSingleWord($name);
        return strtoupper($name);
    }

    function issetor(&$var = false, $default = false) {
    return (isset($var) && $var) ? $var : $default;
}

function isValidYear($year)
{
     // Convert to timestamp
     $start_year         =   strtotime(date('Y') - 100); //100 Years back
     $end_year           =   strtotime(date('Y')); // Current Year
     $received_year      =   strtotime($year);

    // Check that user date is between start & end
    return (($received_year >= $start_year) && ($received_year <= $end_year));
}

function boxColor($val, $r1, $r2){
    $classes = ['bg-success', 'bg-warning', 'bg-danger'];

    if($val != null){
        if($val >= 0 && $val <= $r1){
            echo $classes[0];
        }else if($val >= $r1 && $val <= $r2){
            echo $classes[1];
        }else{
            echo $classes[2];
        }
    }else{
        echo "bg-default";
    }
}

function activeClass($variable, $expected, $class){
    return $variable == $expected ? $class : '';
}

function eissetor(&$var = false, $default = false){
    echo issetor($var, $default);
}

function eissetor2($var = false, $default = false){
    $var2 = $var;
    echo issetor($var2, $default);
}

function makeInitialsFromSingleWord($name){
    preg_match_all('#([A-Z]+)#', $name, $capitals);
    if (count($capitals[1]) >= 2) {
        return substr(implode('', $capitals[1]), 0, 2);
    }
    return strtoupper(substr($name, 0, 2));
}


function validateDate($date, $format = 'd/m/Y')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function toSQLDate($date){
    if($date == '') return null;

    $d = DateTime::createFromFormat('d/m/Y', $date);

    return $d->format('Y-m-d');
}

function namaHari($tanggal){
    $daftar_hari = array(
     'Sunday' => 'Minggu',
     'Monday' => 'Senin',
     'Tuesday' => 'Selasa',
     'Wednesday' => 'Rabu',
     'Thursday' => 'Kamis',
     'Friday' => 'Jumat',
     'Saturday' => 'Sabtu'
    );
    
    $d = DateTime::createFromFormat('d/m/Y', $tanggal);

    $date = $d->format('Y/m/d');

    $namahari = date('l', strtotime($date));

    return $daftar_hari[$namahari];
}

function namaHari2($tanggal){
    $daftar_hari = array(
     'Sunday' => 'Minggu',
     'Monday' => 'Senin',
     'Tuesday' => 'Selasa',
     'Wednesday' => 'Rabu',
     'Thursday' => 'Kamis',
     'Friday' => 'Jumat',
     'Saturday' => 'Sabtu'
    );
    
    $d = DateTime::createFromFormat('Y-m-d', $tanggal);

    $date = $d->format('Y/m/d');

    $namahari = date('l', strtotime($date));

    return $daftar_hari[$namahari];
}

function getBulanName($bln){
    if ($bln == "01") { $bln_txt = "Januari"; }  
    else if ($bln == "02") { $bln_txt = "Februari"; }  
    else if ($bln == "03") { $bln_txt = "Maret"; }  
    else if ($bln == "04") { $bln_txt = "April"; }  
    else if ($bln == "05") { $bln_txt = "Mei"; }  
    else if ($bln == "06") { $bln_txt = "Juni"; }  
    else if ($bln == "07") { $bln_txt = "Juli"; }  
    else if ($bln == "08") { $bln_txt = "Agustus"; }  
    else if ($bln == "09") { $bln_txt = "September"; }  
    else if ($bln == "10") { $bln_txt = "Oktober"; }  
    else if ($bln == "11") { $bln_txt = "November"; }  
    else if ($bln == "12") { $bln_txt = "Desember"; }   
    else { $bln_txt = ""; }

    return $bln_txt;
}

function getBulanNameShort($bln){
    if ($bln == "01") { $bln_txt = "Jan"; }  
    else if ($bln == "02") { $bln_txt = "Feb"; }  
    else if ($bln == "03") { $bln_txt = "Mar"; }  
    else if ($bln == "04") { $bln_txt = "Apr"; }  
    else if ($bln == "05") { $bln_txt = "Mei"; }  
    else if ($bln == "06") { $bln_txt = "Jun"; }  
    else if ($bln == "07") { $bln_txt = "Jul"; }  
    else if ($bln == "08") { $bln_txt = "Ags"; }  
    else if ($bln == "09") { $bln_txt = "Sep"; }  
    else if ($bln == "10") { $bln_txt = "Okt"; }  
    else if ($bln == "11") { $bln_txt = "Nov"; }  
    else if ($bln == "12") { $bln_txt = "Des"; }   
    else { $bln_txt = ""; }

    return $bln_txt;
}

function getDateFull ($tanggal) {
    $pc_satu    = explode(" ", $tanggal);
    if (count($pc_satu) < 2) {  
        $tgl1       = $pc_satu[0];
        $jam1       = "";
    } else {
        $jam1       = $pc_satu[1];
        $tgl1       = $pc_satu[0];
    }
    
    $pc_dua     = explode("-", $tgl1);
    $tgl        = $pc_dua[2];
    $bln        = $pc_dua[1];
    $thn        = $pc_dua[0];
    
    $datetime = \DateTime::createFromFormat('Y-m-d', $tanggal);
    
    
    return $tgl." ".getBulanName($bln)." ".$thn."  ".$jam1;
}

function romawi($angka)
{
    $hsl = "";
    if ($angka < 1 || $angka > 5000) { 
        // Statement di atas buat nentuin angka ngga boleh dibawah 1 atau di atas 5000
        $hsl = "Batas Angka 1 s/d 5000";
    } else {
        while ($angka >= 1000) {
            // While itu termasuk kedalam statement perulangan
            // Jadi misal variable angka lebih dari sama dengan 1000
            // Kondisi ini akan di jalankan
            $hsl .= "M"; 
            // jadi pas di jalanin , kondisi ini akan menambahkan M ke dalam
            // Varible hsl
            $angka -= 1000;
            // Lalu setelah itu varible angka di kurangi 1000 ,
            // Kenapa di kurangi
            // Karena statment ini mengambil 1000 untuk di konversi menjadi M
        }
    }


    if ($angka >= 500) {
        // statement di atas akan bernilai true / benar
        // Jika var angka lebih dari sama dengan 500
        if ($angka > 500) {
            if ($angka >= 900) {
                $hsl .= "CM";
                $angka -= 900;
            } else {
                $hsl .= "D";
                $angka-=500;
            }
        }
    }
    while ($angka>=100) {
        if ($angka>=400) {
            $hsl .= "CD";
            $angka -= 400;
        } else {
            $angka -= 100;
        }
    }
    if ($angka>=50) {
        if ($angka>=90) {
            $hsl .= "XC";
            $angka -= 90;
        } else {
            $hsl .= "L";
            $angka-=50;
        }
    }
    while ($angka >= 10) {
        if ($angka >= 40) {
            $hsl .= "XL";
            $angka -= 40;
        } else {
            $hsl .= "X";
            $angka -= 10;
        }
    }
    if ($angka >= 5) {
        if ($angka == 9) {
            $hsl .= "IX";
            $angka-=9;
        } else {
            $hsl .= "V";
            $angka -= 5;
        }
    }
    while ($angka >= 1) {
        if ($angka == 4) {
            $hsl .= "IV"; 
            $angka -= 4;
        } else {
            $hsl .= "I";
            $angka -= 1;
        }
    }

    return ($hsl);
}

function getDateFullNoTime ($tanggal) {
    $pc_satu    = explode(" ", $tanggal);
    if (count($pc_satu) < 2) {  
        $tgl1       = $pc_satu[0];
    } else {
        $tgl1       = $pc_satu[0];
    }
    
    $pc_dua     = explode("-", $tgl1);
    $tgl        = $pc_dua[2];
    $bln        = $pc_dua[1];
    $thn        = $pc_dua[0];
    
    $datetime = \DateTime::createFromFormat('Y-m-d', $tanggal);
    
    
    return $tgl." ".getBulanName($bln)." ".$thn;
}

function getDateShort($date){
    if($date == '' || $date == '0000-00-00') return '';

    $d = DateTime::createFromFormat('Y-m-d', $date);

    return $d->format('d/m/Y');
}

function selisih($tanggal1, $tanggal2){
    $awal  = date_create(toSQLDate($tanggal1));
    $akhir = date_create(toSQLDate($tanggal2)); // waktu sekarang
    $diff  = date_diff( $awal, $akhir );

    $selisih = '';

    if($diff->y != 0)
    $selisih .= $diff->y . ' tahun ';

    if($diff->m != 0)
    $selisih .= $diff->m . ' bulan ';
    
    if($diff->d != 0)
    $selisih .= $diff->d . ' hari ';

    return $selisih;
}

function optionBulan($selected){
    $bulan=array("", "Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
    $jlh_bln=count($bulan);
    for($c=1; $c<$jlh_bln; $c++){
        if($selected == $c){
            echo"<option selected value=$c> $bulan[$c] </option>";
        }else{
            echo"<option value=$c> $bulan[$c] </option>";
        }
    }
}

function optionTahun($tahun){
    $ci =& get_instance();
    // $ci->load->model('general_m');

    $data = $ci->general_m->getTahunPerkara();

    foreach ($data as $row) {
        $selected = "";
        
        if($row->tahun == $tahun)
            $selected = "selected";

        echo "<option value=$row->tahun $selected>$row->tahun</option>";
    }
}

function menuActive($s1){
    $ci =& get_instance();

    $segment = $ci->uri->segment(1);

    if($s1 == $segment){
      echo "kt-menu__item--here kt-menu__item--open";
    }

}

function onlyAdmin(){
    return checkAccess(1);
}

function checkAccess($v = 0){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    if($v){
        return $hakAkses == $v;
    }else{
        return $hakAkses != 0 && $hakAkses < 6;
    }
}

function accessAdmin(){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    return $hakAkses == 1;
}

function accessAdminOperator(){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    return $hakAkses == 1 || $hakAkses == 2;
}

function accessMajelis(){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    return $hakAkses >= 1 && $hakAkses <= 4;
}

function accessPP(){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    return $hakAkses == 4;
}

function accessMeja3(){
    $ci =& get_instance();

    $hakAkses = intval($ci->session->userdata('grup_id'));

    return $hakAkses == 5;
}

function optionTahunPutusan($tahun){
    $ci =& get_instance();
    // $ci->load->model('general_m');

    $data = $ci->general_m->getTahunPutusan();

    foreach ($data as $row) {
        $selected = "";
        
        if($row->tahun == $tahun)
            $selected = "selected";

        echo "<option value=$row->tahun $selected>$row->tahun</option>";
    }
}
