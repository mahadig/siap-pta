$(function (e) {
    "use strict";

    // $('.singledate').daterangepicker({
        
    //     locale: {
	   //    format: 'DD/MM/YYYY',
    //       cancelLabel: 'Clear'
	   //  },
    //     singleDatePicker: true,
    //     // showDropdowns: true,
    //     // autoUpdateInput: false,
    //     autoApply: true
    // });

    $('.singledate').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        language: "id",
        daysOfWeekDisabled: "0,6",
        daysOfWeekHighlighted: "0,6"
    });

    var base = (document.querySelector('base') || {}).href;

	//SELECT2
    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        
        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
        if (repo.id) {
            markup += "<div class='select2-result-repository__description'><small>" + repo.nip + "</small></div>";
        }

        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.nama || repo.text;
    }

	$("#jenis_putusan").select2({placeholder: "Jenis Putus", minimumResultsForSearch: -1,allowClear: true,});
    
    $("#pp_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan NIP / Nama Penelaah";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketikkan NIP / Nama Penelaah",
            allowClear: true,
            ajax: {
                url: base + 'api/panitera/cari',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $("#satker_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan Nama Satker";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketikkan Nama Satker",
            allowClear: true,
            ajax: {
                url: base + 'api/e_laporan/satker',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    // $("#nomor_perkara_banding").inputmask("99/AAAAA/9999/AAAA");

});