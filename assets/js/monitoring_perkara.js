"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };


        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // fixedHeader: true
            // scrollY: "60vh",
            // scrollCollapse: true,
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row px-3 '<'col-sm-12 col-md-5 filter-bar'><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>
            <'row'<'col-sm-12'tr>>
            <'row px-3 '<'col-sm-12 pt-1 col-md-5'i><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            pageLength: 25,
            order: [[ 0, "disabled" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },
            language: {
            url: base + "assets/datatable-id.json"
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/perkara/monitoring_perkara',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'perkara_id',
                    'nomor_perkara_banding',
                    'nomor_perkara_pa',
                    'proses_all',
                    'proses_kirim_berkas',
                    'proses_pmh',
                    'proses_pp',
                    'proses_phs',
                    'proses_rangka_putusan',
                    'proses_sidang',
                    'proses_minut',
                    'proses_serah',
                    'proses_kirim',
                    'proses_meja3',
                    'proses_anonim',
                    'proses_upload',
                    'proses_arsip'
                    ]
                },
            },
            drawCallback:function( settings){
                initPopovers();

                if(settings.iDraw === 1){
                    $('.filter-bar').html($('#filter').html());

                    $('.singledate').datepicker({
                        format: "dd/mm/yyyy",
                        clearBtn: true,
                        language: "id",
                        daysOfWeekDisabled: "0,6",
                        daysOfWeekHighlighted: "0,6"
                    }).on('changeDate', function (ev) {
                        var elPeriod = $('#filter_period')
                        
                        if(elPeriod.val() === ''){
                            elPeriod.val('tgl_register');
                        }

                        reload();
                    });
                }
                // call your function here
            },
            columns: [
            {data: 'perkara_id'},
            {data: 'nomor_perkara_banding'},
            {data: 'nomor_perkara_pa'},
            {data: 'proses_all'},
            {data: 'proses_kirim_berkas'},
            {data: 'proses_pmh'},
            {data: 'proses_pp'},
            {data: 'proses_phs'},
            {data: 'proses_rangka_putusan'},
            {data: 'proses_sidang'},
            {data: 'proses_minut'},
            {data: 'proses_serah'},
            {data: 'proses_kirim'},
            {data: 'proses_meja3'},
            {data: 'proses_anonim'},
            {data: 'proses_upload'},
            {data: 'proses_arsip'}
            ],
            rowCallback: function(row, data, index) {

                var i = 2;

                //PROSES ALL
                var p_all = parseInt(data.proses_all);
                var cn_all = "red";

                if(p_all < 0){
                    cn_all = "invalid";
                }else if(p_all >= 0 && p_all <= 60){
                    cn_all = "green";
                }else if(p_all <= 90){
                    cn_all = "yellow";
                }else if(isNaN(p_all)){
                    cn_all = '';
                }
                
                $("td:eq("+i+")", row).addClass(cn_all);
                i++;

                //PROSES KIRIM BERKAS
                var p_kirim_berkas = parseInt(data.proses_kirim_berkas);
                var cn_kirim_berkas = "red";

                if(p_kirim_berkas < 0){
                    cn_kirim_berkas = "invalid";
                }if(p_kirim_berkas >= 0 && data <= 30){
                    cn_kirim_berkas = "green";
                }else if(p_kirim_berkas > 30 &&data <= 45){
                    cn_kirim_berkas = "yellow";
                }else if(isNaN(p_kirim_berkas)){
                    cn_kirim_berkas = '';
                }
                $("td:eq("+i+")", row).addClass(cn_kirim_berkas);
                i++;

                // ** ATURAN
                //PROSES PMH
                var p_pmh = parseInt(data.proses_pmh);
                var cn_pmh = "red";

                if(p_pmh < 0){
                    cn_pmh = "invalid";
                }if(p_pmh >= 0 && p_pmh <= 3){
                    cn_pmh = "green";
                }else if(p_pmh > 3 &&p_pmh <= 7){
                    cn_pmh = "yellow";
                }else if(isNaN(p_pmh)){
                    cn_pmh = '';
                }
                $("td:eq("+i+")", row).addClass(cn_pmh);
                i++;

                // ** ATURAN
                //PROSES PP
                var p_pp = parseInt(data.proses_pp);
                var cn_pp = "red";

                if(p_pp < 0){
                    cn_pp = "invalid";
                }if(p_pp >= 0 && p_pp <= 3){
                    cn_pp = "green";
                }else if(p_pp > 3 && p_pp <= 7){
                    cn_pp = "yellow";
                }else if(isNaN(p_pp)){
                    cn_pp = '';
                }
                $("td:eq("+i+")", row).addClass(cn_pp);
                i++;

                //PROSES PHS
                var p_phs = parseInt(data.proses_phs);
                var cn_phs = "red";

                if(p_phs < 0){
                    cn_phs = "invalid";
                }if(p_phs >= 0 && p_phs <= 3){
                    cn_phs = "green";
                }else if(p_phs > 3 &&p_phs <= 7){
                    cn_phs = "yellow";
                }else if(isNaN(p_phs)){
                    cn_phs = '';
                }
                $("td:eq("+i+")", row).addClass(cn_phs);
                i++;

                //PROSES RANGKA PUTUSAN
                var p_rangka_putusan = parseInt(data.proses_rangka_putusan);
                var cn_rangka_putusan = "red";

                if(p_rangka_putusan < 0){
                    cn_rangka_putusan = "invalid";
                }if(p_rangka_putusan >= 0 && p_rangka_putusan <= 3){
                    cn_rangka_putusan = "green";
                }else if(p_rangka_putusan > 3 &&p_rangka_putusan <= 7){
                    cn_rangka_putusan = "yellow";
                }else if(isNaN(p_rangka_putusan)){
                    cn_rangka_putusan = '';
                }
                $("td:eq("+i+")", row).addClass(cn_rangka_putusan);
                i++;

                // PROSES SIDANG
                var p_sidang = parseInt(data.proses_sidang);
                var cn_sidang = "red";

                if(p_sidang < 0){
                    cn_sidang = "invalid";
                }if(p_sidang >= 0 && p_sidang <= 60){
                    cn_sidang = "green";
                }else if(p_sidang > 60 &&p_sidang <= 90){
                    cn_sidang = "yellow";
                }else if(isNaN(p_sidang)){
                    cn_sidang = '';
                }
                $("td:eq("+i+")", row).addClass(cn_sidang);
                i++;

                //PROSES MINUT
                var p_minut = parseInt(data.proses_minut);
                var cn_minut = "red";

                if(p_minut < 0){
                    cn_minut = "invalid";
                }if(p_minut >= 0 && p_minut <= 1){
                    cn_minut = "green";
                }else if(p_minut > 2 &&p_minut <= 5){
                    cn_minut = "yellow";
                }else if(isNaN(p_minut)){
                    cn_minut = '';
                }

                $("td:eq("+i+")", row).addClass(cn_minut);
                i++;

                //PROSES SERAH
                var p_serah = parseInt(data.proses_serah);
                var cn_serah = "red";

                if(p_serah < 0){
                    cn_serah = "invalid";
                }if(p_serah >= 0 && p_serah <= 1){
                    cn_serah = "green";
                }else if(p_serah > 2 &&p_serah <= 5){
                    cn_serah = "yellow";
                }else if(isNaN(p_serah)){
                    cn_serah = '';
                }

                $("td:eq("+i+")", row).addClass(cn_serah);
                i++;

                //PROSES KIRIM
                var p_kirim = parseInt(data.proses_kirim);
                var cn_kirim = "red";

                if(p_kirim < 0){
                    cn_kirim = "invalid";
                }if(p_kirim >= 0 && p_kirim <= 3){
                    cn_kirim = "green";
                }else if(p_kirim > 3 &&p_kirim <= 7){
                    cn_kirim = "yellow";
                }else if(isNaN(p_kirim)){
                    cn_kirim = '';
                }

                $("td:eq("+i+")", row).addClass(cn_kirim);
                i++;

                //PROSES MEJA 3
                var p_meja3 = parseInt(data.proses_meja3);
                var cn_meja3 = "red";

                if(p_meja3 < 0){
                    cn_meja3 = "invalid";
                }if(p_meja3 >= 0 && p_meja3 <= 3){
                    cn_meja3 = "green";
                }else if(p_meja3 > 3 &&p_meja3 <= 7){
                    cn_meja3 = "yellow";
                }else if(isNaN(p_meja3)){
                    cn_meja3 = '';
                }
                $("td:eq("+i+")", row).addClass(cn_meja3);
                i++;

                //PROSES ANONUM
                var p_anonim = parseInt(data.proses_anonim);
                var cn_anonim = "red";

                if(p_anonim < 0){
                    cn_anonim = "invalid";
                }if(p_anonim >= 0 && p_anonim <= 3){
                    cn_anonim = "green";
                }else if(p_anonim > 3 &&p_anonim <= 7){
                    cn_anonim = "yellow";
                }else if(isNaN(p_anonim)){
                    cn_anonim = '';
                }
                $("td:eq("+i+")", row).addClass(cn_anonim);
                i++;

                //PROSES UPLOAD
                var p_upload = parseInt(data.proses_upload);
                var cn_upload = "red";

                if(p_upload < 0){
                    cn_upload = "invalid";
                }if(p_upload >= 0 && p_upload <= 3){
                    cn_upload = "green";
                }else if(p_upload > 3 &&p_upload <= 7){
                    cn_upload = "yellow";
                }else if(isNaN(p_upload)){
                    cn_upload = '';
                }
                $("td:eq("+i+")", row).addClass(cn_upload);
                i++;

                //PROSES ARSIP
                var p_arsip = parseInt(data.proses_arsip);
                var cn_arsip = "red";

                if(p_arsip < 0){
                    cn_arsip = "invalid";
                }if(p_arsip >= 0 && p_arsip <= 3){
                    cn_arsip = "green";
                }else if(p_arsip > 3 &&p_arsip <= 7){
                    cn_arsip = "yellow";
                }else if(isNaN(p_arsip)){
                    cn_arsip = '';
                }
                $("td:eq("+i+")", row).addClass(cn_arsip);
                i++;
                
            },
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                visible: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 2,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a href="'+base +'perkara/kinerja/'+full.perkara_id+'">'+ full.nomor_perkara_pa +'</a>\
                    </div>';
                }
            },
            {
                targets: 1,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a href="'+base +'perkara/kinerja/'+full.perkara_id+'">'+ full.nomor_perkara_banding +'</a>\
                    </div>';
                }
            },
            {
                targets: 3,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_all);

                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 4,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_kirim_berkas);
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 5,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_pmh);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 6,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_pp);

                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div title="Proses Persidangan" class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 7,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_phs);

                    if(isNaN(data)){
                        
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 8,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_rangka_putusan);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 9,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_sidang);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 10,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_minut);

                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 11,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_serah);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 12,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_kirim);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 13,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_meja3);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 14,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_anonim);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 15,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_upload);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },
            {
                targets: 16,
                width: '1%',
                orderable: false,
                render: function(data, type, full, meta) {
                    var data = parseInt(full.proses_arsip);
                    
                    if(isNaN(data)){
                        data = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ data +'\
                    </div>';
                }
            },


                ],
            });

        

    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        
        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
        if (repo.id) {
            markup += "<div class='select2-result-repository__description'><small>" + repo.nip + "</small></div>";
        }

        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.nama || repo.text;
    }

$("#km_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan NIP / Nama Ketua Majelis";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketua Majelis",
            allowClear: true,
            ajax: {
                url: base + 'api/hakim/cari',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

var reload = function(){
    var nomor_perkara_pa = $('#nomor_perkara_pa').val();
    var nomor_perkara_banding = $('#nomor_perkara_banding').val();
    var km_id = $('#km_id').val();
    var filter_period = $('#filter_period').val();
    var tgl_start = $('#tgl_start').val();
    var tgl_end = $('#tgl_end').val();


    var params = {
        filter: {
            nomor_perkara_pa,nomor_perkara_banding,km_id,filter_period,tgl_start,tgl_end
        }
    };

    datatable.ajax.url(base + 'api/perkara/monitoring_perkara?' + $.param(params)).load();
}

$("#formFilter").on('reset', function(event) {
    $("#km_id").empty();

  setTimeout(function() {
    reload();
  }, 10);

});

$('#nomor_perkara_banding').keyup(debounce(function(){
     reload();
},500));


$('#nomor_perkara_pa').keyup(debounce(function(){
     reload();
},500));

$('#km_id').change(function(){
     reload();
});

$(document).on('change', '#filter_period', function(){
    reload();
});

$(document).on('change', '#bulan', function(){
    reload();
});

$(document).on('keyup', '#tahun', debounce(function(){
     reload();
},500));


};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});