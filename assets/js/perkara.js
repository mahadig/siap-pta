"use strict";
var DaftarPerkara = (function () {
  var initTable2 = function () {
    var base = (document.querySelector("base") || {}).href;

    var table = $("#kt_table_2");

    var initPopover = function (el) {
      var skin = el.data("skin")
        ? "popover-" + el.data("skin")
        : "";
      var triggerValue = el.data("trigger")
        ? el.data("trigger")
        : "hover";
      el.popover({
        trigger: triggerValue,
        template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
      });
    };

    var clearPopover = function (el) {
      el.remove();
    };

    var clearPopovers = function () {
      $("div.popover").each(function () {
        clearPopover($(this));
      });
    };

    var initPopovers = function () {
      clearPopovers();

      $('[data-toggle="kt-popover"]').each(function () {
        initPopover($(this));
      });
    };

    var datatable = table.DataTable({
      scrollX: true,

      dom: `<'row px-3 '<'col-sm-12 col-md-6 filter-bar'><'col-sm-12 col-md-6 pt-2 dataTables_pager'lp>>
            <'row'<'col-sm-12'tr>>
            <'row px-3 '<'col-sm-12 pt-1 col-md-5'i><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>`,

      lengthMenu: [
        5, 10, 25, 50
      ],
      pageLength: 25,
      order: [
        [1, "desc"]
      ],
      language: {
        url: base + "assets/datatable-id.json"
      },
      searchDelay: 500,
      processing: true,
      serverSide: true,
      ajax: {
        url: base + "api/perkara",
        type: "GET",
        data: {
          columnsDef: [
            "perkara_id",
            "nomor_perkara_pa",
            "nomor_perkara_banding",
            "nama_pembanding",
            "nama_terbanding",
            "aksi"
          ]
        }
      },
      drawCallback: function (settings) {
        initPopovers();

        if (settings.iDraw === 1) {
          $(".filter-bar").html($("#filter").html());

          $(".singledate").datepicker({format: "dd/mm/yyyy", clearBtn: true, language: "id", daysOfWeekDisabled: "0,6", daysOfWeekHighlighted: "0,6"}).on("changeDate", function (ev) {
            var elPeriod = $("#filter_period");

            if (elPeriod.val() === "") {
              elPeriod.val("tgl_register");
            }

            reload();
          });
        }
      },
      columns: [
        {
          data: "perkara_id"
        }, {
          data: "nomor_perkara_banding"
        }, {
          data: "nomor_perkara_pa"
        }, {
          data: "nama_terbanding"
        }, {
          data: "nama_pembanding"
        }, {
          data: "aksi",
          responsivePriority: -1
        }
      ],
      columnDefs: [
        {
          targets: -1,
          title: "Aksi",
          width: "1%",
          orderable: false,
          render: function (data, type, full, meta) {
            if (meta.settings.json.authorized != true) {
              datatable.columns([-1]).visible(false);
              return "-";
            } else {
              return ('<div class="btn-group">\
                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                        <i class="mdi mdi-dots-horizontal"></i>\
                        </button>\
                        <div class="dropdown-menu">\
                        <a class="dropdown-item" href="' + base + "perkara/kinerja/" + full.perkara_id + '">Kinerja</a>\
                        <a class="dropdown-item" href="' + base + "perkara/edit/" + full.perkara_id + '">Ubah</a>\
                        <a class="dropdown-item kt_edit_button" data-id="' + full.perkara_id + '" href="javascript:;">Hapus</a>\
                        </div>\
                        </div>');
            }
          }
        }, {
          targets: 0,

          orderable: false,
          width: "1%",
          render: function (data, type, full, meta) {
            return ('\
                    <div class="text-center">\
                    ' + (
            meta.row + 1 + meta.settings._iDisplayStart) + "\
                    </div>");
          }
        }, {
          targets: 2,
          orderable: true,
          width: "5%",
          render: function (data, type, full, meta) {
            return ('\
                    <div class="text-left">\
                    <a href="' + base + "perkara/kinerja/" + full.perkara_id + '">' + full.nomor_perkara_pa + "</a>\
                    </div>");
          }
        }, {
          targets: 1,
          orderable: true,
          width: "5%",
          render: function (data, type, full, meta) {
            return ('\
                    <div class="text-left">\
                    <a href="' + base + "perkara/kinerja/" + full.perkara_id + '">' + full.nomor_perkara_banding + "</a>\
                    </div>");
          }
        }, {
          targets: 3,
          width: "20%",
          orderable: true,
          render: function (data, type, full, meta) {
            return ('\
                    <div class="nama">\
                    ' + full.nama_pembanding + "\
                    </div>");
          }
        }, {
          targets: 4,
          width: "16%",
          orderable: true,
          render: function (data, type, full, meta) {
            return ('\
                    <div class="nama">\
                    ' + full.nama_terbanding + "\
                    </div>");
          }
        }
      ]
    });

    $("body").on("click", ".kt_edit_button", function (e) {
      var id = $(this).data("id");
      var nama = $(this).data("nama");
      Swal.fire({
        title: "Anda yakin ingin menghapus data yang terpilih?",
        text: "Data yang dihapus tidak dapat dikembalikan",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya, Hapus",
        showLoaderOnConfirm: true,
        preConfirm: login => {
          return fetch(base + "api/perkara/delete?id=" + id).then(response => {
            if (!response.ok) {
              throw new Error(response.statusText);
            }
            return response.json();
          }).catch(error => {
            Swal.showValidationMessage(`Request failed: ${error}`);
          });
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(result => {
        if (result.value) {
          swal.fire("Dihapus!", "Data berhasil dihapus.", "success");

          reload();
        }
      });
    });

    function formatRepo(repo) {
      if (repo.loading) 
        return repo.text;
      
      var markup = "<div class='select2-result-repository clearfix'>" + "<div class='select2-result-repository__meta'>" + "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
      if (repo.id) {
        markup += "<div class='select2-result-repository__description'><small>" + repo.nip + "</small></div>";
      }

      return markup;
    }

    function formatRepoSelection(repo) {
      return repo.nama || repo.text;
    }

    $("#km_id").select2({
      language: {
        inputTooShort: function () {
          return "Ketikkan NIP / Nama Ketua Majelis";
        }
      },
      minimumInputLength: 1,
      placeholder: "Ketua Majelis",
      allowClear: true,
      ajax: {
        url: base + "api/hakim/cari",
        dataType: "json",
        delay: 250,
        data: function (params) {
          return {q: params.term, page: params.page};
        },
        processResults: function (data, params) {
          params.page = params.page || 1;

          return {results: data};
        },
        cache: true
      },
      escapeMarkup: function (markup) {
        return markup;
      },
      minimumInputLength: 1,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

    var getParams = function () {
      var nomor_perkara_pa = $("#nomor_perkara_pa").val();
      var nomor_perkara_banding = $("#nomor_perkara_banding").val();
      var km_id = $("#km_id").val();
      var filter_period = $("#filter_period").val();
      var tgl_start = $("#tgl_start").val();
      var tgl_end = $("#tgl_end").val();

      var params = {
        filter: {
          nomor_perkara_pa,
          nomor_perkara_banding,
          km_id,
          filter_period,
          tgl_start,
          tgl_end
        }
      };

      return params;
    };

    var reload = function () {
      var params = getParams();

      datatable.ajax.url(base + "api/perkara?" + $.param(params)).load();
    };

    $("#btnExcel").on("click", function () {
      var params = getParams();

      params["order"] = [
        {
          column: datatable.order()[0][0],
          dir: datatable.order()[0][1]
        }
      ];

      var url = base + "excel/excel_perkara?" + $.param(params) + $.param(datatable.ajax.params());

      window.open(url, "_blank");
    });

    $("#formFilter").on("reset", function (event) {
      $("#km_id").empty();
      $("#filter_period").val("");
      $("#tgl_start").val("");
      $("#tgl_end").val("");

      setTimeout(function () {
        reload();
      }, 10);
    });

    $("#nomor_perkara_banding").keyup(debounce(function () {
      reload();
    }, 500));

    $("#nomor_perkara_pa").keyup(debounce(function () {
      reload();
    }, 500));

    $("#km_id").change(function () {
      reload();
    });

    $(document).on("change", "#filter_period", function () {
      reload();
    });
  };

  return {
    init: function () {
      initTable2();
    }
  };
})();

jQuery(document).ready(function () {
  DaftarPerkara.init();
});