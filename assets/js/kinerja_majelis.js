"use strict";
var KinerjaMajelis = function() {

    var initTable2 = function() {

        $('.singledate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            language: "id",
            daysOfWeekDisabled: "0,6",
            daysOfWeekHighlighted: "0,6"
        }).on('changeDate', function (ev) {
            reload();
        });


        var base = (document.querySelector('base') || {}).href;
        var table = $('#kt_table_2');

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // fixedHeader: true
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            order: [[ 2, "desc" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/perkara/kinerja_perkara',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'km_id', 'nama_km', 'jumlah','perkara_selesai', 
                    'hijau', 'kuning', 'merah', 'kinerja','sisa'
                    ]
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'km_id'},
            {data: 'nama_km'},
            {data: 'jumlah'},
            {data: 'perkara_selesai'},
            {data: 'hijau'},
            {data: 'kuning'},
            {data: 'merah'},
            {data: 'kinerja'},
            {data: 'sisa'}
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                orderable: true,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a class="link" href="'+base +'kinerja_majelis/detail/'+full.km_id+'"><b>'+full.nama_km+'</b></a>\
                    </div>';
                }
            },
            {
                targets: 2,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.jumlah == null) full.jumlah = 0;

                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "beban";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.jumlah).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 3,
                orderable: true,
                render: function(data, type, full, meta) {
                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "selesai";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.perkara_selesai).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 4,
                orderable: true,
                render: function(data, type, full, meta) {
                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "selesai";
                        params.perf = "green";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.hijau).toString() +'</b></a>\
                    </div>';

                    // return '\
                    // <div class="text-center">\
                    // <a class="link" href="'+base +'perkara/lists/'+full.km_id+'/all"><b>'+full.beban+'</b></a>\
                    // </div>';
                }
            },
            {
                targets: 5,
                orderable: true,
                render: function(data, type, full, meta) {
                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "selesai";
                        params.perf = "yellow";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.kuning).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 6,
                orderable: true,
                render: function(data, type, full, meta) {
                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "selesai";
                        params.perf = "red";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.merah).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 7,
                orderable: true,
                render: function(data, type, full, meta) {
                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.kinerja).toString() +'%\
                    </div>';
                }
            },
                        {
                targets: 8,
                orderable: true,
                render: function(data, type, full, meta) {
                    var params = getParams().filter;
                        params.hakim_id = full.km_id;
                        params.type = "sisa";
                        params.list = "kinerja_majelis";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.jumlah - full.perkara_selesai).toString() +'</b></a>\
                    </div>';
                }
            }
            

                ],
            });

$('.singledate').datepicker({
    format: "dd/mm/yyyy",
    clearBtn: true,
    language: "id",
    daysOfWeekDisabled: "0,6",
    daysOfWeekHighlighted: "0,6"
}).on('changeDate', function (ev) {
    var elPeriod = $('#filter_period')
    
    if(elPeriod.val() === ''){
        elPeriod.val('tgl_register');
    }

    reload();
});

$('#btnExcel').on('click', function(){
    var params = getParams();

    params['order'] = [
        { 
            'column': datatable.order()[0][0], 
            'dir': datatable.order()[0][1] 
        }
    ];

    var url = base + 'excel/excel_kinerja_majelis?' + $.param(params) + $.param(datatable.ajax.params());
    
    window.open(url, '_blank');
});

// $('.singledate').on('apply.daterangepicker', function(ev, picker) {
//   reload();
// });

$("#formFilter").on('reset', function(event) {   
  setTimeout(function() {
    reload();
  }, 10);

});

var getParams = function(){
    var tgl_start = $('#tgl_start').val();
    var tgl_end = $('#tgl_end').val();
    
    var params = {
        filter: {
            tgl_start, tgl_end
        }
    };

    return params;
}

var reload = function(){
    var params = getParams();

    datatable.ajax.url(base + 'api/perkara/kinerja_perkara?' + $.param(params)).load();
}



};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    KinerjaMajelis.init();
});