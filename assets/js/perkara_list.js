"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };

        // $(".select2").select2({minimumResultsForSearch: -1});

        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        console.log(params);

        var datatable = table.DataTable({
            // responsive: false,
            // scrollY: false,
            // scrollX: false,
            // scrollCollapse: false,
            // Pagination settings
            dom: `<'row px-3 '<'col-sm-12 col-md-5 filter-bar'><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>
            <'row'<'col-sm-12'tr>>
            <'row px-3 '<'col-sm-12 pt-1 col-md-5'i><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>`,
            
            lengthMenu: [5, 10, 25, 50],
            pageLength: 25,
            order: [[ 0, "disabled" ]],
            language: {
            url: base + "assets/datatable-id.json"
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: base + 'api/perkara?' + $.param(params),
                // url: base + 'api/perkara',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'perkara_id','nomor_perkara_pa','nomor_perkara_banding','nama_pembanding', 'nama_terbanding','aksi',],
                },
            },
            drawCallback:function( settings){
                initPopovers();
                
                if(settings.iDraw === 1){
                    $('.filter-bar').html($('#filter').html());
                }
            },
            columns: [
            {data: 'perkara_id'},
            {data: 'nomor_perkara_banding'},
            {data: 'nomor_perkara_pa'},
            {data: 'nama_terbanding'},
            {data: 'nama_pembanding'},
            {data: 'aksi', responsivePriority: -1},
            ],
            columnDefs: [
            {
                targets: -1,
                title: 'Aksi',
                width: "1%",
                orderable: false,
                render: function(data, type, full, meta) {
                    if(meta.settings.json.authorized != true){
                        datatable.columns([-1]).visible(false);
                        return "-";
                    }else{

                        return '<div class="btn-group">\
                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                            <i class="mdi mdi-dots-horizontal"></i>\
                            </button>\
                            <div class="dropdown-menu">\
                            <a class="dropdown-item" href="'+base +'perkara/kinerja/'+full.perkara_id+'">Kinerja</a>\
                            <a class="dropdown-item" href="'+base +'perkara/edit/'+full.perkara_id+'">Ubah</a>\
                            <a class="dropdown-item kt_edit_button" data-id="'+full.perkara_id+'" href="javascript:;">Hapus</a>\
                            </div>\
                            </div>';
                        }

                        // return '\
                        // <div class="dropdown">\
                        // <a href="javascript:;" class="btn btn-sm btn-default btn-icon btn-icon-md" data-toggle="dropdown">\
                        // <i class="mdi mdi-dots-horizontal"></i>\
                        // </a>\
                        // <div class="dropdown-menu dropdown-menu-right">\
                        // <ul class="kt-nav">\
                        // <li class="kt-nav__item">\
                        // <a href="'+base +'perkara/edit/'+full.perkara_id+'" class="kt-nav__link">\
                        // <i class="kt-nav__link-icon flaticon2-list"></i>\
                        // <span class="kt-nav__link-text">Rincian</span>\
                        // </a>\
                        // </li>\
                        // <li class="kt-nav__item">\
                        // <a href="javascript:;" class="kt-nav__link kt_edit_button" data-id="'+full.perkara_id+'" data-nama="'+full.nama+'">\
                        // <i class="kt-nav__link-icon flaticon2-trash"></i>\
                        // <span class="kt-nav__link-text">Hapus</span>\
                        // </a>\
                        // </li>\
                        // </ul>\
                        // </div>\
                        // </div>\
                        // ';
                    // }
                },
            },
            {
                targets: 0,
                // visible: false,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {
                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1 + meta.settings._iDisplayStart)+'\
                    </div>';
                },
            },
            {
                targets: 2,
                orderable: false,
                width: "5%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a href="'+base +'perkara/kinerja/'+full.perkara_id+'">'+ full.nomor_perkara_pa +'</a>\
                    </div>';
                }
            },
            {
                targets: 1,
                orderable: false,
                width: "5%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a href="'+base +'perkara/kinerja/'+full.perkara_id+'">'+ full.nomor_perkara_banding +'</a>\
                    </div>';
                }
            },
            {
                targets: 3,
                width: "20%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="nama">\
                    '+ full.nama_pembanding +'\
                    </div>';
                }
            },
            {
                targets: 4,
                width: "16%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="nama">\
                    '+full.nama_terbanding+'\
                    </div>';
                },
            }

                ],
            });

$('body').on('click', '.kt_edit_button', function(e) {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            Swal.fire({
                title: 'Anda yakin ingin menghapus data yang terpilih?',
                text: "Data yang dihapus tidak dapat dikembalikan",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
            return fetch( base + 'api/perkara/delete?id=' + id)
              .then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            swal.fire(
                        'Dihapus!',
                        'Data berhasil dihapus.',
                        'success'
                        )

            reload();
          }
        })
        });

    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        
        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
        if (repo.id) {
            markup += "<div class='select2-result-repository__description'><small>" + repo.nip + "</small></div>";
        }

        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.nama || repo.text;
    }

$("#km_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan NIP / Nama Ketua Majelis";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketua Majelis",
            allowClear: true,
            ajax: {
                url: base + 'api/hakim/cari',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

var reload = function(){
    var nomor_perkara_pa = $('#nomor_perkara_pa').val();
    var nomor_perkara_banding = $('#nomor_perkara_banding').val();

    params.filter.nomor_perkara_banding = nomor_perkara_banding;
    params.filter.nomor_perkara_pa = nomor_perkara_pa;

    datatable.ajax.url(base + 'api/perkara?' + $.param(params)).load();
}


$(document).on('reset', '#formFilter', function(event) {
    $("#km_id"). empty();
    
  setTimeout(function() {
    reload();
  }, 10);

});

$(document).on('keyup', '#nomor_perkara_banding', debounce(function(){
     reload();
},500));

$(document).on('keyup', '#nomor_perkara_pa', debounce(function(){
     reload();
},500));

};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});