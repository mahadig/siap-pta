
$(function (e) {
    "use strict";

    var base = (document.querySelector('base') || {}).href;

    //SELECT2
    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        
        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nama + "</div>";
        if (repo.id) {
            markup += "<div class='select2-result-repository__description'><small>" + repo.nip + "</small></div>";
        }

        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.nama || repo.text;
    }

    $("#km_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan NIP / Nama Ketua Majelis";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketikkan NIP / Nama Ketua Majelis",
            allowClear: true,
            ajax: {
                url: base + 'api/hakim/cari',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });


    $("#pp_id").select2({
            language:{
                inputTooShort: function(){
                    return "Ketikkan NIP / Nama PP";
                }
            },
            minimumInputLength: 1,
            placeholder: "Ketikkan NIP / Nama PP",
            allowClear: true,
            ajax: {
                url: base + 'api/panitera/cari',
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    results: data,
                    // pagination: {
                    //     more: (params.page * 30) < data.total_count
                    // }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
    
    $(document).on('change', '#grup', function(){
	    var val = $('#grup').val();

	    if(val == 3){
	    	$('#hakim_block').removeClass('hide');
	    	$('#pp_block').addClass('hide');
	    	// required data-validation-required-message="Kolom ini wajib diisi"
	    	$
	    }else if(val == 4){
	    	$('#pp_block').removeClass('hide');
	    	$('#hakim_block').addClass('hide');
	    }else{
	    	$('#hakim_block').addClass('hide');
	    	$('#pp_block').addClass('hide');
	    }
	});

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
		  filter: function () {
		    return $(this).is(":visible");
		  }
		});
    // $("#nomor_perkara_banding").inputmask("99/AAAAA/9999/AAAA");



});