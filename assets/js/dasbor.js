"use strict";
var Dashboard = (function () {
  var initDasbor = function () {
    var base = (document.querySelector("base") || {}).href;

    function setStatChart(stats) {
      new Chart(document.getElementById("chart1"), {
        type: "line",
        data: {
          labels: stats.label,
          datasets: [
            {
              label: "Dikuatkan",
              data: stats.putus_1,
              fill: false,
              backgroundColor: "#64dd17",
              borderColor: "#64dd17",
              lineTension: 0.1
            }, {
              label: "Dibatalkan",
              data: stats.putus_2,
              fill: false,
              backgroundColor: "#e53935",
              borderColor: "#e53935",
              lineTension: 0.1
            }, {
              label: "Tidak dapat diterima",
              data: stats.putus_3,
              fill: false,
              backgroundColor: "#ffca28",
              borderColor: "#ffca28",
              lineTension: 0.1
            }, {
              label: "Dicabut",
              data: stats.putus_4,
              fill: false,
              backgroundColor: "#42a5f5",
              borderColor: "#42a5f5",
              lineTension: 0.1
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          tooltips: {
            mode: "label"
          }
        }
      });
    }

    $(".singledate").datepicker({format: "dd/mm/yyyy", clearBtn: true, language: "id", daysOfWeekDisabled: "0,6", daysOfWeekHighlighted: "0,6"}).on("changeDate", function (ev) {
      var elPeriod = $("#filter_period");

      if (elPeriod.val() === "") {
        elPeriod.val("tgl_register");
      }

      $(".preloader").show();

      loadData();
    });

    var elFilterPeriod = $("#filter_period");
    var elBeban = $("#p_beban");
    var elSelesai = $("#p_selesai");
    var elSisa = $("#p_sisa");
    var elPersentase = $("#p_persentase");
    var elTglStart = $("#tgl_start");
    var elTglEnd = $("#tgl_end");
    var elGreen = $("#p_green");
    var elYellow = $("#p_yellow");
    var elRed = $("#p_red");
    var elbelumMinut = $("#p_belum_minut");

    var elStats1 = $("#st_1");
    var elStats2 = $("#st_2");
    var elStats3 = $("#st_3");
    var elStats4 = $("#st_4");

    function loadData() {
      $(".preloader").show();

      var params = {
        filter_period: elFilterPeriod.val(),
        tgl_start: elTglStart.val(),
        tgl_end: elTglEnd.val()
      };

      $(".informasi a").each(function (i, el) {
        var url = $(el).attr("href");
        var nUrl = url.split("&dsb&")[0];

        $(el).attr("href", nUrl + "&dsb&" + $.param(params));
      });

      $.ajax(base + "api/dasbor?" + $.param(params)).done(function (res) {
        if (res) {
          elBeban.html(res.overview.beban);
          elSelesai.html(res.overview.selesai);
          elSisa.html(res.overview.sisa);
          elPersentase.html(parseFloat(res.overview.rasio).toString() + "%");

          elGreen.html(res.overview.hijau);
          elYellow.html(res.overview.kuning);
          elRed.html(res.overview.merah);
          elbelumMinut.html(res.overview.belum_minut);

          elStats1.html(res.stats.count.putusan_1);
          elStats2.html(res.stats.count.putusan_2);
          elStats3.html(res.stats.count.putusan_3);
          elStats4.html(res.stats.count.putusan_4);

          setStatChart(res.stats);
        }
        $(".preloader").hide();
      }).fail(function () {
        $(".preloader").hide();
        alert("Terjadi kesalahan dalam pengambilan data, silahkan coba lagi.");
      });
    }

    $("#formFilter").on("reset", function (event) {
      $("#km_id").empty();

      setTimeout(function () {
        loadData();
      }, 10);
    });

    $(document).on("change", "#filter_period", function () {
      loadData();
    });

    loadData();
  };

  return {
    init: function () {
      initDasbor();
    }
  };
})();

jQuery(document).ready(function () {
  Dashboard.init();
});
