"use strict";
var KinerjaMajelis = function() {

    var initTable2 = function() {

        $('.singledate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            language: "id",
            daysOfWeekDisabled: "0,6",
            daysOfWeekHighlighted: "0,6"
        }).on('changeDate', function (ev) {
            reload();
        });


        var base = (document.querySelector('base') || {}).href;
        var table = $('#kt_table_2');

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // fixedHeader: true
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            order: [[ 2, "desc" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/perkara/kinerja_hakim',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'km_id', 'nama_km', 'jumlah','perkara_selesai', 
                    'hijau', 'kuning', 'merah'
                    ]
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'id'},
            {data: 'nama'},
            {data: 'km'},
            {data: 'anggota'},
            {data: 'resume'},
            {data: 'konsep'},
            {data: 'total'}
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                orderable: true,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <b>'+full.nama+'</b>\
                    </div>';
                }
            },
            {
                targets: 2,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.km == null) full.km = 0;

                    return '<div class="text-center"><strong>'+parseFloat(full.km).toString()+'</strong></div>';
                }
            },
            {
                targets: 3,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.anggota == null) full.anggota = 0;

                    return '<div class="text-center"><strong>'+parseFloat(full.anggota).toString()+'</strong></div>';
                }
            },
            {
                targets: 4,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.resume == null) full.resume = 0;

                    return '<div class="text-center"><strong>'+parseFloat(full.resume).toString()+'</strong></div>';
                }
            },
            {
                targets: 5,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.konsep == null) full.konsep = 0;

                    return '<div class="text-center"><strong>'+parseFloat(full.konsep).toString()+'</strong></div>';
                }
            },
            {
                targets: 6,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.total == null) full.total = 0;

                    return '<div class="text-center"><strong>'+parseFloat(full.total).toString()+'</strong></div>';
                }
            }
            

                ],
            });

$('.singledate').datepicker({
    format: "dd/mm/yyyy",
    clearBtn: true,
    language: "id",
    daysOfWeekDisabled: "0,6",
    daysOfWeekHighlighted: "0,6"
}).on('changeDate', function (ev) {
    var elPeriod = $('#filter_period')
    
    if(elPeriod.val() === ''){
        elPeriod.val('tgl_register');
    }

    reload();
});

$('#btnExcel').on('click', function(){
    var params = getParams();

    params['order'] = [
        { 
            'column': datatable.order()[0][0], 
            'dir': datatable.order()[0][1] 
        }
    ];

    var url = base + 'excel/excel_kinerja_majelis?' + $.param(params) + $.param(datatable.ajax.params());
    
    window.open(url, '_blank');
});

// $('.singledate').on('apply.daterangepicker', function(ev, picker) {
//   reload();
// });

$("#formFilter").on('reset', function(event) {   
  setTimeout(function() {
    reload();
  }, 10);

});

var getParams = function(){
    var tgl_start = $('#tgl_start').val();
    var tgl_end = $('#tgl_end').val();
    
    var params = {
        filter: {
            tgl_start, tgl_end
        }
    };

    return params;
}

var reload = function(){
    var params = getParams();

    datatable.ajax.url(base + 'api/perkara/kinerja_hakim?' + $.param(params)).load();
}



};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    KinerjaMajelis.init();
});