"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };


        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            responsive: false,
            scrollY: false,
            scrollX: true,
            scrollCollapse: true,
            // Pagination settings
            dom: `
            <'row'<'col-sm-12'>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            order: [[ 0, "disabled" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/perkara/sisa_perkara',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'km_id','nama_km','beban','putus', 'sisa',],
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'km_id'},
            {data: 'nama_km'},
            {data: 'beban'},
            {data: 'putus'},
            {data: 'sisa'}
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 2,
                width: "5%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    <a class="link" href="'+base +'perkara/lists/'+full.km_id+'/all"><b>'+full.beban+'</b></a>\
                    </div>';
                }
            },
            {
                targets: 1,
                width: "15%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <a class="link" href="'+base +'kinerja_majelis/detail/'+full.km_id+'"><b>'+full.nama_km+'</b></a>\
                    </div>';
                }
            },
            {
                targets: 3,
                width: "5%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    <a class="link" href="'+base +'perkara/lists/'+full.km_id+'/done"><b>'+full.putus+'</b></a>\
                    </div>';
                }
            },
            {
                targets: 4,
                width: "5%",
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    <a class="link" href="'+base +'perkara/lists/'+full.km_id+'/pending"><b>'+full.sisa+'</b></a>\
                    </div>';
                },
            }

                ],
            });


var reload = function(){
    // var jenis = $('#jenisSurvey').val();
    // var kuesioner = $('#kuesioner').val();
    // var bulan = $('#bulan').val();
    // var tahun = $('#tahun').val();

    // var params = {
    //     jenis,kuesioner,bulan,tahun
    // };

    // datatable.ajax.url(base + 'api/perkara?' + $.param(params)).load();

    datatable.ajax.url(base + 'api/perkara').load();
}

$('#jenisSurvey').on('change', function(e){
    reload();
});

$('#jenisSurvey').on('change', function(e){
   reload();
});

$('#bulan').on('change', function(e){
   reload();
});

$('#tahun').on('change', function(e){
   reload();
});



};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});