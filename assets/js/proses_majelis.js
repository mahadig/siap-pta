"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        $('.singledate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            language: "id",
            daysOfWeekDisabled: "0,6",
            daysOfWeekHighlighted: "0,6"
        }).on('changeDate', function (ev) {
            reload();
        });

        var base = (document.querySelector('base') || {}).href;
        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // fixedHeader: true
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            order: [[ 2, "desc" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/panitera/proses',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'pp_id',
                    'nama_pp',
                    'jumlah',
                    'minutasi',
                    'sisa',
                    'konsep_p',
                    'konsep_h',
                    'konsep_k',
                    'konsep_m',
                    'minut_p',
                    'minut_h',
                    'minut_k',
                    'minut_m'
                    ]
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'pp_id'},
            {data: 'nama_pp'},
            {data: 'jumlah'},
            {data: 'minutasi'},
            {data: 'sisa'},
            {data: 'konsep_h'},
            {data: 'konsep_h'},
            {data: 'konsep_k'},
            {data: 'konsep_m'},
            {data: 'minut_p'},
            {data: 'minut_h'},
            {data: 'minut_k'},
            {data: 'minut_m'}
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "5%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 2,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.jumlah == null) full.jumlah = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                        // params.type = "selesai";
                        params.list = "kinerja_pp";

                        return '\
                        <div class="text-center ">\
                        <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                        parseFloat(full.jumlah).toString() +'</b></a>\
                        </div>';
                    }
                },
                {
                    targets: 3,
                    width: "5%",
                    orderable: true,
                    render: function(data, type, full, meta) {
                        if(full.minutasi == null) full.minutasi = 0;

                        var params = getParams().filter;
                        params.pp_id = full.pp_id;
                        params.type = "selesai";
                        params.list = "kinerja_pp";

                        return '\
                        <div class="text-center ">\
                        <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                        parseFloat(full.minutasi).toString() +'</b></a>\
                        </div>';
                    }
                },
                {
                    targets: 4,
                    width: "5%",
                    orderable: true,
                    render: function(data, type, full, meta) {
                        if(full.sisa == null) full.sisa = 0;

                        var params = getParams().filter;
                        params.pp_id = full.pp_id;
                        params.type = "sisa";
                        params.list = "kinerja_pp";

                        return '\
                        <div class="text-center ">\
                        <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                        parseFloat(full.sisa).toString() +'</b></a>\
                        </div>';
                    }
                },
                {
                    targets: 5,
                    width: "5%",
                    orderable: true,
                    render: function(data, type, full, meta) {
                        if(full.konsep_p == null) full.konsep_p = 0;

                        var params = getParams().filter;
                        params.pp_id = full.pp_id;
                        params.type = "konsep_p";
                        params.list = "kinerja_pp";

                        return '\
                        <div class="text-center ">\
                        <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                        parseFloat(full.konsep_p).toString() +'</b></a>\
                        </div>';
                }
            },
                {
                    targets: 6,
                    width: "5%",
                    orderable: true,
                    render: function(data, type, full, meta) {
                        if(full.konsep_h == null) full.konsep_h = 0;

                        var params = getParams().filter;
                        params.pp_id = full.pp_id;
                        params.type = "konsep_h";
                        params.list = "kinerja_pp";

                        return '\
                        <div class="text-center ">\
                        <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                        parseFloat(full.konsep_h).toString() +'</b></a>\
                        </div>';

                    // return '\
                    // <div class="text-center">\
                    // <a class="link" href="'+base +'perkara/lists/'+full.km_id+'/all"><b>'+full.beban+'</b></a>\
                    // </div>';
                }
            },
            {
                targets: 7,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.konsep_k == null) full.konsep_k = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "konsep_k";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.konsep_k).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 8,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.konsep_k == null) full.konsep_k = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "konsep_m";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.konsep_m).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 9,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.minut_p == null) full.minut_p = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "minut_p";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.minut_p).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 10,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.minut_h == null) full.minut_h = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "minut_h";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+ 
                    parseFloat(full.minut_h).toString() +'</b></a>\
                    </div>';
                }
            },
            {
                targets: 11,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.minut_k == null) full.minut_k = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "minut_k";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+
                    parseFloat(full.minut_k).toString() +'</b></a>\
                    </div>';

                }
            },
            {
                targets: 12,
                width: "5%",
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.minut_m == null) full.minut_m = 0;

                    var params = getParams().filter;
                    params.pp_id = full.pp_id;
                    params.type = "minut_m";
                    params.list = "kinerja_pp";

                    return '\
                    <div class="text-center ">\
                    <a class="link" href="'+base +'perkara/lists?'+$.param(params)+'"><b>'+
                    parseFloat(full.minut_m).toString() +'</b></a>\
                    </div>';

                }
            },
            {
                targets: 1,
                orderable: true,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    <b>' + full.nama_pp + '</b>\
                    </div>';

                }
            }

            ],
        });


$('#btnExcel').on('click', function(){
    var params = getParams();

    params['order'] = [
        { 
            'column': datatable.order()[0][0], 
            'dir': datatable.order()[0][1] 
        }
    ];

    var url = base + 'excel/excel_proses_majelis?' + $.param(params) + $.param(datatable.ajax.params());
    
    window.open(url, '_blank');
});

// $('.singledate').on('apply.daterangepicker', function(ev, picker) {
//   reload();
// });

$("#formFilter").on('reset', function(event) {   
  setTimeout(function() {
    reload();
}, 10);

});

var getParams = function(){
    var tgl_start = $('#tgl_start').val();
    var tgl_end = $('#tgl_end').val();
    
    var params = {
        filter: {
            tgl_start, tgl_end
        }
    };

    return params;
}

var reload = function(){
    var params = getParams();

    datatable.ajax.url(base + 'api/panitera/proses?' + $.param(params)).load();
}



};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});