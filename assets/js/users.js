"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };


        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // scrollY: false,
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row px-3 '<'col-sm-12 pt-1 col-md-5'i><'col-sm-12 col-md-7 pt-2 dataTables_pager'lp>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            order: [[ 0, "disabled" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/users',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'id', 'username', 'nama','jabatan','hak_akses','status','aksi',],
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'id'},
            {data: 'username'},
            {data: 'nama'},
            {data: 'jabatan'},
            {data: 'hak_akses'},
            {data: 'status'},
            {data: 'aksi', responsivePriority: -1},
            ],
            columnDefs: [
            {
                targets: -1,
                title: 'Aksi',
                width: "10%",
                orderable: false,
                render: function(data, type, full, meta) {
                    if(meta.settings.json.authorized != true){
                        datatable.columns([-1]).visible(false);
                        return "-";
                    }else{

                        return '\
                            <div class="text-center">\
                            <a href="'+base +'settings/users/edit/'+full.id+'">Ubah</a>\
                            |\
                            <a class="kt_edit_button" data-id="'+full.id+'" href="javascript:;">Hapus</a>\
                            </div>\
                            ';
                        }

                        // return '\
                        // <div class="dropdown">\
                        // <a href="javascript:;" class="btn btn-sm btn-default btn-icon btn-icon-md" data-toggle="dropdown">\
                        // <i class="mdi mdi-dots-horizontal"></i>\
                        // </a>\
                        // <div class="dropdown-menu dropdown-menu-right">\
                        // <ul class="kt-nav">\
                        // <li class="kt-nav__item">\
                        // <a href="'+base +'perkara/edit/'+full.perkara_id+'" class="kt-nav__link">\
                        // <i class="kt-nav__link-icon flaticon2-list"></i>\
                        // <span class="kt-nav__link-text">Rincian</span>\
                        // </a>\
                        // </li>\
                        // <li class="kt-nav__item">\
                        // <a href="javascript:;" class="kt-nav__link kt_edit_button" data-id="'+full.perkara_id+'" data-nama="'+full.nama+'">\
                        // <i class="kt-nav__link-icon flaticon2-trash"></i>\
                        // <span class="kt-nav__link-text">Hapus</span>\
                        // </a>\
                        // </li>\
                        // </ul>\
                        // </div>\
                        // </div>\
                        // ';
                    // }
                },
            },
            {
                targets: 0,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1 + meta.settings._iDisplayStart)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+ (full.username ? full.username : '<i>(Belum ditentukan)</i>') +'\
                    </div>';
                }
            },
            {
                targets: 2,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    '+ full.nama +'\
                    </div>';
                }
            },
            {
                targets: 3,
                orderable: false,
                render: function(data, type, full, meta) {

                    if(full.jabatan_text == null){
                        full.jabatan_text = "-";
                    }

                    return '\
                    <div class="text-left">\
                    '+ full.jabatan_text +'\
                    </div>';
                }
            },
            {
                targets: 4,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    '+ full.grup_text +'\
                    </div>';
                }
            },
            {
                targets: 5,
                width: "10%",
                orderable: false,
                render: function(data, type, full, meta) {

                    if(full.status === "1"){
                        return '\
                        <div class="text-center">\
                        <i class="fa text-success fa-check">\
                        </div>';
                    }else{
                        return '\
                        <div class="text-center">\
                        <i class="fa text-danger fa-times">\
                        </div>';
                    }
                }
            }

                ],
            });


$('body').on('click', '.kt_edit_button', function(e) {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            Swal.fire({
                title: 'Anda yakin ingin menghapus data yang terpilih?',
                text: "Data yang dihapus tidak dapat dikembalikan",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
            return fetch( base + 'api/users/delete?id=' + id)
              .then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            swal.fire(
                        'Dihapus!',
                        'Data berhasil dihapus.',
                        'success'
                        )

            reload();
          }
        })
        });


var reload = function(){
    // var jenis = $('#jenisSurvey').val();
    // var kuesioner = $('#kuesioner').val();
    // var bulan = $('#bulan').val();
    // var tahun = $('#tahun').val();

    // var params = {
    //     jenis,kuesioner,bulan,tahun
    // };

    // datatable.ajax.url(base + 'api/tabulasi?' + $.param(params)).load();

    datatable.ajax.url(base + 'api/hakim').load();
}

$('#jenisSurvey').on('change', function(e){
    reload();
});

$('#jenisSurvey').on('change', function(e){
   reload();
});

$('#bulan').on('change', function(e){
   reload();
});

$('#tahun').on('change', function(e){
   reload();
});


};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});