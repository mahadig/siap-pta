"use strict";
var KinerjaMajelis = function() {

    var initTable2 = function() {

        $('.singledate').datepicker({
            format: "dd/mm/yyyy",
            clearBtn: true,
            language: "id",
            daysOfWeekDisabled: "0,6",
            daysOfWeekHighlighted: "0,6"
        }).on('changeDate', function (ev) {
            reload();
        });


        var base = (document.querySelector('base') || {}).href;
        var table = $('#kt_table_2');

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // fixedHeader: true
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            // pageLength: 10,
            
            order: [[ 2, "desc" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/panitera/kinerja_pp',
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'km_id', 'nama_pp', 'k_konsep','k_minut', 
                    'nilai_singkron', 'nilai_telaah', 'nilai_akhir'
                    ]
                },
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'km_id'},
            {data: 'nama_pp'},
            {data: 'k_konsep'},
            {data: 'k_minut'},
            {data: 'nilai_singkron'},
            {data: 'nilai_telaah'},
            {data: 'nilai_akhir'}
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "1%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                orderable: true,
                render: function(data, type, full, meta) {

                    // return '\
                    // <div class="text-left">\
                    // <a class="link" href="'+base +'kinerja_majelis/detail/\
                    // '+full.pp_id + '"><b>'+full.nama_pp+'</b></a>\
                    // </div>';

                    return '\
                    <div class="text-left ">\
                    <b>'+ full.nama_pp +'</b>\
                    </div>';
                }
            },
            {
                targets: 2,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.k_konsep == null) full.k_konsep = 0;

                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.k_konsep).toString() +'%\
                    </div>';
                }
            },
            {
                targets: 3,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.k_minut == null) full.k_minut = 0;

                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.k_minut).toString() +'%\
                    </div>';
                }
            },
            {
                targets: 4,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.nilai_singkron == null) full.nilai_singkron = 0;

                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.nilai_singkron).toString() +'%\
                    </div>';
                }
            },
            {
                targets: 5,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.nilai_telaah == null) full.nilai_telaah = 0;

                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.nilai_telaah).toString() +'%\
                    </div>';
                }
            },
            {
                targets: 6,
                orderable: true,
                render: function(data, type, full, meta) {
                    if(full.nilai_akhir == null) full.nilai_akhir = 0;

                    return '\
                    <div class="text-center ">\
                    '+ parseFloat(full.nilai_akhir).toString() +'%\
                    </div>';
                }
            }
            

                ],
            });

$('.singledate').datepicker({
    format: "dd/mm/yyyy",
    clearBtn: true,
    language: "id",
    daysOfWeekDisabled: "0,6",
    daysOfWeekHighlighted: "0,6"
}).on('changeDate', function (ev) {
    var elPeriod = $('#filter_period')
    
    if(elPeriod.val() === ''){
        elPeriod.val('tgl_register');
    }

    reload();
});

$('#btnExcel').on('click', function(){
    var params = getParams();

    params['order'] = [
        { 
            'column': datatable.order()[0][0], 
            'dir': datatable.order()[0][1] 
        }
    ];

    var url = base + 'excel/excel_kinerja_pp?' + $.param(params) + $.param(datatable.ajax.params());
    
    window.open(url, '_blank');
});

// $('.singledate').on('apply.daterangepicker', function(ev, picker) {
//   reload();
// });

$("#formFilter").on('reset', function(event) {   
  setTimeout(function() {
    reload();
  }, 10);

});

var getParams = function(){
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();

    var params = {
        filter: {bulan,tahun}
    };

    return params;
}

var reload = function(){
    var params = getParams();

    datatable.ajax.url(base + 'api/panitera/kinerja_pp?' + $.param(params)).load();
}

$("#formFilter").on('reset', function(event) {
    
  setTimeout(function() {
    reload();
  }, 10);

});

$('#bulan').on('change', function(e){
   reload();
});

$('#tahun').on('change', function(e){
   reload();
});



};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    KinerjaMajelis.init();
});