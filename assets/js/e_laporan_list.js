"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };


        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // scrollY: false,
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            pageLength: 25,
            order: [[ 0, "disabled" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/e_laporan?' + $.param(params),
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'id', 'nama_satker', 'nama_pp','tgl_konfirmasi','tgl_telaah','keterangan'],
                },
            },
            rowCallback: function(row, data, index) {
                // console.log(data);
                if(data.nama_pp == null || data.tgl_konfirmasi == null || data.tgl_telaah == null){
                    $(row).addClass("yellow");
                }
            },
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'id'},
            {data: 'nama_satker'},
            {data: 'nama_pp'},
            {data: 'tgl_konfirmasi'},
            {data: 'tgl_telaah'},
            {data: 'keterangan'}
            // {data: 'aksi', responsivePriority: -1},
            ],
            columnDefs: [
            {
                targets: -1,
                title: 'Aksi',
                width: "9%",
                orderable: false,
                render: function(data, type, full, meta) {
                    if(meta.settings.json.authorized != true){
                        datatable.columns([-1]).visible(false);
                        return "-";
                    }else{

                        var bulan = $('#bulan').val();
                        var tahun = $('#tahun').val();

                        var params = {
                            bulan,tahun
                        };

                        return '<div class="text-center mt-0 mb-0"><a class="btn btn-sm btn-primary text-white" href="'+base +'e_laporan/edit/'+full.satker_id+'?'+$.param(params)+'">Ubah</a></div>';

                        // return '<div class="btn-group">\
                        //     <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                        //     <i class="mdi mdi-dots-horizontal"></i>\
                        //     </button>\
                        //     <div class="dropdown-menu">\
                        //     <a class="dropdown-item" href="'+base +'e_laporan/edit/'+full.satker_id+'?'+$.param(params)+'">Ubah</a>\
                        //     <a class="dropdown-item kt_edit_button" data-id="'+full.id+'" href="javascript:;">Hapus</a>\
                        //     </div>\
                        //     </div>';
                        }

                        // return '\
                        // <div class="dropdown">\
                        // <a href="javascript:;" class="btn btn-sm btn-default btn-icon btn-icon-md" data-toggle="dropdown">\
                        // <i class="mdi mdi-dots-horizontal"></i>\
                        // </a>\
                        // <div class="dropdown-menu dropdown-menu-right">\
                        // <ul class="kt-nav">\
                        // <li class="kt-nav__item">\
                        // <a href="'+base +'perkara/edit/'+full.perkara_id+'" class="kt-nav__link">\
                        // <i class="kt-nav__link-icon flaticon2-list"></i>\
                        // <span class="kt-nav__link-text">Rincian</span>\
                        // </a>\
                        // </li>\
                        // <li class="kt-nav__item">\
                        // <a href="javascript:;" class="kt-nav__link kt_edit_button" data-id="'+full.perkara_id+'" data-nama="'+full.nama+'">\
                        // <i class="kt-nav__link-icon flaticon2-trash"></i>\
                        // <span class="kt-nav__link-text">Hapus</span>\
                        // </a>\
                        // </li>\
                        // </ul>\
                        // </div>\
                        // </div>\
                        // ';
                    // }
                },
            },
            {
                targets: 0,
                orderable: false,
                width: "5%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1 + meta.settings._iDisplayStart)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    '+ full.nama_singkat +'\
                    </div>';
                }
            },
            {
                targets: 2,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    '+ (full.nama_pp == null ? '-' : full.nama_pp) +'\
                    </div>';
                }
            },
            {
                targets: 3,
                // width: "10%",
                orderable: false,
                render: function(data, type, full, meta) {
                    var tanggal = moment(full.tgl_konfirmasi).format('DD/MM/YYYY');

                    if(full.tgl_konfirmasi == null){
                        tanggal = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ tanggal +'\
                    </div>';
                }
            },
            {
                targets: 4,
                // width: "10%",
                orderable: false,
                render: function(data, type, full, meta) {

                    var tanggal = moment(full.tgl_telaah).format('DD/MM/YYYY');

                    if(full.tgl_telaah == null){
                        tanggal = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ tanggal +'\
                    </div>';
                }
            }

                ],
            });


$('body').on('click', '.kt_edit_button', function(e) {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            Swal.fire({
                title: 'Anda yakin ingin menghapus data yang terpilih?',
                text: "Data yang dihapus tidak dapat dikembalikan",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
            return fetch( base + 'api/e_laporan/delete?id=' + id)
              .then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            swal.fire(
                        'Dihapus!',
                        'Data berhasil dihapus.',
                        'success'
                        )

            reload();
          }
        })
        });

var getParams = function(){
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();

    var params = {
        filter: {bulan,tahun}
    };

    return params;
}

$('#btnExcel').on('click', function(){
    var params = getParams();

    var url = base + 'excel/excel_e_laporan?' + $.param(params);
    window.open(url, '_blank');
});

var reload = function(){
    // var jenis = $('#jenisSurvey').val();
    // var kuesioner = $('#kuesioner').val();
    // var bulan = $('#bulan').val();
    // var tahun = $('#tahun').val();

    var params = getParams();

    // datatable.ajax.url(base + 'api/tabulasi?' + $.param(params)).load();

    datatable.ajax.url(base + 'api/e_laporan?'+ $.param(params)).load();
}

$("#formFilter").on('reset', function(event) {
    
  setTimeout(function() {
    reload();
  }, 10);

});

$('#bulan').on('change', function(e){
   reload();
});

$('#tahun').on('change', function(e){
   reload();
});


};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});