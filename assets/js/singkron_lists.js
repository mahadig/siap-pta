"use strict";
var Tabulasi = function() {


    var initTable2 = function() {

        // var jenis = $('#jenisSurvey').val();
        // var kuesioner = $('#kuesioner').val();
        // var bulan = $('#bulan').val();
        // var tahun = $('#tahun').val();

        // var params = {
        //     jenis,kuesioner,bulan,tahun
        // };


        var base = (document.querySelector('base') || {}).href;

        var table = $('#kt_table_2');
        // begin second table

        var initPopover = function(el) {
            var skin = el.data('skin') ? 'popover-' + el.data('skin') : '';
            var triggerValue = el.data('trigger') ? el.data('trigger') : 'hover';
            el.popover({
                trigger: triggerValue,
                template: '\
                <div class="popover ' + skin + '" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body"></div>\
                </div>'
            });
        }

        var clearPopover = function(el){
            el.remove();
        }

        var clearPopovers = function(){
            $('div.popover').each(function(){
                clearPopover($(this));
            });
        }

        var initPopovers = function() {
            // init bootstrap popover
            clearPopovers();
            
            $('[data-toggle="kt-popover"]').each(function() {
                initPopover($(this));
            });
        }

        var datatable = table.DataTable({
            // responsive: false,
            // scrollY: false,
            // scrollX: true,
            // scrollCollapse: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            // lengthMenu: [5, 10, 25, 50],

            pageLength: 25,
            order: [[ 0, "disabled" ]],

            // language: {
            //     'lengthMenu': 'Display _MENU_',
            // },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                // url: base + 'api/perkara?' + $.param(params),
                url: base + 'api/e_laporan/singkron?' + $.param(params),
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                    'id', 'nama_pp', 'tgl_singkron'],
                },
            },
            
            drawCallback:function( settings){
                initPopovers();
                // call your function here
            },
            columns: [
            {data: 'id'},
            {data: 'nama_pp'},
            {data: 'tgl_singkron'}
            // {data: 'aksi', responsivePriority: -1},
            ],
            columnDefs: [
            
            {
                targets: 0,
                orderable: false,
                width: "5%",
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-center">\
                    '+(meta.row + 1 + meta.settings._iDisplayStart)+'\
                    </div>';
                },
            },
            {
                targets: 1,
                orderable: false,
                render: function(data, type, full, meta) {

                    return '\
                    <div class="text-left">\
                    '+ full.nama_pp +'\
                    </div>';
                }
            },
            {
                targets: 2,
                // width: "10%",
                orderable: false,
                render: function(data, type, full, meta) {
                    var tanggal = moment(full.tgl_singkron).format('DD/MM/YYYY');

                    if(full.tgl_singkron == null){
                        tanggal = '-';
                    }

                    return '\
                    <div class="text-center">\
                    '+ tanggal +'\
                    </div>';
                }
            }

                ],
            });


$('body').on('click', '.kt_edit_button', function(e) {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            Swal.fire({
                title: 'Anda yakin ingin menghapus data yang terpilih?',
                text: "Data yang dihapus tidak dapat dikembalikan",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
            return fetch( base + 'api/e_laporan/delete?id=' + id)
              .then(response => {
                if (!response.ok) {
                  throw new Error(response.statusText)
                }
                return response.json()
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            swal.fire(
                        'Dihapus!',
                        'Data berhasil dihapus.',
                        'success'
                        )

            reload();
          }
        })
        });

var getParams = function(){
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();

    var params = {
        filter: {bulan,tahun}
    };

    return params;
}

$('#btnExcel').on('click', function(){
    var params = getParams();

    var url = base + 'excel/excel_e_laporan?' + $.param(params);
    window.open(url, '_blank');
});

var reload = function(){
    // var jenis = $('#jenisSurvey').val();
    // var kuesioner = $('#kuesioner').val();
    // var bulan = $('#bulan').val();
    // var tahun = $('#tahun').val();

    var params = getParams();

    // datatable.ajax.url(base + 'api/tabulasi?' + $.param(params)).load();

    datatable.ajax.url(base + 'api/e_laporan?'+ $.param(params)).load();
}

$("#formFilter").on('reset', function(event) {
    
  setTimeout(function() {
    reload();
  }, 10);

});

$('#bulan').on('change', function(e){
   reload();
});

$('#tahun').on('change', function(e){
   reload();
});


};

return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
    Tabulasi.init();
});