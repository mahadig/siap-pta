###################
SIAP PTAA
###################

*******************
PENILAIAN
*******************
1. Lama Proses Keseluruhan (register-minutasi)
 a. SOP 2bln ; Perma 3 bln
 b. Hijau 0 – 2 bulan,Kuning 2-3 bulan, merah diatas 3 bulan
2. Lama Proses Pengiriman Berkas Banding (tanggal Kirim)
 a. SOP 1-45hari hijau , 
 b. Sewilayah : 1-30 hijau, 30-45 Kuning, >45 merah
 c. SOP 46-60 
 d. Beda Wilayah; 1-45 hijau, 45-60 kuning, >60 merah
3. Lama Proses PHS (PMH-PHS)
 a. 1-3 Hijau, 3-7 Kuning, >7 Merah
4. Proses Persidangan (Sidang Pertama-Putus)
 a. 0-60 Hijau, 60-90 Kuning, >90 merah
5. Tanggal Minutasi(minut-putus)
 a. 0-1 Hijau, 2-5 Kuning, >5 Merah
6. Tanggal Serah(serah panmud banding-minut)
 a. 0-1 hijau, 2-5 Kuning, >5 Merah
7. Tanggal Kirim (tanggal kirim-tanggal terima)
 a. 0-3 Hijau, 3-7 Kuning, >7 Merah
8. Tanggal Serah Meja 3 (tanggal terima-tanggal kirim satker)
 a. 0-3 Hijau, 3-7 Kuning, >7 Merah
9. Tanggal Anonimasi (anonym-serah berkas)
 a. 0-3 Hijau, 3-7 Kuning, >7 Merah
10. Tanggal Upload (upload-anonim)
 a. 0-3 Hijau, 3-7 Kuning, >7 Merah
